#include "ard_serial_ui.h"

// needed to make the native tests work ??
#include <Arduino.h>

#include "ard_event_handler.h"
#include "ard_event_manager.h"



ArdSerialEventHandler::ArdSerialEventHandler(
                        ArdSysInterface *a_ard_sys_int,
                        ArdEventManager *a_event_manager)
   : m_event_manager(a_event_manager), m_ard_sys_int(a_ard_sys_int),
     m_rx_buffer_pos(0)
{
    Serial.begin(9600);

    for (size_t i = 0; i < m_buffer_max_size; i++) {
        m_rx_buffer[i] = 0;
    }
    m_event_manager->registerEventHandler(this);
}

bool ArdSerialEventHandler::CheckForEvent() {
    if( Serial.available()==0 ) {
        return false;
    }
    // Serial.print(F("Arduino heard you say: "));

    size_t i = 0;
    while (Serial.available()>0 && i<m_buffer_max_size){
      m_rx_buffer[i] = Serial.read();	//read Serial
      i++;
    }
    m_rx_buffer_pos = i;

    return true;
}


void ArdSerialEventHandler::RunEventHandler() {
    // In this base class, we just print an echo of the buffer
    // and release it.
    m_rx_buffer[m_rx_buffer_pos] = '\0';
    Serial.print((char*)m_rx_buffer);
    m_rx_buffer_pos = 0;
}

