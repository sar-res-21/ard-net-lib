/**
 * @file ard_bitmap.h
 * @brief A bitmask of variable length.
 *
 */

#ifndef ARD_BITMASK_H
#define ARD_BITMASK_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h> // for uint8_t
#else
#include <cstdint> // for uint8_t
#endif

namespace ArdNetConstants {
constexpr uint8_t bitmap_max_size = 128; /**< The maximum bitmap size */
constexpr uint8_t bitmap_full =
    129; /**< Used to signal that the bitmap is full*/
} // namespace ArdNetConstants

/**
 * @brief A bitmask of variable length.
 *
 */
class ArdBitMap {
public:
  /**
   * @brief Constructs a new Bit Mask object with n bits.
   *
   * ArdNetConstants::bitmap_max_size is the maximum allowed size for the
   * bitmap. The value is arbiratry but, in any case, it is more than enough for
   * our needs.
   *
   * @param size The number of bits in the mask.
   */
  ArdBitMap(const uint8_t size);

  /**
   * @brief Destroy the Bit Map object
   *
   */
  ~ArdBitMap();

  /**
   * @brief Checks if the bit in position pos is set.
   *
   * This method calls the error function if pos is not a valid index value.
   *
   * @param n The position of the bit to check.
   * @return true The bit is set.
   * @return false The bit is not set.
   */
  bool is_bit_set(uint8_t n);

  /**
   * @brief Set the n-th bit
   *
   * This method calls the error function if pos is not a valid index value.
   *
   * @param n The bit to set.
   */
  void set_bit(uint8_t n);

  /**
   * @brief Unset the n-th bit
   *
   * This method calls the error function if pos is not a valid index value or
   * if the bit was not set.
   *
   * @param n The bit to unset.
   *
   */
  void unset_bit(uint8_t n);

  /**
   * @brief Returns the index of the first unset bit.
   *
   * @return int If the value is the constant ArdNetConstants::bitmap_full it
   * means that all the bits are set.
   */
  uint8_t find_unset_bit();

private:
  const uint8_t num_bits;
  const uint8_t num_bytes;
  uint8_t *bitmap;
};

#endif // end include guard ARD_BITMASK_H