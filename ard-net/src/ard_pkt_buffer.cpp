//#ifndef ARDUINO_AVR_NANO
//#include <iostream>
//#endif

#include "ard_pkt_buffer.h"

ArdPktBuffer::ArdPktBuffer() : curr_size(0) {
  for (uint8_t i = 0; i < ArdNetConstants::ard_pkt_buffer_data_size; ++i) {
    data[i] = 0;
  }
}

void ArdPktBuffer::reset() {
  curr_size = 0;
  for (uint8_t i = 0; i < ArdNetConstants::ard_pkt_buffer_data_size; ++i) {
    data[i] = 0;
  }
}

#ifndef ARDUINO_AVR_NANO
std::ostringstream ArdPktBuffer::prettyPrint() const{
  std::ostringstream os;
  os << "Packet buffer: curr_size = " << int(curr_size) <<std::endl;
  for (int i =0; i< curr_size; ++i){
    os << "data[" << i << "]=" << std::hex << int(data[i]);
    os << std::dec << std::endl;
  }
  return os;
}
#endif

bool operator==(const ArdPktBuffer &b1, const ArdPktBuffer &b2) {
  bool res = false;
  if (b1.curr_size == b2.curr_size) {
    res = true;
    uint8_t i = 0;
    while (i < b1.curr_size && res) {
      if (b1.data[i] != b2.data[i]) {
        res = false;
      }
      ++i;
    }
  }
  return res;
}