/**
 * @file ard_app_train.h
 * @author Christophe Couturier
 * @brief Class for the application of the train
 *
 */

#ifndef ARD_APP_TRAIN_H
#define ARD_APP_TRAIN_H

#include "ard_app_base.h"
#include "ard_hc_sr04.h"
#include "ard_network_application_message.h"

class AppTrain;

class TrainStopFunctor : public TimerFunctor {
  friend class AppTrain;

public:
  TrainStopFunctor();

  void operator()();
  void setParent(AppTrain *a_parent);

protected:
  AppTrain *m_parent;
};




class DistanceReadFunctor : public TimerFunctor {
friend class AppTrain;
public:
  DistanceReadFunctor();

  void operator()();
  void setParent(AppTrain *a_parent);

protected:
  AppTrain *m_parent;
};




class AppTrain: public AppBase {
friend class TrainStopFunctor;
friend class DistanceReadFunctor;
public:
    AppTrain(PktMemPool *a_mem_pool, 
             ArdSysInterface *a_ard_sys_int, 
             ArdEventManager *a_event_manager,
             SendInterface<AppAddr> *a_send_interface,
             AppAddr a_this_addr);
    virtual void init();  /// Call this from the setup() function
  
    int m_motor_pin_1A = 16;    // AIO1
    int m_motor_pin_2A = 3;     // IRQ1
    int m_motor_pin_enable = 6; // DIO3 (PWM Pin)
    int m_sensor_trig_pin[2] = {4, 5};
    int m_sensor_echo_pin[2] = {14, 15};
    bool m_notify_general_controller; // Send a notification to the general controller (in network 254) if state change (default=false)

// Serial Port UI Processing
protected:
    virtual bool ProcessSerialCmd(unsigned char *msg, size_t msgSize);
    void processTrainCmd(char *msg, size_t msgSize);

    // Rx Radio Message Processing
    virtual PktBufPtr processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                    bool *p_result);
    void sendTrainStatusMessage(AppAddr a_l3_dest_addr);
    AppAddr m_last_requester_addr;

// Train Movement
protected:
    bool m_traction_ON;
    volatile bool m_traction_direction;
    volatile uint8_t m_traction_speed;
    volatile uint8_t m_adjusted_speed;
    bool m_last_sent_traction_state; // Last value of m_traction_ON transmitted to central controller
    uint8_t m_last_sent_traction_speed; // Last value of m_adjusted_speed transmitted to central controller
    bool m_last_traction_direction; // Value of m_traction_direction used for last status transmitted to central controller
    HcSr04int m_dist_sensors[2];
    long m_last_distance[2];

    void applyCommand(TrainControlMessage a_tcMsg);
    void moveTrain(bool a_direction, 
                   uint16_t a_duration, uint8_t a_speed);
    void setMotor();
    bool avoidCollision();
    void stopMotor(bool a_update_current_traction_state=true);
    void sendStateToControllerIfNeeded();


    ArdTimerEventHandler m_train_stop_timer;    
    TrainStopFunctor m_train_stop_functor;   

    ArdTimerEventHandler m_distance_read_timer;    
    DistanceReadFunctor m_distance_read_functor;   
};

#endif // end include guard ARD_APP_TRAIN_H
