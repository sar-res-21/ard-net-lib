#include "ard_error.h"
#include "ard_iostream.h"

#ifdef ARDUINO_AVR_NANO

// For the time being, we have the same code for both versions but the Arduino
// one is evenutally going to call the code to make the led blink.

void ard_error(const ard_string_type *msg) {
  (*ard_out_stream) << msg << std::endl;
  // g_event_manager
}

#else
void ard_error(const ard_string_type *msg) {
  (*ard_out_stream) << msg << std::endl;
  throw SimpleException(msg);
}

#endif
