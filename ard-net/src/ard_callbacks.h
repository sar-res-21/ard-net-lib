/**
 * @file ard_callbacks.h
 * @brief Callback related classes.
 *
 */
#ifndef ARD_CALLBACKS_H
#define ARD_CALLBACKS_H

#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_pkt_buffer.h"
#include "ard_sys_interface.h"
#include "ard_utils.h"

/** \addtogroup most_important 
 *  @{
 */

// FIXME: make sure that we do need the following forward declarations
class ArdNetworkLayerOne;


// from https://isocpp.org/wiki/faq/pointers-to-members
#define CALL_MEMBER_FN(object, ptrToMember) ((object).*(ptrToMember))
// FredMemFn points to a member of Fred that takes (char,float)
// typedef  int (Fred::*FredMemFn)(char x, float y);
typedef void (ArdNetworkLayerOne::*ArdNetworkLayerOneSend)(
    ArdUniquePtrMemPool<ArdPktBuffer>, L1Addr);

/**
 * @brief Base class for all the timer functors.  They must define the function
 * call operator that takes no arguments and returns nothing.
 *
 */
class TimerFunctor {
public:
  virtual void operator()() = 0;
};

/**
 * @brief Functor that sends a packet at layer 1.
 *
 * It is used when the radio device is busy.
 *
 * @note This functor is tightly coupled with thee network layer one.  There is
 * no point in trying to use it somewhere else.
 *
 */
class SendPktBufferLOneFunc : public TimerFunctor {
public:
  SendPktBufferLOneFunc(ArdNetworkLayerOne *a_net_layer,
                        ArdNetworkLayerOneSend a_send_fun)
      : m_net_layer(a_net_layer), m_send_fun(a_send_fun),
        m_pkt_buf_ptr(nullptr), m_dst_addr() {}
  void operator()() override {
    if (m_pkt_buf_ptr) {
      CALL_MEMBER_FN(*m_net_layer, m_send_fun)
      (ard_move(m_pkt_buf_ptr), m_dst_addr);
    } else {
      ard_error(ARD_F("Layer1Functor operator() pkt_buff_ptr not valid"));
    }
  }
  void setPktBuff(PktBufPtr a_ptr, L1Addr a_dst_addr) {
    m_pkt_buf_ptr = ard_move(a_ptr);
    m_dst_addr = a_dst_addr;
  }

private:
  ArdNetworkLayerOne *m_net_layer;
  ArdNetworkLayerOneSend m_send_fun;
  PktBufPtr m_pkt_buf_ptr;
  L1Addr m_dst_addr;
};

/**
 * @brief A functor that makes the LED blink.
 *
 * Meant to be used by ard_error.
 */
class BlinkLEDFunc : public TimerFunctor {
public:
  explicit BlinkLEDFunc(ArdSysInterface *a_ard_sys_int);
  void operator()() override;

private:
  enum LEDState { ON, OFF };
  LEDState m_state;
  ArdSysInterface *m_ard_sys_int;
};

/** @}*/

#endif // End of include guard ARD_CALLBACKS_H
