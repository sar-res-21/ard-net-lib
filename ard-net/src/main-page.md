Welcome to the docs
===================

This documentation is automatically generated from the source code
of the project.

Please use Classes and Files menus above or the search text zone to find
the information you need.

[This page](group__most__important.html) gives short-cuts to most
relevant classes that you'll need to implement the project.


<!-- And [this one](group__aloha.html) lists libraries related to the part of
the project about Aloha. Note that **you do not need the Aloha related
code for the SAR**. Just ignore all classes or files with "aloha" in
their name. -->
