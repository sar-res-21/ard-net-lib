#include <Arduino.h>

#include "ard_network_application_message.h" // for ntohs
#include "ard_network_eth.h"
// #include <IPAddress.h> // FOR DEBUG ONLY!!!! (IPAddress in udpSerialPrint...)



/********************************************************************
 *                        ArdEthEventHandler                       *
 * *****************************************************************/

ArdEthEventHandler::ArdEthEventHandler(ArdNetworkEthernet *a_net_layer_eth,
        ArdSysInterface *a_ard_sys_int)
    : m_net_layer_eth(a_net_layer_eth), m_ard_sys_int(a_ard_sys_int) {}

bool ArdEthEventHandler::CheckForEvent() {
    //this must be called for ethercard functions to work.
    ether.packetLoop(ether.packetReceive());

    // Nerver emit event. ether.packetLoop does it for us...
    return false;
}

void ArdEthEventHandler::RunEventHandler() {
  m_net_layer_eth->retrieveEthernetFrame();
}

/********************************************************************
 *            Functor for EthListen (EtherCard API)                 *
 * ******************************************************************/
ArdNetworkEthernet *g_uggly_pointer_to_active_instance = NULL;

/********************************************************************
 *                    Ethernet Layer One                            *
 * ******************************************************************/

uint8_t Ethernet::buffer[100];
static byte mymac[] = { 0xd4,0x81,0xd7,0x37,0xf0,0x13 };


ArdNetworkEthernet::ArdNetworkEthernet(PktMemPool *a_mem_pool,
                                       ArdSysInterface *a_ard_sys_int,
                                       ArdEventManager *a_event_manager,
                                       L1Addr a_this_addr)
    : ArdNetworkLayer(a_mem_pool, a_ard_sys_int, a_event_manager, nullptr,
                      a_this_addr),
      m_event_handler(this, a_ard_sys_int)
      {
           g_uggly_pointer_to_active_instance = this;
      }

bool ArdNetworkEthernet::init() {
    info_pr(ARD_F("[ArdNetworkEthernet] Try to initialize Ethernet interface (error if no other trace)...\n"));
    if (ether.begin(sizeof Ethernet::buffer, mymac, m_ss_pin) == 0){
    // if (ether.begin(sizeof Ethernet::buffer, m_mac, m_ss_pin) == 0){
        ard_error(ARD_F("Failed to access Ethernet controller\n"));
        return false;
    }
    
    const static uint8_t ip[] = {192,168,2,100};
    const static uint8_t gw[] = {192,168,2,254};
    const static uint8_t dns[] = {192,168,2,1};
    const static uint8_t mask[] = {255,255,255,0};
    if( !ether.staticSetup(ip, gw, dns, mask) ){
        lib_debug_pr(ARD_F("Failed to access Ethernet controller\n"));
    }
    
    ether.addEthListener(&ArdNetworkEthernet::ethListener);
    m_event_manager->registerEventHandler(&m_event_handler); 

    info_pr(ARD_F("[ArdNetworkEthernet] Ethernet init OK\n"));
    return true;
}

void ArdNetworkEthernet::sendRequest(PktBufPtr a_p, L1Addr a_l1_addr) {
    lib_debug_pr(ARD_F("Ethernet Layer Send, size: "), int(a_p->curr_size), ARD_F("\n"));
    if (!m_ard_sys_int) {
        ard_error(ARD_F("Ethernet Layer sendRequest m_sys_interface is null\n "));
    }

    // Add length field (2 bytes)
    PktBufPtr pdu = m_mem_pool->AllocateSlot();
    uint16_t sdu_length = a_p->curr_size;
    pdu->curr_size = a_p->curr_size + 2;
    for (uint8_t i = 0; i < sdu_length; ++i)
        pdu->data[i+2] = a_p->data[i];
    uint16_t *p = (uint16_t*)pdu->data; 
    *p= htons(sdu_length);

// char hexBuf[8];
// for(int i=0; i<pdu->curr_size; i++){
//     sprintf(hexBuf, "%02X", (uint8_t)pdu->data[i] );
//     hexBuf[4]='\0'; // just for safety
//     info_pr(ARD_F(" "), hexBuf);
// }

    // ether.ethSend(a_p->data, a_p->curr_size);
    ether.ethSend(pdu->data, pdu->curr_size);
}

void ArdNetworkEthernet::ethListener(const char *data, uint16_t len)
{
    ArdNetworkEthernet *p_this = g_uggly_pointer_to_active_instance;
    if (!p_this) return;

    lib_debug_pr(ARD_F("Received RAW Ethernet: "), len, ARD_F(" bytes\n") );
    for (uint16_t i=0; i<len; i++){
        Serial.print((uint8_t)data[i], HEX);
        Serial.print(" ");
    } 
    lib_debug_pr(ARD_F("\n"));

    if (len < 2){
        error_pr(ARD_F("Discarding Too small Ethernet frame\n"));
        return;
    }

    // Retrieve actual data length
    uint16_t sdu_length = *((uint16_t*)data);
    sdu_length = ntohs(sdu_length);
    lib_debug_pr(ARD_F("  Actual length or eth data: "), sdu_length, ARD_F(" bytes\n") );

    // Allocate a buffer and copy rx data (without eth header nor padding)
    PktBufPtr a_p = p_this->m_mem_pool->AllocateSlot();
    a_p->curr_size = sdu_length;

    for (uint8_t i = 0; i < sdu_length; ++i)
        a_p->data[i] = data[ 2 + i];

    // PktBufPtr a_p = p_this->m_mem_pool->AllocateSlot();
    // for (uint8_t i = 0; i < len; ++i)
    //     a_p->data[i] = data[i];
    // a_p->curr_size = len;

    // Pass the buffer to the upper layer 
    if (p_this->m_upper_layer) {
        // lib_debug_pr(ARD_F("Eth passing message to upper layer\n"));
        // lib_debug_pr(ARD_F("a_p->data[0]: "), a_p->data[0], ARD_F("\n"));
        p_this->m_upper_layer->onDataReceived(ard_move(a_p), 
                     AnyAddr(p_this->m_this_addr), p_this->m_l_id);
    }
}


void ArdNetworkEthernet::retrieveEthernetFrame() {
//   PktBufPtr a_p = m_mem_pool->AllocateSlot();
//   for (uint8_t i = 0; i < rf12_len; ++i)
//     a_p->data[i] = rf12_data[i];
//   a_p->curr_size = rf12_len;

//   lib_debug_pr(ARD_F("L1 Receive called with "), int(a_p->curr_size),
//                ARD_F("  bytes+++++++++++++++++++++++++++++++++++++++++++\n"));

//   if (m_upper_layer) {
//     lib_debug_pr(ARD_F("L2 passing message to upper layer\n"));
//     lib_debug_pr(ARD_F("a_p->data[0]: "), a_p->data[0], ARD_F("\n"));
//     m_upper_layer->onDataReceived(ard_move(a_p), AnyAddr(m_this_addr), m_l_id);
//   }
}
