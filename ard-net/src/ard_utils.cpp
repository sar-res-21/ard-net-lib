#include "ard_utils.h"
#include "Arduino.h"
#include "ard_log.h"

void softResetArduino(bool a_print_trace)
{
    if(a_print_trace){
        info_pr(ARD_F("\nResetting...\n\n"));
        delay(150); // Tempo for trace output
    }
    asm volatile ("  jmp 0"); // Reset the program
}
