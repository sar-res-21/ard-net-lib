#include "ard_event_manager.h"
#include "ard_error.h"
#include "ard_log.h"

ArdEventManager::ArdEventManager(ArdSysInterface *a_ard_sys_int)
    : m_next_timer(nullptr), m_ard_sys_int(a_ard_sys_int) {
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    m_event_handlers[i] = nullptr;
    m_timer_event_handlers[i] = nullptr;
  }
}

void ArdEventManager::registerEventHandler(ArdEventHandler *a_event_handler) {
  uint8_t i;
  for (i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (!m_event_handlers[i]) {
      m_event_handlers[i] = a_event_handler;
      return;
    }
  }
  if (i == ArdNetConstants::event_hndlr_slots) {
    error_pr(ARD_F("Event Manager: m_event_handlers full\n"));
    ard_error(ARD_F("Event Manager: m_event_handlers full\n"));
  }
}

void ArdEventManager::registerEventHandler(
    ArdTimerEventHandler *a_timer_event_handler) {
  uint8_t i;
  for (i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (!m_timer_event_handlers[i]) {
      m_timer_event_handlers[i] = a_timer_event_handler;
      setNextTimer();
      return;
    }
  }
  if (i == ArdNetConstants::event_hndlr_slots) {
    error_pr(ARD_F("Event Manager: m_timer_event_handlers full\n"));
    ard_error(ARD_F("Event Manager: m_timer_event_handlers full\n"));
  }
}

void ArdEventManager::removeEventHandler(ArdEventHandler *a_event_handler) {
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (m_event_handlers[i] == a_event_handler) {
      m_event_handlers[i] = nullptr;
    }
  }
}

void ArdEventManager::removeEventHandler(
    ArdTimerEventHandler *a_timer_event_handler) {
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (m_timer_event_handlers[i] == a_timer_event_handler) {
      m_timer_event_handlers[i] = nullptr;
    }
  }
  if (a_timer_event_handler == m_next_timer) {
    setNextTimer();
  }
  setNextTimer();
}

uint8_t ArdEventManager::loopIteration() {
  uint8_t event_count = 0;
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (m_event_handlers[i]) {
      if (m_event_handlers[i]->CheckForEvent()) {
        m_event_handlers[i]->RunEventHandler();
        ++event_count;
      }
    }
  }
  // Handle timers
  while (m_next_timer &&
         m_next_timer->m_abs_deadline < m_ard_sys_int->getMillis()) {
    ++event_count;
    m_next_timer->RunEventHandler();
    setNextTimer();
  }
  return event_count;
}

uint8_t ArdEventManager::availableSlots() {
  uint8_t count = 0;
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (!m_event_handlers[i]) {
      ++count;
    }
  }
  return count;
}

void ArdEventManager::setNextTimer() {
  // This is not the most elegant solution but I don't know how to get the
  // maximum value for an unsigned long in a portable way.
  unsigned long min = 0;
  for (uint8_t i = 0; i < ArdNetConstants::event_hndlr_slots; ++i) {
    if (m_timer_event_handlers[i]) {
      if (min == 0) {
        min = m_timer_event_handlers[i]->m_abs_deadline;
        m_next_timer = m_timer_event_handlers[i];
      } else {
        if (m_timer_event_handlers[i]->m_abs_deadline < min) {
          min = m_timer_event_handlers[i]->m_abs_deadline;
          m_next_timer = m_timer_event_handlers[i];
        }
      }
    }
  }
  // We are making the assumption that there is no wrap-around of the timer
  // counter.  This should definitely be true in our use case, where the
  // Arduinos will be on for short periods of time.
  if (min == 0) {
    m_next_timer = nullptr;
  }
}
