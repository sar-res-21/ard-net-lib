#include "ard_app_switch.h"
#include "ard_app_central_controller.h" // For parsing functions
#include "ard_network_address.h"


///////////////////////////////////////////////////////////////////////
///    Initialisation & contructors/destructors
///////////////////////////////////////////////////////////////////////
AppSwitch::AppSwitch(PktMemPool *a_mem_pool, 
                     ArdSysInterface *a_ard_sys_int, 
                     ArdEventManager *a_event_manager,
                     SendInterface<AppAddr> *a_send_interface,
                     AppAddr a_this_addr)
    : AppBase(a_mem_pool, a_ard_sys_int, a_event_manager,
              a_send_interface, a_this_addr),
      currentDirection(true), m_previous_direction(true),
      m_notify_general_controller(false),
      m_servo_timer(a_event_manager, a_ard_sys_int),
      m_last_requester_addr(0)
{
    m_servo_functor.setParent(this);
}

void AppSwitch::init() {
    info_pr(ARD_F("[AppSwitch] Init (App Addr="), m_this_addr.m_addr, ARD_F(")\n"));

    ArdNetworkLayer::m_ard_sys_int->arPinMode(servoPin, OUTPUT);

    myservo.attach(servoPin);  // Attaches the servo on pin to the servo object
    ArdNetworkLayer::m_ard_sys_int->arPinMode(servoPin, INPUT);

    SetPosition(currentDirection);
}

//////////////////////////////////////////////////////////////////////
//    Radio Rx Processing
//////////////////////////////////////////////////////////////////////
PktBufPtr AppSwitch::processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                   bool *p_result) {
  lib_debug_pr(ARD_F("AppSwitch received, size: "), int(a_p->curr_size),
               ARD_F("\n"));

  bool tst;
  a_p = AppBase::processRxData(ard_move(a_p), a_src_addr, &tst);
  if (tst) {
    *p_result = true;
    return (ard_move(a_p));
  }

  AppMessage appMsg;
  if (!appMsg.analyze(a_p->data, a_p->curr_size) ){
      *p_result = false;
      return (ard_move(a_p));
  }

  m_last_requester_addr = mapAddr<AnyAddr, AppAddr>(a_src_addr, a_src_addr);

  if (appMsg.m_msg_type==APP_TYP_SWITCH_STATUS_REQUEST){
    info_pr(ARD_F("Received Switch Status Request from "), m_last_requester_addr.m_addr,
            ARD_F("\n"));

    sendSwitchStatusMessage(m_last_requester_addr);
    if (m_notify_general_controller)
            sendSwitchStatusMessage(GetAppAddress(APP_DEV_CONTROLLER, 
                                    ArdNetConstants::occ_network_number)); // Inform the central controller
        *p_result = true;
        return (ard_move(a_p));
  }

  if (appMsg.m_msg_type==APP_TYP_SWITCH_CONTROL){
          PktBufPtr sdu = appMsg.deSerialize(ard_move(a_p), m_mem_pool);
          SwitchControlMessage scMsg; 
          scMsg.analyze(sdu->data, sdu->curr_size);

          info_pr(ARD_F("Received Switch Control from "), m_last_requester_addr.m_addr,
                  ARD_F(": toggle="), scMsg.m_toggle, ARD_F(", direction="),
                  scMsg.m_direction, ARD_F("\n"));

          if (scMsg.m_toggle){
              ToggleSwitch();
          } else if (scMsg.m_direction == 0){
              SetPosition(false);
          } else {
                SetPosition(true);
          }
          *p_result = true;
          return (ard_move(a_p));
  }
  *p_result = false;
  return (ard_move(a_p));
}

void AppSwitch::sendSwitchStatusMessage(AppAddr a_dest_addr){
    SwitchStatusMessage ssMsg;
    ssMsg.m_direction = currentDirection;
    PktBufPtr pdu = ssMsg.serialize(m_mem_pool);
    info_pr(ARD_F("Sending Switch Status to "), a_dest_addr.m_addr, ARD_F("\n") );

    send(ard_move(pdu),
        APP_TYP_SWITCH_STATUS, 
        a_dest_addr);          
}


///////////////////////////////////////////////////////////////////////
///    Serial Port Management
///////////////////////////////////////////////////////////////////////
bool AppSwitch::ProcessSerialCmd(unsigned char *msg, size_t msgSize)
{
    if ( AppBase::ProcessSerialCmd(msg, msgSize) )
        return true;

    msg[msgSize] = '\0';
    lib_debug_pr(ARD_F("[AppSwitch]: ProcessSerialCmd\n"));
    lib_debug_pr((char *)msg, ARD_F("\n"));

    switch((char)msg[m_pos_in_cmd_parsing]){
        // Rail Switch
        case 's':
        case 'S':
          ProcessSwitchCmd((char*)msg+m_pos_in_cmd_parsing+1, msgSize-m_pos_in_cmd_parsing-1);
          return true;
          break;

        default:
          info_pr(ARD_F("   Humm! unknown command for "), m_dest, ARD_F("\n"));
          return false;
          break;
    } // END of switch(msg[0])

} // AppSwitch::ProcessSerialCmd


void AppSwitch::ProcessSwitchCmd(char *msg, size_t msgSize)
{
  
    lib_debug_pr(ARD_F("Switch Command Parsing...\n"));
    SwitchControlMessage scMsg; 
    if ( !AppCentralController::ParseSwitchCommand(msg, msgSize, &scMsg) ){
      lib_debug_pr(ARD_F("Switch Command Parsing FAILED: skipping this command\n"));
      return; // Parsing failed (invalid command)
    }
    
    lib_debug_pr(ARD_F("    Toggle: "), scMsg.m_toggle, ARD_F("\n"));
    lib_debug_pr(ARD_F("    Direction: "), scMsg.m_direction, ARD_F("\n"));

    // Toggle?
    if (scMsg.m_toggle){
        ToggleSwitch();
        return;
    }
    // Direction
    if (scMsg.m_direction == 0){
        SetPosition(false);
        return;
    }
    if (scMsg.m_direction == 1){
        SetPosition(true);
        return;
    }

} // AppSwitch::ProcessTrainCmd


///////////////////////////////////////////////////////////////////////
///    Switch Movement
///////////////////////////////////////////////////////////////////////
ServoTimerFunctor::ServoTimerFunctor()
{}

void ServoTimerFunctor::setParent(AppSwitch *a_parent){
      m_parent = a_parent;
}


void ServoTimerFunctor::operator()() {
  lib_debug_pr(ARD_F("Servo Timer called\n"));
  m_parent->StopServo();
}

void AppSwitch::sendStateToControllerIfNeeded(){
    if (m_previous_direction != currentDirection){
        sendSwitchStatusMessage(m_last_requester_addr); // Inform the last requester
        if (m_notify_general_controller)
            sendSwitchStatusMessage(GetAppAddress(APP_DEV_CONTROLLER, 
                                    ArdNetConstants::occ_network_number)); // Inform the central controller
        m_previous_direction = currentDirection;
    }
}

void AppSwitch::SetPosition(bool direction){
    currentDirection = direction;

    info_pr(ARD_F("New Switch position: "), 
            currentDirection, 
            ARD_F("\n") );
    lib_debug_pr(ARD_F("\tStart Servo\n"));

    unsigned char val = direction ? servoPosition_1 : servoPosition_2;
    ArdNetworkLayer::m_ard_sys_int->arPinMode(servoPin, OUTPUT);
    myservo.write(val);

    m_servo_timer.startTimer(500, &m_servo_functor, false);
    
    sendStateToControllerIfNeeded();
}

void AppSwitch::ToggleSwitch(){
    currentDirection = !currentDirection;
    SetPosition(currentDirection);
    info_pr(ARD_F("Switch Toggled. New position: "), 
            currentDirection, 
            ARD_F("\n") );
}


void AppSwitch::StopServo()
{
    lib_debug_pr(ARD_F("\tStop Servo\n"));
    m_servo_timer.stopTimer();
    ArdNetworkLayer::m_ard_sys_int->arPinMode(servoPin, INPUT);  // Stop the servo
}
