/**
 * @file ard_serial_ui.h
 * @author Christophe Couturier
 * @brief Serial port user interface
 *
 */

#ifndef ARD_SERIAL_UI_H
#define ARD_SERIAL_UI_H

#include "ard_event_handler.h"

class ArdSerialEventHandler : public ArdEventHandler {
public:
  ArdSerialEventHandler(ArdSysInterface *a_ard_sys_int,
                        ArdEventManager *a_event_manager);
  virtual ~ArdSerialEventHandler() = default;
  virtual bool CheckForEvent();
  virtual void RunEventHandler();

protected:
  ArdEventManager *m_event_manager;
  ArdSysInterface *m_ard_sys_int;
  static const size_t m_buffer_max_size = 32;
  uint8_t m_rx_buffer[m_buffer_max_size];
  size_t m_rx_buffer_pos;
};

#endif // end include guard ARD_SERIAL_UI_H
