#include "ard_network_layer_one.h"
#include "ard_log.h"
#include "ard_network_layer_base.h"
#include "ard_utils.h"

/********************************************************************
 *                       Layer One                                  *
 ********************************************************************/

ArdNetworkLayerOne::ArdNetworkLayerOne(PktMemPool *a_mpool,
                                       ArdSysInterface *a_ard_sys_int,
                                       ArdEventManager *a_event_manager,
                                       L1Addr a_this_addr)
    : ArdNetworkLayer(a_mpool, a_ard_sys_int, a_event_manager, nullptr,
                      a_this_addr),
      m_timer_func(this, &ArdNetworkLayerOne::sendRequest),
      m_timer_handler(a_event_manager, a_ard_sys_int) {}

void ArdNetworkLayerOne::sendRequest(PktBufPtr a_p, L1Addr a_l1_addr) {
  lib_debug_pr(ARD_F("Layer 1 Send, size: "), int(a_p->curr_size), ARD_F("\n"));
  if (!m_ard_sys_int) {
    ard_error(ARD_F("Layer 1 sendRequest m_sys_interface is null\n "));
  }

  int retry = 5;
  while (!m_ard_sys_int->radioCanSend() && retry > 0) {
    m_ard_sys_int->arDelay(1);
    m_ard_sys_int->radioRecvDone();
    retry--;
  }
  // if (m_ard_sys_int->radioCanSend()) {
  if (retry != 0) {
    m_ard_sys_int->radioSendStart(0, a_p->data, a_p->curr_size);
    lib_debug_pr(ARD_F("Layer 1: frame sent!*****************\n"));
    m_ard_sys_int->radioSendWait(0);
    if (m_upper_layer) {
      m_upper_layer->dataHandlingDone(ard_move(a_p), true);
    } else {
      ard_error(ARD_F("Layer1: cannot call upper layer\n"));
    }
  } else {
    lib_debug_pr(ARD_F("Layer 1: device busy\n"));
    m_timer_func.setPktBuff(ard_move(a_p), a_l1_addr);
    m_timer_handler.startTimer(10, &m_timer_func);
  }
}

void ArdNetworkLayerOne::retrieveRadioFrame() {
  PktBufPtr a_p = m_mem_pool->AllocateSlot();
  for (uint8_t i = 0; i < rf12_len; ++i)
    a_p->data[i] = rf12_data[i];
  a_p->curr_size = rf12_len;

  m_ard_sys_int->radioRecvDone();

  lib_debug_pr(ARD_F("L1 Receive called with "), int(a_p->curr_size),
               ARD_F("  bytes+++++++++++++++++++++++++++++++++++\n"));

  if (m_upper_layer) {
    lib_debug_pr(ARD_F("L2 passing message to upper layer\n"));
    // lib_debug_pr(ARD_F("a_p->data[0]: "), a_p->data[0], ARD_F("\n"));
    m_upper_layer->onDataReceived(ard_move(a_p), AnyAddr(m_this_addr), m_l_id);
  }
}