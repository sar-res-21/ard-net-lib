#ifndef GPIO_INTERRUPT_H
#define GPIO_INTERRUPT_H


class GpioFunctor {
public:
  virtual void operator()() = 0;
};

class GpioInterrupt {
public:
  enum class TriggerMode { Change, Falling, Rising };

  GpioInterrupt();
  ~GpioInterrupt();

  void init(
      int a_pin, TriggerMode a_trig_mode, 
      GpioFunctor* a_callback_functor);

  void _isr(); ///< Interrupt sub routine. Should not be called. The only reason
               ///< for it to be public is that we need to call it from the
               ///< common interrupt handlers

  unsigned long m_debouncing_interval_in_ms = 50;

protected:
  int m_ioPin;
  GpioFunctor* m_callback_functor;

  static int TriggerMode2EnableInterrupt(TriggerMode);
  unsigned long m_last_call_ts;

};

#endif // GPIO_INTERRUPT_H
