/**
 * @file ard_address.h
 * @brief Simple template for generic network addresses.
 *
 */

#ifndef ARD_ADDRESS_H
#define ARD_ADDRESS_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else
#include <cstdint> // for uint8_t
#endif

#include "ard_log.h"
#include "ard_error.h"


/**
 * @brief Simple template for a generic (numerical) network address of layer 2
 * or 3.
 *
 * For the sake of consistency, the radio module (Layer 1) offers the same
 * inteface as the other layers.  It needs, therefore, its own address type.  As
 * there are no actual addresses at layer 1, they are defined as an empty
 * structure, so that they do not consume memory.
 *
 * @tparam T The address type (e.g., uint8_t, uint16_t)
 * @tparam Parameter a "phantom" parameter so that we can define incompatible
 * addresses of the same underlying type (see
 * https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/ for
 * more details)
 */
template <typename T, typename Parameter = struct Phantom> struct ArdAddress {
  /**
   * @brief Builds an address (by default with value 0).
   * @param a_addr The numerical value of the address.
   */
  explicit ArdAddress(T a_addr = 0) : m_addr(a_addr), m_size(sizeof(T)){};
  /**
   * @brief Builds the binary representation of the address in the buffer
   * pointed by a_buf.
   *
   * @param a_buf Thee pointer pointing to the where to build the binary
   * representation.
   */
  void serialize(uint8_t *a_bf);
  /**
   * @brief Converts the binary representation of the address to its
   * corresponding value.
   *
   * @param a_buf A pointer to the first byte of the binary representation.
   */
  void deSerialize(uint8_t *a_buf);
  /**
   * @brief Compares two addresses.
   *
   * @param a1 The first address.
   * @param a2 The second address.
   * @return True if the two addresses are equal, false otherwise.
   */
  friend bool operator==(ArdAddress<T, Parameter> const &a1,
                         ArdAddress<T, Parameter> const &a2) {
    return (a1.m_addr == a2.m_addr);
  }
  T m_addr;
  uint8_t m_size;
};

template <typename T, typename Parameter>
void ArdAddress<T, Parameter>::serialize(uint8_t *a_buf) {
  if (m_size == 0)
    return;
  uint8_t j = m_size - 1;
  for (uint8_t i = 0; i < m_size; ++i) {
    a_buf[j] = (m_addr & (0xff << (8 * i))) >> (8 * i);
    j--;
  }
}

template <typename T, typename Parameter>
void ArdAddress<T, Parameter>::deSerialize(uint8_t *a_buf) {
  if (m_size == 0)
    return;
  m_addr = 0;
  uint8_t j = m_size - 1;
  for (uint8_t i = 0; i < m_size; ++i) {
    m_addr |= a_buf[j] << (8 * i);
    j--;
  }
}

struct EmptyAddr {};

inline bool operator==(EmptyAddr const &, EmptyAddr const &) { return true; }

using L2Addr = ArdAddress<uint8_t, struct L2Phantom>;
using L3Addr = ArdAddress<uint16_t, struct L3Phantom>;
using AppAddr = ArdAddress<uint8_t, struct AppPhantom>;

using L1Addr = EmptyAddr;

/********************* Addr Union *******************************/
// FIXME: write the docstrings
enum class AddrType { App, L3, L2, L1 };
struct AnyAddr {
  AddrType m_addr_type;
  union {
    AppAddr m_app_addr;
    L3Addr m_l3_addr;
    L2Addr m_l2_addr;
    L1Addr m_l1_addr;
  };
  explicit AnyAddr(AppAddr a_app_addr)
      : m_addr_type(AddrType::App), m_app_addr(a_app_addr) {}
  explicit AnyAddr(L3Addr a_l3_addr)
      : m_addr_type(AddrType::L3), m_l3_addr(a_l3_addr) {}
  explicit AnyAddr(L2Addr a_l2_addr)
      : m_addr_type(AddrType::L2), m_l2_addr(a_l2_addr) {}
  explicit AnyAddr(L1Addr a_l1_addr)
      : m_addr_type(AddrType::L1), m_l1_addr(a_l1_addr) {}
};

bool operator==(const AnyAddr& a1, const AnyAddr& a2);

// Not sure if this is a good idea but I don't have a better one for now.  We
// use template specializations to implement the supported translations.  The
// compiler should be able to pick the right one each time.

// Note that we must use templates because simple function overloading does
// not allow to override functions that differ only in the return type.

template <typename U, typename L> L mapAddr(U a_addr, U a_this_addr) {
  ard_error(ARD_F("Unsupported address translation\n"));
  return L();
}

// We must declare these specializations here otherwise the compiler does not
// find them if they are only in the .cpp file.  As they are full
// specializations we cannot define them in a header file.
template <>
L3Addr mapAddr<AppAddr, L3Addr>(AppAddr a_addr, AppAddr a_this_addr);
template <> 
AppAddr mapAddr<L3Addr, AppAddr>(L3Addr a_addr, L3Addr a_this_addr);
template <> L2Addr mapAddr<L3Addr, L2Addr>(L3Addr a_addr, L3Addr a_this_addr);
template <>
L2Addr mapAddr<AppAddr, L2Addr>(AppAddr a_addr, AppAddr a_this_addr);
template <> L1Addr mapAddr<L3Addr, L1Addr>(L3Addr a_addr, L3Addr a_this_addr);
template <> L1Addr mapAddr<L2Addr, L1Addr>(L2Addr a_addr, L2Addr a_this_addr);


template <> AppAddr mapAddr<AnyAddr, AppAddr>(AnyAddr a_addr, AnyAddr m_this_addr);
template <> AppAddr mapAddr<L2Addr, AppAddr>(L2Addr a_addr, L2Addr a_this_addr);

// FIXME: turn these enums into enum class...
enum AppDevices {
  APP_DEV_CONTROLLER = 0x00,
  APP_DEV_TRAIN = 0x01,
  APP_DEV_SWITCH = 0x08,
  APP_DEV_OCC = 0x0D,
  APP_DEV_GATEWAY = 0x0E,
  APP_DEV_BCAST = 0x0F
};
enum L2DevicesAdd {
  L2_DEV_CONTROLLER = APP_DEV_CONTROLLER,
  L2_DEV_TRAIN = APP_DEV_TRAIN,
  L2_DEV_SWITCH = APP_DEV_TRAIN,
  L2_DEV_GATEWAY = APP_DEV_GATEWAY,
  L2_DEV_BCAST = APP_DEV_BCAST
};

namespace ArdNetConstants {
  constexpr uint8_t occ_network_number = 0xFE;     /**< The address of the OCC network */
}

// FIXME: move the corresponding code to the send interfaces.
L3Addr GetL3Address(AppDevices a_dev, uint8_t a_network);
uint8_t GetNetwork(L3Addr a_l3_add);
L2Addr GetL2Address(L3Addr a_l3_add, uint8_t a_local_network_number);
L2Addr GetL2Address(AppDevices a_dev, uint8_t a_network);
L2Addr GetL2Address(L3Addr a_l3_add);


AppAddr GetAppAddress(uint8_t a_dev, uint8_t a_network);
uint8_t GetNetAddress(AppAddr a_app_addr);
uint8_t GetDevAddress(AppAddr a_app_addr);

#endif // ARD_ADDRESS_H
