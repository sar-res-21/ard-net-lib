/**
 * @file ard_app_switch.h
 * @author Christophe Couturier
 * @brief Class for the application of the switch
 *
 */

#ifndef ARD_APP_SWITCH_H
#define ARD_APP_SWITCH_H

#include "ard_app_base.h"
#include "ard_network_application_message.h"
#include "ard_sys_interface.h"

#ifdef ARDUINO_AVR_NANO
#include <Servo.h>
#endif

class AppSwitch;

class ServoTimerFunctor : public TimerFunctor {
friend class AppSwitch;
public:
  ServoTimerFunctor();

  void operator()();
  void setParent(AppSwitch *a_parent);

protected:
  AppSwitch *m_parent;
};



class AppSwitch: public AppBase {
friend class ServoTimerFunctor;
public:
    AppSwitch(PktMemPool *a_mem_pool, 
              ArdSysInterface *a_ard_sys_int, 
              ArdEventManager *a_event_manager,
              SendInterface<AppAddr> *a_send_interface,
              AppAddr a_this_addr);
    virtual void init();  /// Call this from the setup() function

    int servoPin = 5;   // DIO2 which is PWM compatible
    unsigned char servoPosition_1 = 55;   // Empirical values for end of course
    unsigned char servoPosition_2 = 110;  // Empirical values for end of course
    bool m_notify_general_controller;     // Send a notification to the general controller (in network 254) if state change (default=false)

protected:
    Servo myservo;
    bool currentDirection;
    bool m_previous_direction;


// Serial Port UI Processing
protected:
    virtual bool ProcessSerialCmd(unsigned char *msg, size_t msgSize);
    void ProcessSwitchCmd(char *msg, size_t msgSize);

    // Rx Radio Message Processing
    virtual PktBufPtr processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                    bool *p_result);
    void sendSwitchStatusMessage(AppAddr a_dest_addr);
    AppAddr m_last_requester_addr;

// Servo Control Functions
public:
    void SetPosition(bool direction);
    void ToggleSwitch();
protected:
    void StopServo();
    void sendStateToControllerIfNeeded();

    ArdTimerEventHandler m_servo_timer;    
    ServoTimerFunctor m_servo_functor;   
};

#endif // end include guard ARD_APP_SWITCH_H
