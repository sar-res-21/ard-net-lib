
#ifndef ARD_NETWORK_LAYER_BASE_H
#define ARD_NETWORK_LAYER_BASE_H

/** \addtogroup most_important
 *  @{
 */

#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_sys_interface.h"
#include "ard_utils.h"

class ArdEventManager;

using LayerId = uint8_t;

/**
 * @brief Class representing the "South" interface of a network layer
 * (receiving data).
 *
 * Network layers that need to use a "lower layer" should inherit from this
 * class in order to expose the onDataReceived and sendCompleted methods.
 */
class ArdNetSouthInterface {
public:
  /**
   * @brief This method is called (by the lower layer) whenever there is new
   * data for this network layer.
   *
   * The source address type is AnyAddr so that it is possible to have
   * arbitrary network stacks.
   *
   * @param a_p pointer to the packet buffer containing the message that was
   * received.
   * @param a_src_addr the (lower layer) address of the sender.  This is of
   * type AnyAddr.
   * @param a_l_id the identifier of the lower layer that is making the call
   * (i.e., the one that received the message).  This is needed only for
   * routing, when a single layer three sits on top of two different layer
   * twos (one for each interface of the router).
   */
  virtual void onDataReceived(PktBufPtr a_p, AnyAddr a_src_addr,
                              LayerId a_l_id) = 0;
  /**
   * @brief The lower layer calls this method to return ownership of the
   * packet buffer.
   *
   * The lower layer calls this method when it has completed processing the
   * message.  Typically this happens after the lower layer has copied the
   * data to a new packet buffer that contains the lower layer header
   * followed by the payload (i.e., the message of the upper layer).
   *
   * When a sender calls sendRequest it must give (with arrd_move) a packet
   * buffer pointer, relinquishing ownership of that data.  In certain
   * cases, for example if the sender would like to store the message in
   * order to send it again at a leter time, it is useful to "get back the
   * token" from the lower layer.  That is for the lower layer to give back
   * the packet buffer pointer that owns the data of the message.
   *
   * @note
   * It is wrong to assume that the lower layer calls this method after it
   * has sent the packet.  The lower layer calls this method when it does
   * not need to access the packet buffer (i.e., the payload from the point
   * of view of the lower layer).
   *
   * @param a_p
   * @param res
   */
  virtual void dataHandlingDone(PktBufPtr a_p, bool res) = 0;
};

/**
 * @brief Template class representing the "north" interface of a network layer.
 *
 * Network layers and applications that need to use a "lower layer" should
 * inherit from this class in order to expose the sendRequest method.
 *
 * @note As the constructor sets to nullptr the pointer pointing to the
 * corresponding South interface, it is imperative to call
 * setUpperLayer before calling sendRequest.
 *
 * @tparam T  The type of address used in sendRequest.  Typically this is the
 * address of the lower layer, i.e., the one inheriting from
 * ArdNetNorthInterface.
 */
template <typename T> class ArdNetNorthInterface {
public:
  /**
   * @brief Default constructor.
   *
   * @note As the constructor sets to nullptr the pointer pointing to the
   * corresponding South interface, it is imperative to call
   * setUpperLayer before calling sendRequest.
   */
  ArdNetNorthInterface() : m_upper_layer(nullptr) {}
  /**
   * @brief The upper layer (or application) must call this method to send a
   * packet.
   *
   * @param a_p A unique pointer to the buffer containing the data to send
   * (i.e., the payload).
   * @param a_dst_addr The destination address of this packet.
   */
  virtual void sendRequest(PktBufPtr a_p, T a_dst_addr) = 0;
  /**
   * @brief Bind this interface with the corresponding South interface of the
   * "upper layer" protocol (or application).
   *
   * @param a_south_int The pointer to the South interface instance to bind to
   *
   */
  virtual void setUpperLayer(ArdNetSouthInterface *a_south_int) {
    m_upper_layer = a_south_int;
  }

protected:
  ArdNetSouthInterface *m_upper_layer;
};

/**
 * @brief Abstract Base Class Template of the Send Interface.
 *
 * Network layers that send data using a lower layer must construct
 * (instantiate) the appropriate SendInterfaceImplementation and then pass
 * a pointer to it to the constructor of the network layer, which
 * mush inherit from ArdNetworkLayer.
 *
 * Technical explanation:
 * As we want to be able to build arbitrary protocol stacks without having to
 * modify each protocol implementation, we use the policy design pattern to
 * implement address translation from one layer to another.  The problem is
 * the following: a network layer (A) knows only its own address type, it does
 * not know the address type of the other layers.  Therefore if we decide to
 * connect layer A on top of layer B, so that A will use B to send data, we
 * need to "translate" the addresses from A to B.  As layer A only knows
 * addresses of type A and layer B only those of type B, A knows the address
 * of type A of the destination.  But we need to give to layer B the address
 * of type B of the destination.
 *
 * The idea of the policy based pattern is that we have different
 * implementations of the same policy (translating addresses in our case).
 * When building a network layer object, we also instantiate (and build) the
 * corresponding address translation policy and pass it to the network layer.
 *
 * Finally, we want each network layer to be completely independent of all
 * other layers.  This way we can create arbitrary protocol stacks, for
 * example we can place the application layer directly on top of layer two or
 * on top of layer three, which is then on top ol layer two.  All this
 * without changing the code of each layer but only the code the "builds and
 * connects the layers." (This code is usually in the main function.)  Because of
 * this, a network layer must not know the type of the addresses of the lower
 * layer, hence the existence of the SendInterface, which only depends on the
 * address type of the protocol itself, and the SendInterfaceImplementation,
 * which does depend on address type of the lower layer as well.
 *
 * With this solution, whenever a layer has data to send, it calls the
 * sendRequest method of the SendInterfaceImplementation, which inherits from
 * the SendInterface, and provides the payload (a packet buffer pointer) and
 * the address of the destination of the same type as the layer.  The
 * sendRequest of the SendInterfaceImplementation calls the mapAddr template
 * function to find the lower layer address of the destination and then calls
 * the sendRequest method of the lower layer.
 *
 * @tparam U
 */
template <typename U> class SendInterface {
public:
  /**
   * @brief Ask the lower layer to send data to the a_dst_addrr address
   *
   * @param a_p packet buffer pointer pointing to the payload to send
   * @param a_dst_addr the destination address
   */
  virtual void sendRequest(PktBufPtr a_p, U a_dst_addr) = 0;
};

/**
 * @brief Implementation of the sendInterface
 *
 * Implements the sendInterface by adding the dependency on the lower layer
 * address type.  Therefore this template must change whenever the protocol
 * stacks changes.  For example, if we want to connect the application layer
 * directly only top of layer two, we would create an instance of the
 * sendInterface with sendInterface<AppAddr, L2Addr>, while if we want to
 * connect the application layer to Layer Three, we would crate an instance
 * of type sendInterface<AppAddr, L3Addr>.
 *
 * @tparam U the address type of the upper layer
 * @tparam L the address type of the lower layer
 */
template <typename U, typename L>
class SendInterfaceImplementation : public SendInterface<U> {
public:
  /**
   * @brief Constructs the SendInterfaceImplementation
   *
   * We need the local address to be able to do the mapping between the
   * different layers.
   *
   * @param a_this_addr the local address of the upper layer.
   */
  explicit SendInterfaceImplementation(U a_this_addr)
      : m_lower_layer(nullptr), m_this_address(a_this_addr) {}
  /**
   * @brief Binds the sendInterface to the lower layer.
   *
   * @note
   * It is imperative to call this function before sending any data,
   * otherwise the sendRequest method will not work.
   *
   * @param a_lower_layer a pointer to the lower layer.
   */
  void setLowerLayer(ArdNetNorthInterface<L> *a_lower_layer) {
    m_lower_layer = a_lower_layer;
  }
  /**
   * @brief Sends a message to the a_dest_address (same address type as the
   * "upper layer").
   *
   * This method calls the mapAddr function to find the lower layer address
   * corresponding to the upper layer destination address.  Then it calls
   * the sendRequest method of the lower layer to send the message.
   *
   * @param a_p pointer to the packet buffer containing the message.
   * @param a_dst_addr the destination address.
   */
  void sendRequest(PktBufPtr a_p, U a_dst_addr) override {
    L lower_addr = mapAddr<U, L>(a_dst_addr, m_this_address);
    if (m_lower_layer) {
      m_lower_layer->sendRequest(ard_move(a_p), lower_addr);
    } else {
      ard_error(ARD_F("sendRequest called but lower layer not set\n"));
    }
  }

private:
  ArdNetNorthInterface<L> *m_lower_layer;
  U m_this_address;
};

/**
 * @brief Base class for all the network layers.
 *
 * All network layers should inherit from this class, as well as
 * ArdNetNorthInterface if they have an "upper layer" and/or
 * ArdNetSouthInterface if they have a "lower layer."
 *
 * Classes representing applications should not inherit from this class but
 * rather from ArdSouthIntefrace.
 *
 * @tparam T The address type of this layer.
 */

template <typename T> class ArdNetworkLayer {
public:
  /**
   * @brief Constructs the base network layer class by setting the memory
   * pool, system interface, send interface, and  the local address.
   *
   * We need the memory pool in order to allocate slots to build the messages
   * of this layer.
   *
   * The memory pool can be shared by different layers (and/or different
   * instances of the same layer).  There is no need to allocate one for each
   * layer but this is a bad idea.  It is better to  allocate a larger pool and
   * share it among multiple layers in order to maximize resource usage.
   *
   * We need a pointer to the system interface object in case we need to
   * create a timer.  As most, even if not all, layers need to create a
   * timer, I decided to add the parameter to the constructor of the base
   * class.
   *
   * We need a reference to the event manager for the timers as well and we also
   * needed in the layer one, in order to check if a new frame has arrived.
   *
   * @param a_mem_pool A pointer to the memory pool where to allocate packet
   * buffers.
   * @param a_event_manager A pointer to the event manager.
   * @param a_sys_int A pointer to the system interface object.
   * @param a_send_interface A pointer to the send interface that will be
   * used when sending packets.
   * @param a_this_addr The address of this network layer instance.
   *
   */
  explicit ArdNetworkLayer(PktMemPool *a_mem_pool,
                           ArdSysInterface *a_ard_sys_int,
                           ArdEventManager *a_event_manager,
                           SendInterface<T> *a_send_interface, T a_this_addr)
      : m_mem_pool(a_mem_pool), m_ard_sys_int(a_ard_sys_int),
        m_event_manager(a_event_manager), m_send_interface(a_send_interface),
        m_this_addr(a_this_addr), m_l_id(0) {}

protected:
  PktMemPool *m_mem_pool;
  ArdSysInterface *m_ard_sys_int;
  ArdEventManager *m_event_manager;
  SendInterface<T> *m_send_interface;
  uint8_t m_l_id;
  T m_this_addr;
};

/** @}*/

/**
 * @brief Symbolic names for the left and right interfaces in a routing layer.
 */
enum class RoutingInterfaceName { left, right, none };


template <typename T> class ArdNetworkRoutingLayer {
public:
  /**
   * @brief Constructs the base network routing class by setting the memory
   * pool, system interface, send interfaces, and local addresses.
   *
   * For the sake of simplicity (and uniformity) the network layer stores a
   * pointer to the memory pool and another to the system interface, even
   * though not all routing layers will need them.
   *
   * The routing layer supports only two interfaces ("left" and "right") so
   * that it is possible to allocate everything statically
   *
   * @param a_mem_pool A pointer to the memory pool where to allocate packet
   * buffers.
   * @param a_event_manager A pointer to the event manager.
   * @param a_sys_int A pointer to the system interface object.
   * @param a_send_interface_left A pointer to the send interface for the
   * left interface.
   * @param a_send_interface_right A pointer to the send interface for the
   * right interface.
   * @param a_this_addr_left The address of this network layer instance on
   * the left interface.
   * @param a_this_addr_right The address of this network layer instance on
   * the right interface.
   *
   */
  explicit ArdNetworkRoutingLayer(PktMemPool *a_mem_pool,
                                  ArdSysInterface *a_ard_sys_int,
                                  ArdEventManager *a_event_manager,
                                  SendInterface<T> *a_send_interface_left,
                                  SendInterface<T> *a_send_interface_right,
                                  T a_this_addr_left, T a_this_addr_right)
      : m_mem_pool(a_mem_pool), m_ard_sys_int(a_ard_sys_int),
        m_event_manager(a_event_manager),
        m_send_interface_left(a_send_interface_left),
        m_send_interface_right(a_send_interface_right),
        m_this_addr_left(a_this_addr_left),
        m_this_addr_right(a_this_addr_right), m_l_id(0) {}

protected:
  PktMemPool *m_mem_pool;
  ArdSysInterface *m_ard_sys_int;
  ArdEventManager *m_event_manager;
  SendInterface<T> *m_send_interface_left;
  SendInterface<T> *m_send_interface_right;
  uint8_t m_l_id;
  T m_this_addr_left;
  T m_this_addr_right;
};

#endif // ARD_NETWORK_LAYER_BASE_H
