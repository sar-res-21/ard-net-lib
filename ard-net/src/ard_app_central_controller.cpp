#include "ard_app_central_controller.h"

#ifndef ARDUINO_AVR_NANO
#include <stdio.h>
#include <string.h>
#endif

///////////////////////////////////////////////////////////////////////
///    Initialisation & contructors/destructors
///////////////////////////////////////////////////////////////////////
AppCentralController::AppCentralController(
                 PktMemPool *a_mem_pool, 
                 ArdSysInterface *a_ard_sys_int, 
                 ArdEventManager *a_event_manager,
                 SendInterface<AppAddr> *a_send_interface,
                 AppAddr a_this_addr)
    : AppBase(a_mem_pool, a_ard_sys_int, a_event_manager, 
              a_send_interface, a_this_addr)
{
    // m_local_add = GetL3Address(APP_DEV_CONTROLLER, a_local_network_add);
}

void AppCentralController::init() {
    info_pr(ARD_F("[AppCentralController] Init (App Addr="), 
            m_this_addr.m_addr, ARD_F(")\n"));
}

//////////////////////////////////////////////////////////////////////
//    Radio Rx Processing
//////////////////////////////////////////////////////////////////////
PktBufPtr AppCentralController::processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                              bool *p_result) {
  lib_debug_pr(ARD_F("AppCentralController received, size: "),
               int(a_p->curr_size), 
               ARD_F("\n"));
  // for (uint8_t i=0; i<a_p->curr_size; i++)
  //   lib_debug_pr(a_p->data[i], ARD_F(" "));
  // lib_debug_pr(ARD_F("\n"));

  bool tst;
  a_p = AppBase::processRxData(ard_move(a_p), a_src_addr, &tst);
  if (tst) {
    *p_result = true;
      return (ard_move(a_p));
  } 

  AppAddr src_app_add = mapAddr<AnyAddr, AppAddr>(a_src_addr, a_src_addr);
  AppMessage appMsg;
  appMsg.analyze(a_p->data, a_p->curr_size);

  PktBufPtr sdu;
  
  if (appMsg.m_msg_type==APP_TYP_SWITCH_STATUS){
      sdu = appMsg.deSerialize(ard_move(a_p), m_mem_pool);
      SwitchStatusMessage ssMsg; 
      ssMsg.analyze(sdu->data, sdu->curr_size);
      info_pr(ARD_F("Received Switch Status from "), 
              src_app_add.m_addr,
              ARD_F(", direction="), ssMsg.m_direction, ARD_F("\n"));
      *p_result = true;
      return (ard_move(a_p));
  }

  if (appMsg.m_msg_type==APP_TYP_TRAIN_STATUS){ 
      sdu = appMsg.deSerialize(ard_move(a_p), m_mem_pool);
      TrainStatusMessage tsMsg; 
      tsMsg.analyze(sdu->data, sdu->curr_size);
      info_pr(ARD_F("Received Train Status from "), 
              src_app_add.m_addr,
              ARD_F(", direction="), tsMsg.m_direction, ARD_F(", speed="),
              tsMsg.m_speed, ARD_F("\n"));
      *p_result = true;
      return (ard_move(a_p));
  }

  *p_result = false;
  return (ard_move(a_p));
}

void AppCentralController::sendSwitchStatusRequest(){
    SwitchStatusRequestMessage ssrMsg;
    PktBufPtr pdu = ssrMsg.serialize(m_mem_pool);
    send(ard_move(pdu),
        APP_TYP_SWITCH_STATUS_REQUEST, 
        GetAppAddress(APP_DEV_SWITCH, m_dest)
        );          
}

void AppCentralController::sendTrainStatusRequest(){
    TrainStatusRequestMessage tsrMsg;
    PktBufPtr pdu = tsrMsg.serialize(m_mem_pool);
    send(ard_move(pdu),
        APP_TYP_TRAIN_STATUS_REQUEST, 
        GetAppAddress(APP_DEV_TRAIN, m_dest)
        );          
}

///////////////////////////////////////////////////////////////////////
///    Serial Port Management
///////////////////////////////////////////////////////////////////////
bool AppCentralController::ProcessSerialCmd(unsigned char *msg, size_t msgSize)
{
    if ( AppBase::ProcessSerialCmd(msg, msgSize) )
        return true;

    msg[msgSize] = '\0';
    // lib_debug_pr(ARD_F("[AppCentralController]: ProcessSerialCmd"));
    // lib_debug_pr((char *)msg);

    switch((char)msg[m_pos_in_cmd_parsing]){
        // Train movement
        case 't':
        case 'T':
          lib_debug_pr(ARD_F("   Ok, this is a train command for "), m_dest, ARD_F("\n")); 
          ProcessTrainCmd((char*)msg+m_pos_in_cmd_parsing+1, msgSize-m_pos_in_cmd_parsing-1);
          return true;
          break;

        // Rail Switch
        case 's':
        case 'S':
          lib_debug_pr(ARD_F("   Ok, this is a switch command for "), m_dest, ARD_F("\n"));
          ProcessSwitchCmd((char*)msg+m_pos_in_cmd_parsing+1, msgSize-m_pos_in_cmd_parsing-1);
          return true;
          break;

        // Request Status (Rail or Switch)
        case 'r':
        case 'R':
          lib_debug_pr(ARD_F("   Ok, this is a Status Request for "), m_dest, ARD_F("\n"));
          ProcessRequest((char*)msg+m_pos_in_cmd_parsing+1, msgSize-m_pos_in_cmd_parsing-1);
          return true;
          break;

        default:
          info_pr(ARD_F("   Humm! unknown command for "), m_dest, ARD_F("\n"));
          return false;
          break;
    } // END of switch(msg[0])

} // AppCentralController::ProcessSerialCmd


/////////////////////////////////////////////
///    Train Command
void AppCentralController::ProcessTrainCmd(char *msg, size_t msgSize)
{
    TrainControlMessage tcMsg; 
    if ( !ParseTrainCommand(msg, msgSize, &tcMsg) ){
      lib_debug_pr(ARD_F("Train Command Parsing FAILED: skipping this command\n"));
      return; // Parsing failed (invalid command)
    }
    
    lib_debug_pr(ARD_F("    train direction: "), tcMsg.m_direction , ARD_F("\n"));
    lib_debug_pr(ARD_F("    train duration: "), tcMsg.m_duration , ARD_F("\n"));
    lib_debug_pr(ARD_F("    train speed: "), tcMsg.m_speed , ARD_F("\n"));

    PktBufPtr buff = tcMsg.serialize(m_mem_pool);
    send(ard_move(buff), 
         APP_TYP_TRAIN_CONTROL,
         GetAppAddress(APP_DEV_TRAIN, m_dest) ); // 1: Train 
        

} // CentralControllerApp::ProcessTrainCmd

bool AppCentralController::ParseTrainCommand(
      char *msg, size_t msgSize, TrainControlMessage *tc)
{
    // Note: the message is supposed to be a null terminated string...

    // Line is structured as this (1st caracter is a 't or 'T'
    // and is supposed to have been removed):
    //   f|F|b|B|s|S [duration] [speed]


    // Default parameter value
    tc->m_direction = TRAIN_DIR_STOP;
    tc->m_duration = 0;             
    tc->m_speed = 255;       

    if( msgSize<1 ){
        return false; // no  command
    }

    // Parse 1st parm: F (forward) /  B (backward) / S (Stop)
    switch( msg[0] ){
        case 'f':
        case 'F':
          tc->m_direction = TRAIN_DIR_FW;
          break;
        case 'b':
        case 'B':
          tc->m_direction = TRAIN_DIR_BW;
          break;
        case 's':
        case 'S':
          tc->m_direction = TRAIN_DIR_STOP;
          tc->m_speed = 0;
          return true; // parsing finished

        default:
          return false; // no valid command
    } // END of switch(msg[0])


    // Goto next param
    msg++;  // remove at least the 1st param
    while( (strlen(msg)!=0)  && msg[0]==' ' ) msg++;
    if( strlen(msg)<1 ) return true; // no more params but command ok

    // Parse Duration
    int test, duration, speed;
    test = sscanf(msg, "%d %d", &duration, &speed);
    if( test == 0 ) return true; // no valid duration but command ok
    if( test >= 1 ) tc->m_duration = (uint16_t)duration;
    if( test >= 2 ) tc->m_speed = (uint8_t)speed;

    // The End
    return true;
} // ParseTrainCommand


/////////////////////////////////////////////
///    Switch Command
void AppCentralController::ProcessSwitchCmd(char *msg, size_t msgSize)
{
    SwitchControlMessage scMsg; 
    if ( !ParseSwitchCommand(msg, msgSize, &scMsg) ){
      lib_debug_pr(ARD_F("Switch Command Parsing FAILED: skipping this command\n"));
      return; // Parsing failed (invalid command)
    }
    
    info_pr(ARD_F("Sending Switch Command to "), m_dest,
            ARD_F(", Toggle="), scMsg.m_toggle,
            ARD_F(", Direction="), scMsg.m_direction,
            ARD_F("\n"));

    PktBufPtr buff = scMsg.serialize(m_mem_pool);
        send(ard_move(buff), 
         APP_TYP_SWITCH_CONTROL,
         GetAppAddress(APP_DEV_SWITCH, m_dest) );

} // CentralControllerApp::ProcessTrainCmd

bool AppCentralController::ParseSwitchCommand(
      char *msg, size_t msgSize, SwitchControlMessage *sc)
{
    sc->m_toggle = 0;
    sc->m_direction = 0;

    if( msgSize<2 )
      return false; // no  command

    switch((char)msg[0]){
        // Toggle
        case 't':
        case 'T':
          sc->m_toggle = 1;
          break;
        // Left
        case '0':
          sc->m_toggle = 0;
          sc->m_direction = 0;
          break;
        // Right
        case '1':
          sc->m_toggle = 0;
          sc->m_direction = 1;
          break;

        default:
          return false; // no valid command          
    } // END of switch(msg[0])

    // The End
    return true;
} // ParseSwitchCommand


/////////////////////////////////////////////
///    Train or Switch Status Request Command
void AppCentralController::ProcessRequest(char *msg, size_t msgSize)
{
    if( msgSize<1 )
      return; // no command

    switch((char)msg[0]){
        // Train
        case 't':
        case 'T':
          sendTrainStatusRequest();
          break;

        // Switch
        case 's':
        case 'S':
          sendSwitchStatusRequest();
          break;

        default:
          return; // no valid command          
    } // END of switch(msg[0])

} // ProcessRequest
