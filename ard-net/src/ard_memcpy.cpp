#include "ard_memcpy.h"


#ifdef ARDUINO_AVR_NANO
void* ard_memcpy( void* dest, const void* src, size_t count ){
  return memcpy(dest, src, count);
}
#else
void* ard_memcpy( void* dest, const void* src, size_t count ){
  return memcpy(dest, src, count);
}
#endif