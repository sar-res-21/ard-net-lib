/**
 * @file ard_log.h
 * @brief Very simple logging solution using variadic templates and #defines to
 * disable it at compile time.
 *
 * Hopefully this will enable us to save some memory, which is a very precious
 * resource on an Arduino :(.
 */

#ifndef ARD_LOG_H
#define ARD_LOG_H

#include "ard_iostream.h"

#ifndef ARDUINO_AVR_NANO
// Define a dummy "F macro" to mimic the Arduino F macro
#define ARD_F(argument) argument
// Turn on all debug messages for native builds
#define DEBUG_TRUE
#define ERROR_TRUE
#define LIBRARY_DEBUG_TRUE
#else
#define ARD_F(argument) F(argument)
#endif

// For the time being we define here the different preprocessor macros.  We
// should find a more elegant solution though.
//    --> Now it has been move in platformio.ini
// #define DEBUG_TRUE
// #define ERROR_TRUE
// #define LIBRARY_DEBUG_TRUE

inline void info_pr(){};

/**
 * @brief Simple template function that can be used to print information
 * messages.
 *
 * It takes an arbitrary number of parameters and it prints them in order.
 *
 * These messages are always printed.  debug_pr and lib_debug_pr can be used to
 * show debug messages.
 *
 * @note By default, it prints uint8_t variables as ASCII characters.  Cast
 * these variables to int in order to get readable output.  Example:
 * @code
 *
 * uint8_t i; info_pr(ARD_F("The value of i is: "), int(i), ARD_F("\n"));
 *
 * @endcode
 *
 * The example uses the ARD_F macro to encapsulate strings.  It is important to
 * always do this in order to save memory on the Arduino.
 *
 * @note These messages are always printed.  Use sparingly.  Use debug_pr() to
 * print messages only if DEBUG_TRUE is defined.
 *
 */
template <typename T, typename... Types>
inline void info_pr(T var1, Types... var2) {
  (*ard_out_stream) << var1;
  info_pr(var2...);
}

#ifdef DEBUG_TRUE

inline void debug_pr(){};

template <typename T, typename... Types>
inline void debug_pr(T var1, Types... var2) {
  (*ard_out_stream) << var1;
  debug_pr(var2...);
}
#else


/**
 * @brief Simple template function that can be used to print debug
 * messages.
 *
 * It takes an arbitrary number of parameters and it prints them in order.
 *
 * These messages are printed only when the DEBUG_TRUE symbol is defined.
 *
 * @note By default, it prints uint8_t variables as ASCII characters.  Cast
 * these variables to int in order to get readable output.  Example:
 * @code
 *
 * uint8_t i;
 * debug_pr(ARD_F("The value of i is: "), int(i), ARD_F("\n"));
 *
 * @endcode
 *
 * The example uses the ARD_F macro to encapsulate strings.  It is important to
 * always do this in order to save memory on the Arduino.
 * 
 * @note These messages are printed only for debug builds.
 */
inline void debug_pr(){};

template <typename T, typename... Types>
inline void debug_pr(T var1, Types... var2) {
  debug_pr(var2...);
}
#endif

#ifdef LIBRARY_DEBUG_TRUE

inline void lib_debug_pr(){};

template <typename T, typename... Types>
inline void lib_debug_pr(T var1, Types... var2) {
  (*ard_out_stream) << var1;
  lib_debug_pr(var2...);
}
#else
inline void lib_debug_pr(){};

template <typename T, typename... Types>
inline void lib_debug_pr(T var1, Types... var2) {
  lib_debug_pr(var2...);
}
#endif

#ifdef ERROR_TRUE

inline void error_pr(){};

template <typename T, typename... Types>
inline void error_pr(T var1, Types... var2) {
  (*ard_out_stream) << var1;
  error_pr(var2...);
}
#else
inline void error_pr(){};

template <typename T, typename... Types>
inline void error_pr(T var1, Types... var2) {
  error_pr(var2...);
}
#endif

#endif // End include guard ARD_LOG_H