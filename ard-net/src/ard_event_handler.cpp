#include "ard_event_handler.h"
#include "ard_event_manager.h"
#include "ard_network_layer_one.h" // for the retrieveRadioFrame of layer one.

/********************************************************************
 *                      ArdTimerEventHandler                       *
 * *****************************************************************/
ArdTimerEventHandler::ArdTimerEventHandler(ArdEventManager *a_event_manager,
                                           ArdSysInterface *a_ard_sys_int)
    : m_event_manager(a_event_manager), m_ard_sys_int(a_ard_sys_int),
      m_timer_func(nullptr), m_loop(false), m_delay(0), m_abs_deadline(0),
      m_active(false) {}

void ArdTimerEventHandler::startTimer(uint16_t a_delay,
                                      TimerFunctor *a_timer_func, bool a_loop) {
  m_delay = a_delay;
  m_timer_func = a_timer_func;
  m_loop = a_loop;
  m_abs_deadline = m_ard_sys_int->getMillis() + a_delay;
  m_event_manager->registerEventHandler(this);
  if (a_loop) {
    (*a_timer_func)();
  }
  m_active = true;
}

bool ArdTimerEventHandler::CheckForEvent() {
  if (m_loop && (m_ard_sys_int->getMillis() > m_abs_deadline)) {
    m_abs_deadline += m_delay;
  }
  return (m_ard_sys_int->getMillis() > m_abs_deadline);
}

void ArdTimerEventHandler::RunEventHandler() {
  if (!m_timer_func) {
    ard_error(ARD_F("TimerEventHandler: trying to run an invalid functor\n"));
  } else {
    // (*m_timer_func)();
    if (m_loop) {
      // set the timer again
      m_abs_deadline = m_ard_sys_int->getMillis() + m_delay;
      m_event_manager->setNextTimer();
    } else {
      // de-register the timer handler
      m_event_manager->removeEventHandler(this);
    }
    (*m_timer_func)();
  }
}

void ArdTimerEventHandler::stopTimer() {
  m_active = false;
  // de-register the timer handler
  m_event_manager->removeEventHandler(this);
}

/********************************************************************
 *                      ARDRF12EventHandler                       *
 * *****************************************************************/
ARDRF12EventHandler::ARDRF12EventHandler(ArdNetworkLayerOne *a_net_layer_one,
        ArdSysInterface *a_ard_sys_int)
    : m_net_layer_one(a_net_layer_one), m_ard_sys_int(a_ard_sys_int) {}

bool ARDRF12EventHandler::CheckForEvent() {
  return (m_ard_sys_int->radioRecvDone() && rf12_crc == 0);
}

void ARDRF12EventHandler::RunEventHandler() {
  m_net_layer_one->retrieveRadioFrame();
}
