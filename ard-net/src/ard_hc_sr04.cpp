#include "ard_hc_sr04.h"
#include "ard_log.h"
#include <Arduino.h>
#include <EnableInterrupt.h>

const unsigned long DIST_2_TIME=58;

///////////////////////////////////////////////////////////////////////
///    Some uggly stuffs about interrupt management....
///////////////////////////////////////////////////////////////////////
// To increase/decrease the number of handlers:
//    - add/remove NULLs in  hcsr04Parents
//    - declare new / remove _isrXXX functions
//    - add their pointers in isrTable
HcSr04int *hcsr04Parents[]={NULL, NULL, NULL, NULL};
const size_t MAX_NB_HCSR04 = sizeof(hcsr04Parents) / sizeof(*hcsr04Parents);
void _isr1() {
    hcsr04Parents[0]->_isr();
}
void _isr2() {
    hcsr04Parents[1]->_isr();
}
void _isr3() {
    hcsr04Parents[2]->_isr();
}
void _isr4() {
    hcsr04Parents[3]->_isr();
}
void (*const isrTable[])() = {
    _isr1,
    _isr2,
    _isr3,
    _isr4,
};

void( *allocHcsr04(HcSr04int *thisPointer))(){
  size_t i;
  for(i=0; i<MAX_NB_HCSR04 && hcsr04Parents[i]!=NULL; i++); // Look for 1st NULL in hcsr04Parents

  if(i >= MAX_NB_HCSR04){ // could return NULL
      error_pr(ARD_F("Too many HCSR04"));
  }

  hcsr04Parents[i] = thisPointer;
  return isrTable[i];
}
void freeHcsr04(HcSr04int *thisPointer){
  size_t i;
  for(i=0; i<MAX_NB_HCSR04 && hcsr04Parents[i]!=thisPointer; i++); // Look for thisPointer in hcsr04Parents

  if( i<MAX_NB_HCSR04 && hcsr04Parents[i]==thisPointer)
      hcsr04Parents[i] = NULL;
}



///////////////////////////////////////////////////////////////////////
///    The Class
///////////////////////////////////////////////////////////////////////
HcSr04int::HcSr04int(int max_dist)
{
  finished = false;
  overDistance = false;
  max_time = max_dist * DIST_2_TIME;
  startTs = 0;
}
HcSr04int::HcSr04int(int trigger_pin, int echo_pin, int max_dist): HcSr04int(max_dist)
{
  this->trigger_pin = trigger_pin;
  this->echo_pin = echo_pin;
  init();
}
HcSr04int::~HcSr04int(){
  freeHcsr04(this);
}

void HcSr04int::init()
{
  // Configure pins
  pinMode(trigger_pin, OUTPUT);
  digitalWrite(trigger_pin, LOW);
  pinMode(echo_pin, INPUT);

  // Attach isr
  void (*pIsr)() = allocHcsr04(this);
  enableInterrupt(echo_pin, pIsr, CHANGE);
}

void HcSr04int::start()
{
  finished=false;
  startTs = 0;
  overDistance=false;
  digitalWrite(trigger_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger_pin, LOW);
  StartResetTimeout();
}

void HcSr04int::StartResetTimeout(){
  trigTs = micros(); // to deal with measurement timeout reset
}
void HcSr04int::ResetMeasureIfNeeded(){
  if( (micros()-trigTs)>max_time*10 )  // x10 is purely empirical!
      start();
}

bool HcSr04int::isFinished()
{
    ResetMeasureIfNeeded(); // Magic workaround to deal with strange behaviour of the sensor having trouble to restart after a timeout
   if( finished ) return true;

   if( startTs!=0 && ((micros()-startTs)>max_time) ){
     overDistance = true;
     return true;
   }

   return false;
}


long HcSr04int::getDistance(bool restart)
{
  long ret;

  if( overDistance ){ // we had a timeout while waiting for the echo
    ret = -1;
  } else {
    ret = (endTs-startTs)/DIST_2_TIME;
  }

  if( restart )  start();

  return ret;
}


void HcSr04int::_isr()
{
  switch( digitalRead(echo_pin) ){
    case HIGH:
      if( startTs ) return;  // Has already been started
      startTs = micros();
      break;

    case LOW:
      if( !startTs ) return;  // Has not already been started
      endTs = micros();
      finished = true;
      break;
  }
}
