#ifndef ARD_EVENT_HANDLER_MOCK_H
#define ARD_EVENT_HANDLER_MOCK_H

#include "gmock/gmock.h"

#include "ard_event_handler.h"

class ArdEventHandlerMock : public ArdEventHandler {
public:
  MOCK_METHOD0(CheckForEvent, bool());
  MOCK_METHOD0(RunEventHandler, void());
};

#endif // ARD_EVENT_HANDLER_MOCK_H
