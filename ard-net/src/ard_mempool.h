#ifndef ARD_ArdMemPool_H
#define ARD_ArdMemPool_H
/**
 * @file ard_mempool.h
 * @brief Simple memory management class template. Coupled with the
 * ard_unique_ptr_mempool class.  (ard_mempool cannot be used without it.)
 *
 * @copyright Copyright (c) 2019
 *
 * Simple template class that store objects of type T, each one of the same
 * fixed size.  The number of slots is set at compilation time.
 */

#include "ard_bitmap.h" // for the CBitMap class
#include "ard_error.h"  // for the ar_error function
#include "ard_log.h"    // to be able to log

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else

#include <cstdint> // for uint8_t

#endif
// see https://en.cppreference.com/w/cpp/types/integer

namespace ArdNetConstants {
constexpr uint8_t mem_pool_max_size =
    16; /**< The maximum number of slots in a ArdMemPool */
constexpr uint8_t mem_pool_full = 255; /**< Used to signal a full ArdMemPool */
constexpr uint8_t mem_pool_default_slots =
    13; /**< The default number of slots in the ArdMemPool */
constexpr uint8_t mem_pool_invalid_index = 254; /**< Used to signal am invalid
                                        index in the ArdUniquePtrMemPool.*/
} // namespace ArdNetConstants

// Forward definition so that we can use it as the return type inside ArdMemPool
template <typename T> struct ArdUniquePtrMemPool;

/**
 * @brief Stores items of type T
 *
 * Right now the number of slots is set as a constant at compile time
 * (ArdNetConstants::mem_pool_default_slots).  The solution is not very elegant
 * but it works.  We need either a constant, or a template parameter, if we want
 * to allocate an array as a data member (i.e., avoid using new).  I tried using
 * the template parameter but it was not straightforward to make it work with
 * the ArdUniquePtrMemPool.  I had some weird compiler error messages and I just
 * gave up.
 *
 * Another possible solution is to allocate the memory buffer as a global
 * variable and pass it to the constructor.  I did not have time to check
 * whether indexing on such a pointer would work (i.e., whether mem[i] would
 * indeed point to the i-th object).  I was not able to find any clear
 * explanation online as people just use new.
 *
 * Unfortunately, the compiler for Arduino does not support placement new.
 * Therefore this is not an option.
 *
 * @tparam T The item type
 *
 */
template <typename T> class ArdMemPool {
public:
  /**
   * @brief Construct a new Memory Pool object of a given size
   *
   * Allocates enough memory for n instances of T as a data member, without
   * using new.
   */
  ArdMemPool();

  /**
   * @brief Allocates a slot
   *
   * @return ArdUniquePtrMemPool<T> unique pointer owning the newly allocated
   * slot. It returns nullptr when the memory pool is full.
   *
   */

  ArdUniquePtrMemPool<T> AllocateSlot();

  /**
   * @brief Returns the number of available slots.
   *
   * @return uint8_t The number of available slots.
   */
  uint8_t AvailableSlots();

  /**
   * @brief Get the Pointer Of Slot object
   *
   * @param i The index of the slot.
   * @return T* The corresponding pointer.
   */
  T *GetPointerOfSlot(uint8_t i);

  /**
   * @brief Release the i-th slot
   *
   * @param i The slot to be released.
   */
  void ReleaseSlot(uint8_t i);

  /**
   * @brief Destroy the Mem Pool object
   *
   */
  // ~ArdMemPool() { delete[] mem; };

private:
  T mem[ArdNetConstants::mem_pool_default_slots];
  ArdBitMap bitmap;
  uint8_t num_used_slots;
  uint8_t total_slots;
};

/*************** Beginning of Definitions ********/
template <typename T>
ArdMemPool<T>::ArdMemPool()
    : num_used_slots(0), total_slots(ArdNetConstants::mem_pool_default_slots),
      bitmap(ArdNetConstants::mem_pool_default_slots) {
  if (ArdNetConstants::mem_pool_default_slots >
      ArdNetConstants::mem_pool_max_size) {
    error_pr(ARD_F("ArdMemPool: requested size("),
             int(ArdNetConstants::mem_pool_default_slots),
             ARD_F(") too large, maximum value: "),
             int(ArdNetConstants::mem_pool_max_size), "\n");
    ard_error(ARD_F("ArdMemPool: requested size too large\n"));
  }
  lib_debug_pr(ARD_F("Created ArdMemPool with "), int(total_slots),
               ARD_F("slots\n"));
}

template <typename T> ArdUniquePtrMemPool<T> ArdMemPool<T>::AllocateSlot() {
  if (num_used_slots == total_slots) {
    lib_debug_pr(ARD_F("ArdMemPool full (AllocateSlot)\n"));
    return ArdUniquePtrMemPool<T>(this,
                                  ArdNetConstants::mem_pool_invalid_index);
  }
  uint8_t n = bitmap.find_unset_bit();
  if (n == ArdNetConstants::bitmap_full) {
    error_pr(
        ARD_F("ArdMemPool internal error, the bitmap is full but it should "
              "not be\n"));
    error_pr(ARD_F("     num_used_slots: "), int(num_used_slots),
             " total_slots: ", int(total_slots), "\n");
    ard_error(ARD_F("ArdMemPool internal error, bitmap unexpectedly full\n"));
  }
  bitmap.set_bit(n);
  num_used_slots++;
  lib_debug_pr(ARD_F("ArdMemPool AllocateSlot allocated slot: "), int(n),
               ARD_F("\n"));
  return ArdUniquePtrMemPool<T>(this, n);
}

template <typename T> uint8_t ArdMemPool<T>::AvailableSlots() {
  return total_slots - num_used_slots;
}

template <typename T> T *ArdMemPool<T>::GetPointerOfSlot(uint8_t i) {
  if (i >= 0 and i < total_slots and bitmap.is_bit_set(i))
    return &(mem[i]);
  else {
    error_pr(ARD_F("ArdMemPool error: asking for pointer of slot: "), int(i));
    error_pr(ARD_F("  that is either not set or larger than total_slots ("),
             int(total_slots), ARD_F("\n"));
    ard_error(ARD_F("ArdMemPool asking for pointer of invalid slot\n"));
    return nullptr;
  }
}

template <typename T> void ArdMemPool<T>::ReleaseSlot(uint8_t i) {
  if (bitmap.is_bit_set(i)) {
    bitmap.unset_bit(i);
    num_used_slots--;
    lib_debug_pr(ARD_F("ArdMemPool freeing slot "), int(i), ARD_F("\n"));
  } else {
    error_pr(ARD_F("ArdMemPool error, realising slot "), int(i),
             ARD_F(" that is not set\n"));
    ard_error(ARD_F("ArdMemPool trying to release a slot which is not set\n"));
  }
}

/**********************************************************************
 *                                                                    *
 *                      ArdUniquePtrMemPool                           *
 *                                                                    *
 * ********************************************************************/

/**
 * @brief A "smart pointer" that owns a slot in the mempool.
 *
 * This class must be used ArdMemPool as they are tightly coupled.  It is meant
 * to have the similar semantics (and usage) to the unique_ptr of the standard
 * library, minus the custom deleter, comparison operators, swap, and reset.  I
 * removed these mehods as we don't need them for the moment and they increased
 * the complexity (and potential bugs).
 *
 * It is possible to implement a more elegant solution using a custom deleter,
 * which could be used to store the index but I did not have time to look into
 * this, specifically at the consequences in terms of memory needed to store the
 * custom deleter.  To make this idea work, one would need to create a custom
 * deleter for each pointer, basically a functor storing the corresponding
 * mempool index.
 *
 * Each pointer has a reference to the corresponding mempool as well the index
 * of the slots it points to.
 *
 * Inspired by this implementation:
 * https://github.com/skarupke/compile_time/blob/master/dunique_ptr.hpp
 *
 * @tparam T The type the pointer points to.
 *
 * @warning
 * The mempool must use the same type T.
 *
 * Inspired by this implementation:
 * https://github.com/skarupke/compile_time/blob/master/dunique_ptr.hpp
 */
template <typename T> struct ArdUniquePtrMemPool {
  // friend template class ArdMemPool<T>;
  friend class ArdMemPool<T>;

  /**
   * @brief The default constructor.  Constructs and invalid pointer.
   *
   */
  explicit ArdUniquePtrMemPool(ArdMemPool<T> *a_mempool = nullptr)
      : m_mempool(a_mempool),
        m_slot_index(ArdNetConstants::mem_pool_invalid_index) {}

  /**
   * @brief Delete the copy Constructor.
   *
   */
  ArdUniquePtrMemPool(const ArdUniquePtrMemPool &) = delete;

  /**
   * @brief Delete the copy assignment operator.
   *
   */
  ArdUniquePtrMemPool &operator=(const ArdUniquePtrMemPool &) = delete;

  /**
   * @brief Move Constructor.
   *
   * Just copy the index from the source and set the index of the source to
   * invalid.
   *
   * @param other The source pointer.
   */
  ArdUniquePtrMemPool(ArdUniquePtrMemPool &&other)
      : m_mempool(other.m_mempool), m_slot_index(other.m_slot_index) {
    other.m_slot_index = ArdNetConstants::mem_pool_invalid_index;
  }

  /**
   * @brief Move assignment operator.
   *
   * No need to use the swap pattern as we don't currently have a swap method (I
   * don't think we need it.)
   *
   * @param other The source pointer.
   * @return ArdUniquePtrMemPool& the result of the move.
   */
  ArdUniquePtrMemPool &operator=(ArdUniquePtrMemPool &&other) {
    if ((m_slot_index != ArdNetConstants::mem_pool_invalid_index) &&
        (m_mempool != nullptr)) {
      m_mempool->ReleaseSlot(m_slot_index);
    }
    m_slot_index = other.m_slot_index;
    m_mempool = other.m_mempool;
    other.m_slot_index = ArdNetConstants::mem_pool_invalid_index;
    return *this;
  }

  /**
   * @brief Destroy the Ard Unique Ptr Mem Pool object.
   *
   * Simply release the corresponding slot, as long as the index is valid.  We
   * need the test because this could be a dandling pointer following a move
   * operation.
   *
   */
  ~ArdUniquePtrMemPool() {
    if (m_slot_index != ArdNetConstants::mem_pool_invalid_index)
      m_mempool->ReleaseSlot(m_slot_index);
  }

  /**
   * @brief Convert the pointer to bool.
   *
   * @return true If the slot index is valid.
   * @return false  If the slot index is not valid.
   */
  explicit operator bool() const {
    return ((m_slot_index != ArdNetConstants::mem_pool_invalid_index) &&
            (m_mempool != nullptr));
  }

  /**
   * @brief Get the corresponding pointer.
   *
   * @return T* The pointer to the object stored in the memepool (nullptr if the
   * index is not valid, i.e., if this pointer points to nothing.)
   */
  T *get() const {
    if ((m_slot_index != ArdNetConstants::mem_pool_invalid_index) &&
        (m_mempool != nullptr))
      return m_mempool->GetPointerOfSlot(m_slot_index);
    else
      return nullptr;
  }

  /**
   * @brief Dereference operator.
   *
   * @return T& The object pointed by this pointer.
   */
  T &operator*() const { return *get(); }

  /**
   * @brief Member access (structure dereference) operator.
   *
   * @return T* The pointer to the object stored in the mempool (nullptr if the
   * index is not valid).
   */
  T *operator->() const { return get(); }

  bool operator==(const ArdUniquePtrMemPool &other) const {
    return (m_slot_index == other.m_slot_index) &&
           (&m_mempool == &(other.m_mempool));
  }

  bool operator!=(const ArdUniquePtrMemPool &other) const {
    return (m_slot_index != other.m_slot_index) ||
           (&m_mempool != &(other.m_mempool));
  }
  // bool operator<(const ard_unique_ptr &other) const { return ptr < other.ptr;
  // } bool operator<=(const ard_unique_ptr &other) const {
  //   return !(other < *this);
  // }
  // bool operator>(const ard_unique_ptr &other) const { return other < *this; }
  // bool operator>=(const ard_unique_ptr &other) const {
  //   return !(*this < other);
  // }

private:
  /**
   * @brief Construct a new Ard Unique Ptr Mem Pool object.
   *
   * This constructor is private so that only ArdMemPool can call it, ensuring
   * that nobody else can create these pointers.
   *
   * @param a_mempool the corresponding mempool
   * @param a_i the corresponding index in the mempool
   */
  ArdUniquePtrMemPool(ArdMemPool<T> *a_mempool, uint8_t a_slot_index)
      : m_mempool(a_mempool), m_slot_index(a_slot_index) {}

  ArdMemPool<T> *m_mempool; ///< Pointer to the corresponding mempool
  uint8_t m_slot_index;     ///< And the slot index in the mempool
};

struct ArdPktBuffer;
using PktBufPtr = ArdUniquePtrMemPool<ArdPktBuffer>;
using PktMemPool = ArdMemPool<ArdPktBuffer>;

#endif // # define ARD_ArdMemPool_H