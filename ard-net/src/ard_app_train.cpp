#include "ard_app_train.h"
#include "ard_app_central_controller.h" // For parsing functions

#ifndef ARDUINO_AVR_NANO
using namespace std; // for min
#endif

///////////////////////////////////////////////////////////////////////
///    Initialisation & contructors/destructors
///////////////////////////////////////////////////////////////////////
AppTrain::AppTrain(PktMemPool *a_mem_pool, 
                   ArdSysInterface *a_ard_sys_int, 
                   ArdEventManager *a_event_manager,
                   SendInterface<AppAddr> *a_send_interface,
                   AppAddr a_this_addr)
    : AppBase(a_mem_pool, a_ard_sys_int, a_event_manager,
              a_send_interface, a_this_addr),
    m_traction_ON(false),
    m_traction_direction(false),
    m_traction_speed(0),
    m_adjusted_speed(0),
    m_last_sent_traction_state(0),
    m_last_sent_traction_speed(0),
    m_notify_general_controller(false),
    m_last_traction_direction(m_traction_direction),
    m_train_stop_timer(a_event_manager, a_ard_sys_int),
    m_distance_read_timer(a_event_manager, a_ard_sys_int)   
{
    m_last_distance[0] = 0;
    m_last_distance[1] = 0;
    m_train_stop_functor.setParent(this);
    m_distance_read_functor.setParent(this);
}

void AppTrain::init() {
    info_pr(ARD_F("[AppTrain] Init (App Addr="), m_this_addr.m_addr, ARD_F(")\n"));

    // Configure pins for Motor Control:
    ArdNetworkLayer::m_ard_sys_int->arPinMode(m_motor_pin_1A, OUTPUT);      // 1A
    ArdNetworkLayer::m_ard_sys_int->arPinMode(m_motor_pin_2A, OUTPUT);      // 2A
    ArdNetworkLayer::m_ard_sys_int->arPinMode(m_motor_pin_enable, OUTPUT);  // EN1,2
    ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_enable, LOW);// motor off

    // Configure pins for Distance Sensor
    for( int i=0; i<2; i++){
        m_dist_sensors[i].trigger_pin = m_sensor_trig_pin[i];
        m_dist_sensors[i].echo_pin = m_sensor_echo_pin[i];
        m_dist_sensors[i].init();
        m_dist_sensors[i].start();
    }
    m_distance_read_timer.startTimer(50, &m_distance_read_functor, true);    
}

//////////////////////////////////////////////////////////////////////
//    Radio Rx Processing
//////////////////////////////////////////////////////////////////////
PktBufPtr AppTrain::processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                  bool *p_result) {
  lib_debug_pr(ARD_F("AppTrain received, size: "), int(a_p->curr_size),
               ARD_F("\n"));

  bool tst;
  a_p = AppBase::processRxData(ard_move(a_p), a_src_addr, &tst);
  if (tst) {
    *p_result = true;
    return (ard_move(a_p));
  }

  AppMessage appMsg;
  if (!appMsg.analyze(a_p->data, a_p->curr_size) ){
      *p_result = false;
      return (ard_move(a_p));
  }

  m_last_requester_addr = mapAddr<AnyAddr, AppAddr>(a_src_addr, a_src_addr);


  if (appMsg.m_msg_type==APP_TYP_TRAIN_STATUS_REQUEST){
    info_pr(ARD_F("Received Train Status Request from "), m_last_requester_addr.m_addr,
            ARD_F("\n"));

    sendTrainStatusMessage(m_last_requester_addr);
    if (m_notify_general_controller)
            sendTrainStatusMessage(GetAppAddress(APP_DEV_CONTROLLER, 
                                   ArdNetConstants::occ_network_number)); // Inform the central controller
        *p_result = true;
        return (ard_move(a_p));
  }
 
  if (appMsg.m_msg_type==APP_TYP_TRAIN_CONTROL){
        PktBufPtr sdu = appMsg.deSerialize(ard_move(a_p), m_mem_pool);
        TrainControlMessage tcMsg; 
        tcMsg.analyze(sdu->data, sdu->curr_size);
        info_pr(ARD_F("Received Train Control from "), m_last_requester_addr.m_addr,
                ARD_F(": direction="), tcMsg.m_direction, ARD_F(", duration="),
                tcMsg.m_duration, ARD_F(", speed="), tcMsg.m_speed,
                ARD_F("\n"));
        applyCommand(tcMsg);
        *p_result = true;
        return (ard_move(a_p));
  }
  *p_result = false;
  return (ard_move(a_p));
}

void AppTrain::sendTrainStatusMessage(AppAddr a_dest_addr){
    TrainStatusMessage tsMsg;
    tsMsg.m_direction = m_traction_direction ? TRAIN_DIR_FW : TRAIN_DIR_BW;
    if (!m_traction_ON) tsMsg.m_direction = TRAIN_DIR_STOP;
    tsMsg.m_speed = m_adjusted_speed;
    PktBufPtr pdu = tsMsg.serialize(m_mem_pool);
    info_pr(ARD_F("Sending Train Status to "), a_dest_addr.m_addr, ARD_F("\n") );
        
    send(ard_move(pdu),
        APP_TYP_TRAIN_STATUS, 
        a_dest_addr);          
}


///////////////////////////////////////////////////////////////////////
///    Serial Port Management
///////////////////////////////////////////////////////////////////////
bool AppTrain::ProcessSerialCmd(unsigned char *msg, size_t msgSize)
{
    if ( AppBase::ProcessSerialCmd(msg, msgSize) )
        return true;

    msg[msgSize] = '\0';
    lib_debug_pr(ARD_F("[AppTrain]: processSerialCmd"));
    lib_debug_pr((char *)msg);

    switch((char)msg[m_pos_in_cmd_parsing]){
        // Train movement
        case 't':
        case 'T':
          lib_debug_pr(ARD_F("   Ok, this is a train command for "), m_dest, ARD_F("\n"));
          processTrainCmd((char*)msg+m_pos_in_cmd_parsing+1, msgSize-m_pos_in_cmd_parsing-1);
          return true;
          break;

        default:
          info_pr(ARD_F("   Humm! unknown command for "), m_dest, ARD_F("\n"));
          return false;
          break;
    } // END of switch(msg[0])

} // AppTrain::processSerialCmd


void AppTrain::processTrainCmd(char *msg, size_t msgSize)
{
    lib_debug_pr(ARD_F("Train Command Parsing...\n"));
    TrainControlMessage tcMsg; 
    if ( !AppCentralController::ParseTrainCommand(msg, msgSize, &tcMsg) ){
      lib_debug_pr(ARD_F("Train Command Parsing FAILED: skipping this command\n"));
      return; // Parsing failed (invalid command)
    }
    // lib_debug_pr(ARD_F("    Train direction: "), tcMsg.m_direction, ARD_F("\n"));
    // lib_debug_pr(ARD_F("    Duration: "), tcMsg.m_duration, ARD_F("\n"));
    // lib_debug_pr(ARD_F("    Speed: "), tcMsg.m_speed, ARD_F("\n"));

    applyCommand(tcMsg);
} // AppTrain::processTrainCmd



///////////////////////////////////////////////////////////////////////
///    Train Movement
///////////////////////////////////////////////////////////////////////
TrainStopFunctor::TrainStopFunctor()
{}
void TrainStopFunctor::setParent(AppTrain *a_parent){
      m_parent = a_parent;
}
void TrainStopFunctor::operator()() {
  lib_debug_pr(ARD_F("Train Stop Timer called\n"));
  m_parent->stopMotor();
}


DistanceReadFunctor::DistanceReadFunctor()
{}
void DistanceReadFunctor::setParent(AppTrain *a_parent){
      m_parent = a_parent;
}
void DistanceReadFunctor::operator()() {
    for(int i=0; i<2; i++){
        m_parent->m_last_distance[i] = m_parent->m_dist_sensors[i].getDistance(true);
    }
    if( m_parent->m_traction_ON == true) {
        m_parent->setMotor();
    }
}

void AppTrain::sendStateToControllerIfNeeded(){
    // bool previous_traction_state = m_traction_ON;
    if (m_last_sent_traction_state != m_traction_ON
     || m_last_sent_traction_speed != m_adjusted_speed
     || m_last_traction_direction != m_traction_direction)
    {
        sendTrainStatusMessage(m_last_requester_addr); // Inform the last requester
        if (m_notify_general_controller)
            sendTrainStatusMessage(GetAppAddress(APP_DEV_CONTROLLER, 
                                   ArdNetConstants::occ_network_number)); // Inform the central controller
        m_last_sent_traction_state = m_traction_ON;
        m_last_sent_traction_speed = m_adjusted_speed;
        m_last_traction_direction = m_traction_direction;
    }
}

void AppTrain::applyCommand(TrainControlMessage a_tcMsg)
{
    lib_debug_pr(ARD_F("Apply Train Command:\n"));
    lib_debug_pr(ARD_F("    Train direction: "), a_tcMsg.m_direction, ARD_F("\n"));
    lib_debug_pr(ARD_F("    Duration: "), a_tcMsg.m_duration, ARD_F("\n"));
    lib_debug_pr(ARD_F("    Speed: "), a_tcMsg.m_speed, ARD_F("\n"));

    if(a_tcMsg.m_direction==TRAIN_DIR_STOP){
        m_traction_ON = false;
        stopMotor();
        return;
    }
    bool dir = (a_tcMsg.m_direction==TRAIN_DIR_FW) ? true : false;
    m_traction_ON = true;
    moveTrain(dir, a_tcMsg.m_duration, a_tcMsg.m_speed);
} // TrainControllerApp::ApplyCommand

void AppTrain::moveTrain(bool a_direction, 
                         uint16_t a_duration, 
                         uint8_t a_speed)
{
    m_train_stop_timer.stopTimer(); // Cancel any previously programmed stop timer
    if (a_duration!=0){ // If a duration was specified, set the stop timer
        m_train_stop_timer.startTimer(a_duration, &m_train_stop_functor);
    }

    m_traction_direction = a_direction;
    m_traction_speed = a_speed;
    setMotor();
} // AppTrain::moveTrain
 
void AppTrain::setMotor()
{
    // AAC: Stop  motor if too near from obstacle
    if ( avoidCollision() )return;

    if (m_traction_direction) {
        ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_1A, HIGH);
        ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_2A, LOW);
    } else {
        ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_1A, LOW);
        ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_2A, HIGH);
    }
    ArdNetworkLayer::m_ard_sys_int->arAnalogWrite(m_motor_pin_enable, m_adjusted_speed); // TODO: created a ard_sys_int wrapper...for AnalogWrite
    sendStateToControllerIfNeeded();
} // AppTrain::setMotor

bool AppTrain::avoidCollision()
{
    long stopDistance = 10;
    long slowingDistance = 15;
    unsigned char slowingSpeed = 200;
    long dist = m_traction_direction ? m_last_distance[1] : m_last_distance[0];

    if( dist<=stopDistance ){
        stopMotor(false);
        return true;
    }

    if ( dist<=slowingDistance ){
        m_adjusted_speed = min((uint8_t)m_traction_speed, (uint8_t)slowingSpeed);
    } else {
        m_adjusted_speed = m_traction_speed;
    }

    return false;
} // AppTrain::avoidCollision


void AppTrain::stopMotor(bool a_update_current_traction_state)
{
    ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_1A, LOW);
    ArdNetworkLayer::m_ard_sys_int->arDigitalWrite(m_motor_pin_2A, LOW);
    ArdNetworkLayer::m_ard_sys_int->arAnalogWrite(m_motor_pin_enable, 0); // TODO: created a ard_sys_int wrapper...for AnalogWrite

    if(a_update_current_traction_state){
      m_traction_ON = false;
      m_traction_speed = 0;
    }
    m_adjusted_speed = 0;
    sendStateToControllerIfNeeded();
} // AppTrain::stopMotor
