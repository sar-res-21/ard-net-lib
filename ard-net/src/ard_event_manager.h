/**
 * @file ard_event_manager.h
 *
 */

#ifndef ARD_EVENT_MANAGER_H
#define ARD_EVENT_MANAGER_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else
#include <cstdint> // for uint8_t
#endif

#include "ard_event_handler.h"
#include "ard_sys_interface.h"

class ArdEventHandler;
class ArdTimerEventHandler;

namespace ArdNetConstants {
constexpr uint8_t event_hndlr_slots =
    12; /**< Maximum number of event handlers */
}

#include "ard_event_handler.h"
/**
 * @brief This class is in charge of storing pointers to the different event
 * handlers and to call them (to check if an event has fired and, if it is the
 * case, to call the corresponding handler.)
 *
 * In order to avoid dynamically allocated lists, there is a statically
 * allocated array of pointers to event handlers.  There is another statically
 * allocated array for the timer handlers, which are implemented differently.
 * Rather than checking at every iteration of the even loop if a timer should
 * fire, there is a pointer to the next timer that is going to expire.  When
 * this happens, the pointer is updated.
 *
 */
class ArdEventManager {
public:
  /**
   * @brief Default constructor. It just sets all the event handler pointers to
   * nullpr
   *
   * @arg a_ard_sys_int Pointer to the system interface (used by the timers
   * to access the millis "system" call)
   *
   */
  explicit ArdEventManager(ArdSysInterface *a_ard_sys_int);

  /**
   * @brief Add and event handler to the array of event handlers.
   *
   * @param a_event_handler The pointer to the even handler to add.
   */
  void registerEventHandler(ArdEventHandler *a_event_handler);

  /**
   * @brief Add and a timer event handler to the array of timer event handlers.
   *
   * Specialized version of the register method for timer events, which are
   * added to the m_timer_event_handlers vector rather than the "normal"
   * m_event_handlers vector.
   *
   * @param a_timer_event_handler The pointer to the even handler to add.
   */
  void registerEventHandler(ArdTimerEventHandler *a_timer_event_handler);

  /**
   * @brief Removes an event handler.
   *
   * @param a_event_handler The pointer to the event handler to remove.
   */
  void removeEventHandler(ArdEventHandler *a_event_handler);

  /**
   * @brief Removes a timber event handler.
   *
   * Specialized version of the register method for timer events, which are
   * added to the m_timer_event_handlers vector rather than the "normal"
   * m_event_handlers vector.
   *
   * @param a_timer_event_handler The pointer to the timer event handler to
   * remove.
   */
  void removeEventHandler(ArdTimerEventHandler *a_timer_event_handler);

  /**
   * @brief Runs "one iteration of the event loop."  I.e., for all the
   * registered event handlers it calls checkForEvent if this method returns
   * true, it calls the runEventHandler method.
   *
   * This method returns the number of events that have fired  to ease testing.
   * The return value can be safely ignored.
   *
   * @return int The number of events that fired.
   */
  uint8_t loopIteration();

  /**
   * @brief Returns the number of available slots in the event handler array.
   *
   * Used in the tests to make things easier.
   *
   * @return uint8_t The number of available slots (i.e., that are not nullptr).
   */
  uint8_t availableSlots();

  /**
   * @brief Sets the value of the m_next_timer pointer attribute
   *
   * It goes over the m_timer_event_handlers array and selecting the minimum
   * value of the m_abs_deadline attributes of each timer.
   */
  void setNextTimer();

private:
  ArdEventHandler *m_event_handlers[ArdNetConstants::event_hndlr_slots];
  ArdTimerEventHandler
      *m_timer_event_handlers[ArdNetConstants::event_hndlr_slots];
  ArdTimerEventHandler *m_next_timer;
  ArdSysInterface *m_ard_sys_int;
};

#endif // End of include guard ARD_EVENT_MANAGER_H