/**
 * @file ard_fake_network_interface.h
 * @brief A fake network interface used only by the tests.
 *
 * @note DO NOT USE THIS CLASS
 *
 */


#ifndef ARD_FAKE_NETWORK_INTERFACES_H
#define ARD_FAKE_NETWORK_INTERFACES_H

#include <list>
#include <iterator>

#include "ard_mempool.h"
#include "ard_network_layer_base.h"
#include "ard_pkt_buffer.h"
#include "ard_utils.h" // for ard_move
#include "ard_error.h"

template <typename T>
class ArdFakeNorthInterface : public ArdNetNorthInterface<T> {
public:
  ArdFakeNorthInterface()
      : ArdNetNorthInterface<T>(), m_request_count(0) {}
  void sendRequest(PktBufPtr a_p, T dst_addr) override;
  std::list<ArdPktBuffer> m_pkt_list;
  std::list<T> m_dst_addr_list;
  int m_request_count;
};

template <typename T>
void ArdFakeNorthInterface<T>::sendRequest(PktBufPtr a_p, T dst_addr) {
  ++m_request_count;
  if (a_p) {
    m_pkt_list.push_back(ArdPktBuffer(*a_p));
    m_dst_addr_list.push_back(dst_addr);
  }
  else{
    ard_error(ARD_F("SendRequest called with null pointer!\n"));
  }
  if (this->m_upper_layer) {
    this->m_upper_layer->dataHandlingDone(ard_move(a_p), true);
  }
}

#endif // ARD_FAKE_NETWORK_INTERFACES_H
