#ifndef HC_SR04_H
#define HC_SR04_H

// #ifdef ARDUINO_AVR_NANO
// #include <Arduino.h>
// #endif

/// HC-SR04 ultrasound sensor interrupt based manager.
///
/// This object sets pins and interrupts to drive a HC-SR04 ultrasound
/// sensor. On a Arduino Uno the echo pin can pin 2 (interrupt 1) or
/// pin3 (interrupt 2). The trigger pin can be any digital out pin.
class HcSr04int {

public:
  HcSr04int(
      int max_dist = 500); ///< Construct new object HcSr04int but do not call
                           ///< init(). So user can call it after having
                           ///< specified trigger_pin and echo_pin members.
  HcSr04int(int trigger_pin, int echo_pin,
            int max_dist = 500); ///< Construct new object HcSr04int, attach it
                                 ///< to specified pins and calls init();
  ~HcSr04int();
  void init();  ///< Init pin configuration and interrupt vector installation */
  void start(); ///< Start a new acquisition (ie. send a pulse) */
  bool isFinished(); ///< To be called after start(). Returns true if the echo
                     ///< has been received (or timeout occurred) and false if
                     ///< still waiting for the echo (ie. the measurement is
                     ///< still on going)
  long getDistance(bool restart = false); ///< To be called after isFinished().
                                          ///< Return the distance in cm or -1
                                          ///< in case of a measurement timeout
  // static void _isr(HcSr04int* _this);
  void _isr(); ///< Interrupt sub routine. Should not be called. The only reason
               ///< for it to be public is that we need to call it from the
               ///< common interrupt handlers

  int trigger_pin; ///< The Trigger pin number
  int echo_pin;    ///< The Echo pin number

protected:
  unsigned long
      max_time;      ///< Timeout before declaring that no echo was received
  bool overDistance; ///< True if last measurement was aborted due to timeout

private:
  volatile unsigned long startTs, endTs;
  volatile bool finished; ///< true if an echo has been completely received

  // Next functions are kind  of magic to cope with strange behaviour of the
  // sensor which has trouble to restart after a timeout
  unsigned long trigTs;
  void StartResetTimeout();
  void ResetMeasureIfNeeded();
};

#endif // HC_SR04_H
