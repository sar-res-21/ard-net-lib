
#include "ard_network_layer_two_message.h"
#include "ard_memcpy.h"

/********************************************************************
 *                    Layer Two Messages                            *
 ********************************************************************/
L2Message::L2Message()
    : m_dst_add(0), m_src_add(0), m_msg_type(L2MsgTypes::L2_TYP_NOTYPE),
      m_seq_num(0), m_payload_len(0), m_pkt_buff() {}

L2Message::L2Message(L2Addr a_src_addr, L2Addr a_dst_addr,
                     uint8_t a_payload_len, uint8_t a_seq_num,
                     L2MsgTypes a_msg_type)
    : m_dst_add(a_dst_addr), m_src_add(a_src_addr), m_msg_type(a_msg_type),
      m_seq_num(a_seq_num), m_payload_len(a_payload_len), m_pkt_buff() {}

PktBufPtr L2Message::serialize(PktBufPtr a_sdu,
                               PktMemPool *a_mpool)
{

  m_pkt_buff = a_mpool->AllocateSlot();
  m_dst_add.serialize(&(m_pkt_buff->data[0]));
  m_src_add.serialize(&(m_pkt_buff->data[1]));
  m_pkt_buff->data[2] = m_msg_type;
  m_pkt_buff->data[3] = m_seq_num;
  m_pkt_buff->data[4] = m_payload_len;
  uint8_t payload_size = 0;
  if (a_sdu)
  {
    // Sanity check
    if (a_sdu->curr_size != m_payload_len)
    {
      debug_pr(ARD_F("L2Message inconsistent payload length."),
               ARD_F("Payload size: "), int(a_sdu->curr_size),
               ARD_F(" but m_payload_len: "), int(m_payload_len), ARD_F("\n"));
      ard_error(ARD_F("L2Message inconsistent payload length."));
    }
    ard_memcpy(&m_pkt_buff->data[l2_header_size], a_sdu->data, a_sdu->curr_size);
    payload_size = a_sdu->curr_size;
  }
  else
  {
    ard_error(ARD_F("L2Message Serialize with non valid pointer"));
  }
  m_pkt_buff->curr_size = l2_header_size + payload_size;
  return (a_sdu);
}

PktBufPtr L2Message::serialize(PktMemPool *a_mpool)
{

  PktBufPtr pdu = a_mpool->AllocateSlot();
  m_dst_add.serialize(&(pdu->data[0]));
  m_src_add.serialize(&(pdu->data[1]));
  pdu->data[2] = m_msg_type;
  pdu->data[3] = m_seq_num;
  pdu->data[4] = m_payload_len;
  pdu->curr_size = l2_header_size;
  return (pdu);
}

PktBufPtr L2Message::deSerialize(PktBufPtr pdu,
                                 PktMemPool *a_mem_pool)
{
  // Sanity checks
  if (pdu->curr_size < l2_header_size)
  {
    ard_error(ARD_F("L2Message too short\n"));
  }
  uint8_t typ = pdu->data[2];
  if ((typ != 0x00) && (typ != 0x01))
  {
    ard_error(ARD_F("L2Message with an unknown message type\n"));
  }

  PktBufPtr sdu = a_mem_pool->AllocateSlot();

  m_dst_add.deSerialize(&(pdu->data[0]));
  m_src_add.deSerialize(&(pdu->data[1]));
  m_msg_type = (L2MsgTypes)pdu->data[2];
  m_seq_num = pdu->data[3];

  sdu->curr_size = pdu->curr_size - l2_header_size;

  memcpy(sdu->data, &pdu->data[l2_header_size], sdu->curr_size);

  return (sdu);
}

bool L2Message::analyze(uint8_t *a_p_data, size_t a_length)
{
  // Sanity checks
  if (a_length < l2_header_size)
  {
    ard_error(ARD_F("L2Message too short\n"));
    return false;
  }
  uint8_t typ = a_p_data[2];
  if ((typ != 0x00) && (typ != 0x01))
  {
    ard_error(ARD_F("L2Message with an unknown message type\n"));
    return false;
  }

  m_dst_add.deSerialize(&(a_p_data[0]));
  m_src_add.deSerialize(&(a_p_data[1]));
  m_msg_type = (L2MsgTypes)a_p_data[2];
  m_seq_num = a_p_data[3];

  return true;
}
L2Addr L2Message::getDstAddr(uint8_t *a_p_data)
{
  // The destination address is the first byte.
  return L2Addr(a_p_data[0]);
}
