/**
 * @file: ard_sys_interface.h
 *
 * @brief A mockable system interface for Arduino
 */

#ifndef ARD_SYS_INTERFACE_H
#define ARD_SYS_INTERFACE_H


/**
 * @brief Structure containing all the methods that we need to mock.
 *
 * Given that Google Mock cannot mock free functions, we have grouped all the
 * functions that we need to mock in this base class, which has two different
 * implementations: a "real" one used for Arduino that calls the actual
 * system calls; and a "fake" one which is a mock used in the tests.
 */
struct ArdSysInterface {
  virtual unsigned long getMillis() = 0;
  virtual bool radioCanSend() = 0;
  virtual bool radioRecvDone() = 0;
  virtual void radioSendStart(uint8_t hdr, const void *ptr, uint8_t len) = 0;
  virtual void radioSendWait(uint8_t mode) = 0;
  virtual void arDigitalWrite(uint8_t, uint8_t) = 0;
  virtual void arPinMode(uint8_t pin, uint8_t  mode) = 0;
  virtual void arAnalogWrite(uint8_t pin, uint8_t value) = 0;
  virtual void arDelay(uint32_t duration) = 0;
};


// Not sure if this is the best place to put this define.  For the moment
// it's here because this file is necessarily included whenever digitalWrite
// is used.
#define LED_JNODE 9

#ifdef ARDUINO_AVR_NANO
#include <Arduino.h>
#include <JeeLib.h>

/**
 * @brief The Arduino implementation of the system interface.
 *
 * Each method calls the corresponding Arduino system call.
 */
struct ArdSysInterfaceImpl : public ArdSysInterface {
  unsigned long getMillis() { return millis(); }
  bool radioCanSend() { return rf12_canSend(); }
  bool radioRecvDone() override { return rf12_recvDone(); }
  void radioSendStart(uint8_t hdr, const void *ptr, uint8_t len) {
    rf12_sendStart(hdr, ptr, len);
  }
  void radioSendWait(uint8_t mode) { rf12_sendWait(0); };

  void arDigitalWrite(uint8_t i, uint8_t j) { digitalWrite(i, j); }
  void arPinMode(uint8_t pin, uint8_t  mode)  { pinMode(pin, mode);}
  void arAnalogWrite(uint8_t pin, uint8_t value)  { analogWrite(pin, value);}
  void arDelay(uint32_t duration) { delay(duration); };

};


#else

/**
 * @brief The system interface base class used in the tests.
 *
 * Each test redefines the return values of the function it needs to mock.
 * See the ArdSysInterfaceImplMock for more details.
 */
struct ArdSysInterfaceImpl : public ArdSysInterface {
  unsigned long getMillis() override { return 121; }
  bool radioCanSend() override { return false; }
  bool radioRecvDone() override { return false; }
  void radioSendStart(uint8_t hdr, const void *ptr, uint8_t len) override {}
  void radioSendWait(uint8_t mode) override {}
  void arDigitalWrite(uint8_t, uint8_t) override {};
  void arPinMode(uint8_t pin, uint8_t  mode) override {}
  void arAnalogWrite(uint8_t pin, uint8_t value) override {}
  void arDelay(uint32_t duration) override {};
};

// Misc definitions needed by the native code

// The following two are for digitalWrite
#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0

// Needed to mock the radio interface
extern uint16_t rf12_crc;
extern uint8_t rf12_len;
extern uint8_t rf12_data[];

// Fake Servo 
class Servo {
public:
  Servo() {};
  void attach(uint8_t pin) {};
  void write(uint8_t val) {};
};


#endif // ARDUINO_AVR_NANO

#endif // ARD_SYS_INTERFACE_H
