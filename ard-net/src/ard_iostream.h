/**
 * @file ard_iostream.h
 * @brief Trying to make it possible to use the same output commands in Arduino
 * and the native tests.
 *
 * At first, I wanted to use std::cout in Arduino as well but I have to give up
 * this solution as it was not clear how to cleanly redefine std::cout so
 * that it is the same as the Serial object, which Arduino uses for input and
 * output.
 *
 * For the time being (April 16, 2019), we use type aliases and a global
 * variable (ard_out_stream) that points to Serial in Arduino and std::cout in
 * the native code.  I'm not happy with with solution but I've not yet been able
 * to find a better one.
 *
 * Then we define the usual \c operator<< to make everything work, including the
 * functions for \c std::endl and \c std::hex (\c std::dec) (based on a post on
 * Stack Overflow).
 */

#ifndef ARD_IOSTREAM_H
#define ARD_IOSTREAM_H

#ifdef ARDUINO_AVR_NANO

/*******************************************************************
 *                    ARDUINO VERSION                              *
 ******************************************************************/

#include <Arduino.h>
#include <HardwareSerial.h>
#include <stdint.h>

/**
 * @brief Global variable storing the current base for printing numbers (\c DEC
 * or \c HEX).
 *
 * The two values \c DEC and \c HEX are always defined in Arduino. The native \c
 * print function uses them to control how numbers are printed.
 *
 */
extern int print_base;

using ard_out_stream_type = HardwareSerial;
using ard_in_stream_type = HardwareSerial;

// Generic template
template <class T> inline Print &operator<<(Print &stream, T arg) {
  stream.print(arg);
  return stream;
}

inline Print &operator<<(Print &stream, int num) {
  stream.print(num, print_base);
  return stream;
}

inline Print &operator<<(Print &stream, uint8_t num) {
  stream.print(num, print_base);
  return stream;
}

// From https://stackoverflow.com/a/1134467
// function that takes a custom stream, and returns it
typedef Print &(*PrintStreamManipulator)(Print &);

// take in a function with the custom signature
inline Print &operator<<(Print &stream, PrintStreamManipulator manip) {
  // call the function, and return its value
  return manip(stream);
}

namespace std {
// define the custom endl for this stream.
// note how it matches the `MyStreamManipulator`
// function signature
inline Print &endl(Print &stream) {
  // print a new line
  // stream.print(" adding a new line !!!! \n");
  stream.print("\n");
  return stream;
}

inline Print &hex(Print &stream) {
  // print a new line
  stream.print(" Switching to HEX \n");
  print_base = HEX;
  return stream;
}

inline Print &dec(Print &stream) {
  // print a new line
  stream.print(" Switching to DEC \n");
  print_base = DEC;
  return stream;
}
} // end namespace std

#else
/*******************************************************************
 *                    NATIVE VERSION                               *
 ******************************************************************/

#include <iostream>
class MockSerial;
using ard_out_stream_type = std::ostream;
using ard_in_stream_type = MockSerial;
// The following does not work :( I'm afraid that the re is no easy way to
// "fake" a Serial object that can be used both for input and output.
//
// The error message was about the << operators not being able to convert
// basic_io to basic_iostream:
// candidate template ignored: could not match 'basic_ostream'
//      against 'basic_ios'
// using arstream_type = std::ios;

#endif

extern ard_out_stream_type *ard_out_stream;
extern ard_in_stream_type *ard_in_stream;

#endif // Include guard ARD_IOSTREAM_H