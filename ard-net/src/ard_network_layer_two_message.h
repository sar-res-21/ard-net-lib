/**
 * @file ard_network_layer_two_message.h
 * @brief Classes modeling messages at different layers of the communication
 * stack
 *
 */

#ifndef ARD_NET_LAYER_TWO_MSG_H
#define ARD_NET_LAYER_TWO_MSG_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h> // for uint8_t
#else
#include <cstdint> // for uint8_t
#endif

#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_pkt_buffer.h"

using PktBufPtr = ArdUniquePtrMemPool<ArdPktBuffer>;
using PktMemPool = ArdMemPool<ArdPktBuffer>;

/** \addtogroup most_important 
 *  @{
 */

/********************************************************************
 *                    Layer Two Messages                            *
 ********************************************************************/

/**
 * @brief Enumeration representing the different message types.
 */
enum L2MsgTypes
{
  L2_TYP_ACK = 0x00,   ///< Acknowledgment
  L2_TYP_DATA = 0x01,  ///< Data
  L2_TYP_NOTYPE = 0xff ///< Dummy value used by the default constructor to
                       // signal a non-valid header
};

/**
 * @Brief L2Message models a Layer Message
 *
 * To build a message:
 *   - Declare a new L2Message object.
 *   - Fill the member fields (m_dest_add, m_src_add, m_msg_type, m_seq_mun).
 *     You can also use the constructor that takes as input parameters the
 *     values of each field).
 *   - Call serialize with the payload (using ard_move).
 *   - This returns a new memory buffer with the L2 header + the payload.
 *
 * When receiving a message:
 *   - Declare a new L2Message object.
 *   - Call deSerialize with message to decode.
 *   - This returns the payload (to be passed to upper layer)
 *     and fills the member fields (m_dest_add, m_src_add, m_msg_type,
 *     m_seq_mun) based on the data in the message received.
 *
 */
class L2Message
{
public:
  /**
   * @brief Constructs a new L2Message object with default values in each field.
   *
   * This can be used when constructing an L2Message object that will be
   * used to deserialize a packet buffer.
   *
   */
  L2Message();

  /**
   * @brief Constructs an L2Message with given values for each field (only
   * message type and sequence number can be omitted).
   *
   * This constructor can be used whenever all the fields are known at 
   * construction time (e.g., when sending a message).
   *
   * @param a_src_addr The source address.
   * @param a_dst_addr The destination address.
   * @param a_payload_len The payload length (this does not include the L2
   * header length!)
   * @param a_seq_num The sequence number.
   * @param a_msg_type The message type (DATA or ACK)
   */
  L2Message(L2Addr a_src_addr, L2Addr a_dst_addr, uint8_t a_payload_len,
            uint8_t a_seq_num = 0, L2MsgTypes a_msg_type = L2_TYP_DATA);

  /**
   * @brief Serialize a packet.
   *
   * Once every member field has been filled, call this method with
   * the payload in parameter using ard_move
   * (eg. PktBufPtr p2 = l2.serialize(ard_move(p1));).
   *
   * @param a_sdu The payload from upper layer.  This is the return value.
   * @param a_mpool A reference to the mempool to use to allocate the packet
   * buffer for the layer two frame.
   * @return The payload from the upper layer.
   */
  PktBufPtr serialize(PktBufPtr a_sdu,
                      PktMemPool *a_mpool);

  /**
   * @brief Serialize a packet with no payload.
   *
   * Once every member field has been filled, call this method to obtain a
   * packet buffer pointer containing the corresponding representation.
   *
   * @param a_mpool A reference to the mempool to use to allocate the packet
   * buffer for the layer two frame.
   * @return The serialized packet to be passed to lower layer.
   */
  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mpool);

  /**
   * @brief DeSerialize a packet.
   *
   * Call this method with the received payload in parameter
   * using ard_move
   * (eg. PktBufPtr p3 = l2_back.deSerialize(ard_move(p2));).
   * This will fill extract header fields values in corresponding
   * object members (m_dest_add, m_src_add, m_msg_type, m_seq_mun)
   * and return back the payload to be passed to the upper layer.
   *
   * @param pdu The payload from lower layer.
   * @param a_mem_pool A reference to the memory pool to use to allocate the
   * packet
   * buffer for the payload packet buffer.
   * @return The packet payload to be passed to upper layer.
   */
  PktBufPtr deSerialize(PktBufPtr pdu,
                        PktMemPool *a_mem_pool);

  /**
   * @brief Parse an L2Message packet without transfering buffer
   * property (i.e. NOT having to use ard_move()).
   *
   * Call this method with the received payload in parameter
   * (eg. analyze(pdu->data, pdu->curr_size); ).
   * This will fill extract header fields values in corresponding
   * object members ( m_dst_add, m_src_add, m_msg_type, m_seq_num, 
   * m_payload_len).
   *
   * @param a_p_data The pointer to the buffer to parse (pdu->data).
   * @param a_length The length of the buffer to parse (pdu->curr_size).
   * @return True is parsing was successful False otherwise.
   */
  bool analyze(uint8_t *a_p_data, size_t a_length);

  /**
   * @brief Analyzes an L2Message packet and returns its destination 
   * address.
   *
   * Call this method with a pointer to a packet's payload so that you 
   * can have access the destination address without transfering the
   * buffer property with ard_move().
   *
   * @param a_p_data The pointer to the buffer to parse (pdu->data).
   * @return The sender address as an L2Addr object.
   */
  L2Addr static getDstAddr(uint8_t *a_p_data);

  L2Addr m_dst_add;      /// Destination Address
  L2Addr m_src_add;      /// Source Address
  L2MsgTypes m_msg_type; /// Message Type Field
  uint8_t m_seq_num;     /// Sequence Number
  uint8_t m_payload_len; /// The payload length in bytes
  PktBufPtr m_pkt_buff;  /// Pointer to the packet buffer holding the result of the serialization

  constexpr static uint8_t l2_header_size = 5;
  constexpr static uint8_t l2_seq_num_offset = 3;
};

/** @}*/
#endif // end include guard ARD_NET_LAYER_TWO_MSG_H