#include "ard_app_base.h"
#include "ard_network_application_message.h"

AppBase::AppBase(PktMemPool *a_mem_pool, ArdSysInterface *a_ard_sys_int,
                 ArdEventManager *a_event_manager,
                 SendInterface<AppAddr> *a_send_interface, AppAddr a_this_addr)
    : ArdSerialEventHandler(a_ard_sys_int, a_event_manager),
      ArdNetworkLayer(a_mem_pool, a_ard_sys_int, a_event_manager,
                      a_send_interface, a_this_addr),
      m_mem_pool(a_mem_pool), 
      m_local_network_add(GetNetAddress(a_this_addr)),
      m_dest(GetNetAddress(a_this_addr)),
      m_msg_buffer_pos(0), m_pos_in_cmd_parsing(0),
      m_last_rx_ping_number(0), m_ping_timer(a_event_manager, a_ard_sys_int)
{
  m_ping_functor.setParent(this);
}

void AppBase::init() {
  // info_pr(F("[AppBase] Init\n"));
}

//////////////////////////////////////////////////////////////////////
//    South Interface
//////////////////////////////////////////////////////////////////////
void AppBase::onDataReceived(PktBufPtr a_p, AnyAddr a_src_addr,
                             LayerId a_l_id) {
  lib_debug_pr(ARD_F("App received, size: "), int(a_p->curr_size), ARD_F("\n"));
  bool tst;
  processRxData(ard_move(a_p), a_src_addr, &tst);
}

void AppBase::dataHandlingDone(PktBufPtr, bool res) {}

//////////////////////////////////////////////////////////////////////
//    Send
//////////////////////////////////////////////////////////////////////
void AppBase::send(PktBufPtr a_p, AppMsgTypes a_type, AppAddr a_dest) {
  AppMessage msg;
  msg.m_msg_type = a_type;
  PktBufPtr packet = msg.serialize(ard_move(a_p), m_mem_pool);

  if (!m_send_interface) {
    ard_error(ARD_F("AppBase: No Lower layer\n"));
  }
  m_send_interface->sendRequest(ard_move(packet), a_dest);
}

//////////////////////////////////////////////////////////////////////
//    Radio Rx Processing
//////////////////////////////////////////////////////////////////////
PktBufPtr AppBase::processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                 bool *p_result) {
  // lib_debug_pr(ARD_F("AppBase::processRxData: "), a_p->curr_size,
  // ARD_F("\n")); for(int i=0; i<a_p->curr_size; i++){
  //   lib_debug_pr(ARD_F(" "), (int)a_p->data[i]);
  // }
  // lib_debug_pr(ARD_F("\n"));

  unsigned long diff;
  AppMessage appMsg;
  if (!appMsg.analyze(a_p->data, a_p->curr_size)) {
    *p_result = false;
    return ard_move(a_p);
  }

  if (appMsg.m_msg_type == APP_TYP_PING) {
    PktBufPtr sdu = appMsg.deSerialize(ard_move(a_p), m_mem_pool);
    PingMessage pingMsg;
    pingMsg.deSerialize(ard_move(sdu));

    diff = pingMsg.m_seq_num - m_last_rx_ping_number - 1;
    m_last_rx_ping_number = pingMsg.m_seq_num;
    if (diff == 0) {
      info_pr(ARD_F("."));
    } else {
      info_pr(ARD_F(" "), (int)diff);
    }
    *p_result = true;
    return ard_move(a_p);
  }
  *p_result = false;
  return ard_move(a_p);
}

//////////////////////////////////////////////////////////////////////
//    Serial Port UI Management
//////////////////////////////////////////////////////////////////////
void AppBase::RunEventHandler() {
  size_t i;
  // Copy received data into msg Buffer
  for (i = 0; i < m_rx_buffer_pos; i++) {
    info_pr((char)m_rx_buffer[i]); // Echo
    m_msg_buffer[m_msg_buffer_pos] = m_rx_buffer[i];
    if (++m_msg_buffer_pos == m_buffer_max_size)
      m_msg_buffer_pos = 0;
  }

  // Process msg Buffer (ie. look for delimiters)
  static size_t readIndex = 0; // position reached after last read
  unsigned char newBuff[m_buffer_max_size];
  i = readIndex; // index in read buffer
  bool hasWrappedAround =
      false; // to check if m_msg_buffer_pos ha wrapped around
  if (i >= m_msg_buffer_pos)
    hasWrappedAround = true;
  size_t j = 0; // write index in newBuff buff
  while (hasWrappedAround || (i < m_msg_buffer_pos)) {

    if (m_msg_buffer[i] == '\b') // If backspace
    {
      if (j != 0)
        j--;
    } else { // else
      newBuff[j] = m_msg_buffer[i];
      if (newBuff[j] == '\n') {
        // Treat message
        ProcessSerialCmd(newBuff, j);
        j = 0;
        readIndex = i + 1;
        if (readIndex == m_buffer_max_size)
          readIndex = 0;
      } else {
        j++;
      }
    }

    if (++i == m_buffer_max_size) {
      i = 0;
      hasWrappedAround = false;
    }
  }
} // AppBase::RunEventHandler()

bool AppBase::ProcessSerialCmd(unsigned char *msg, size_t msgSize) {
  msg[msgSize] = '\0';
  lib_debug_pr(ARD_F("[AppBase]: Received on serial port: "));
  lib_debug_pr((char *)msg);

  // if the command starts by a number, this is the destination group number
  bool dest_in_cmd = false;
  m_dest = 0;
  size_t i = 0;
  while ((char)msg[i] >= '0' && (char)msg[i] <= '9' && i < msgSize) {
    dest_in_cmd = true;
    m_dest *= 10;
    uint8_t digit = (uint8_t)((char)msg[i] - '0');
    m_dest += digit;
    i++;
  }

   if (!dest_in_cmd)
     m_dest = m_local_network_add;

  while ((char)msg[i] == ' ' && i < msgSize) {
    i++;
  }

  m_pos_in_cmd_parsing = i;

  switch ((char)msg[i]) {
  // Ping
  case 'p':
  case 'P':
    ProcessPingCmd((char *)&msg[i + 1], msgSize - 1);
    return true;
    break;

    // case 'd':
    // case 'D':
    //   lib_debug_pr(ARD_F("   Sending debug message (\"hello!\")"));
    // //   Send((unsigned char*)"hello!", 7);
    //   return true;
    //   break;

  } // END of switch(msg[i])
  return false;

} // AppBase::ProcessSerialCmd

//////////////////////////////////////////////////////////////////////
//    Ping Command
//////////////////////////////////////////////////////////////////////
PingTimerFunctor::PingTimerFunctor() : m_dest(0) {}

void PingTimerFunctor::setParent(AppBase *a_parent) { m_parent = a_parent; }

void PingTimerFunctor::operator()() {
  lib_debug_pr(ARD_F("Ping Timer called\n"));
  lib_debug_pr(ARD_F("\tpingCounter: "), m_parent->pingCounter, ARD_F("\n"));
  lib_debug_pr(ARD_F("\tdest: "), m_dest, ARD_F("\n"));

  PingMessage msg;
  msg.m_seq_num = m_parent->pingCounter;
  PktBufPtr buffer = msg.serialize(m_parent->m_mem_pool);
 
  m_parent->send(
      ard_move(buffer), APP_TYP_PING,
      GetAppAddress(APP_DEV_BCAST,m_parent->m_local_network_add) 
      ); // Network limited broadcast

  m_parent->pingCounter++;
}

void AppBase::ProcessPingCmd(char *msg, size_t msgSize) {
  if (msgSize < 1)
    return; // no  command

  int pingPeriod = 0;
  int test = sscanf(msg, "%d", &pingPeriod);
  if (test == 0) {
    return;
  }

  if (pingPeriod == 0) { // Stop currently running ping
    lib_debug_pr(ARD_F(" Stop timer\n"));
    m_ping_timer.stopTimer();
    pingCounter = 1;
    return;
  }

  info_pr(ARD_F(" Start Ping timer, ping period="), pingPeriod,
          ARD_F(" dest network="), m_dest, ARD_F("\n"));
  m_ping_functor.m_dest = m_dest;
  m_ping_timer.startTimer(pingPeriod, &m_ping_functor, true);
} // AppBase::ProcessPingCmd