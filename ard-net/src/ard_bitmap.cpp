#include "ard_bitmap.h"
#include "ard_error.h"
#include "ard_log.h"

ArdBitMap::ArdBitMap(const uint8_t size)
    : num_bits(size), num_bytes(((size % 8) == 0) ? size / 8 : (size / 8 + 1)) {
  if (size > ArdNetConstants::bitmap_max_size) {
    error_pr(ARD_F("ArdBitMap: requested size ("), int(size),
             ARD_F(") too large, maximum value: "),
             int(ArdNetConstants::bitmap_max_size), "\n");
    ard_error(ARD_F("ArdBitMap requtested size too large\n"));
  }
  bitmap = new uint8_t[num_bytes];
  debug_pr(ARD_F("New bitmap constructed\n"));
  debug_pr(ARD_F("Size: "), int(size), ARD_F(" num bytes: "), int(num_bytes),
           "\n");
  for (int i = 0; i < num_bytes; i++)
    bitmap[i] = 0;
};

ArdBitMap::~ArdBitMap() {
  debug_pr(ARD_F("Deleting bitmap\n"));
  delete[] bitmap;
}

bool ArdBitMap::is_bit_set(uint8_t n) {
  if (n >= num_bits) {
    error_pr(ARD_F("ArdBitMap::ist_bit_set, invalid index, max="), int(num_bits),
             ARD_F(" given="), int(n), "\n");
    ard_error(ARD_F("is_bit_set invalid index"));
  }
  uint8_t byte = n / 8;
  uint8_t bit = n % 8;
  return ((bitmap[byte] & (1 << bit)) >> bit) == 1;
}

void ArdBitMap::set_bit(uint8_t n) {
  if (n >= num_bits) {
    error_pr(ARD_F("ArdBitMap::set_bit, invalid index, max="), int(num_bits),
             ARD_F(" given="), int(n), "\n");
    ard_error(ARD_F("set_bit invalid index"));
  }
  if (is_bit_set(n)) {
    error_pr(ARD_F("ArdBitMap::set_bit, setting a bit that is already sed, index="),
             int(n), "\n");
    ard_error(ARD_F("set_bit trying to set a bit that it already set"));
  }
  uint8_t byte = n / 8;
  uint8_t bit = n % 8;
  bitmap[byte] |= (1 << bit);
}

void ArdBitMap::unset_bit(uint8_t n) {
  if (n >= num_bits) {
    error_pr(ARD_F("ArdBitMap::unset_bit, invalid index, max="), int(num_bits),
             ARD_F(" given="), int(n), "\n");
    ard_error(ARD_F("ArdBitMap unset_bit: invalid index\n"));
  }
  if (!is_bit_set(n)) {
    error_pr(ARD_F("ArdBitMap::unset_bit, setting a bit that is already sed, index="),
             int(n), "\n");
    ard_error(ARD_F("ArdBitMap unset_bit: bit was not set\n"));
  }
  uint8_t byte = n / 8;
  uint8_t bit = n % 8;
  bitmap[byte] &= ~(1 << bit);
}

uint8_t ArdBitMap::find_unset_bit() {
  // Stupid brute force algorithm but it should work :)
  uint8_t i = 0;

  while (i < num_bits) {
    if (!is_bit_set(i))
      return i;
    i++;
  }
  return ArdNetConstants::bitmap_full;
}
