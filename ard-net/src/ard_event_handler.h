/**
 * @file ard_event_handler.h
 * @author Alberto Blanc
 * @brief Simple event handling system for Arduino.  These classes are meant
 * only to handle events, not to manage the "event loop" itself.  The
 * EventManager does that.
 *
 * @copyright Copyright (c) 2019
 *
 */
#ifndef ARD_EVENTHANDLER_H
#define ARD_EVENTHANDLER_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else
#include <cstdint> // for uint8_t
#endif
// see https://en.cppreference.com/w/cpp/types/integer

#include "ard_callbacks.h"
#include "ard_mempool.h"
#include "ard_pkt_buffer.h"
#include "ard_sys_interface.h"

namespace ArdNetConstants {
constexpr uint8_t srl_evnt_hndlr_buff_size =
    8; /**< Serial Eventhandler buffer size (in bytes)*/
}

// FIXME: make sure that we need this
class ArdNetworkLayerTwoThreeTxs; // Forward class definition (temporary?)

class ArdEventManager;

class ArdEventHandler {
public:
  virtual bool CheckForEvent() = 0;
  virtual void RunEventHandler() = 0;
};

/**
 * @brief Timer event handler.  It stores the absolute deadline
 * and a reference to the functor that must be called when the timer fires.
 *
 * @note  This class stores only a pointer to the functor.  In other words,
 * somebody else is in charge of ensuring that this reference will still be
 * valid when the timer fires and that it is properly disposed after.
 */
class ArdTimerEventHandler : public ArdEventHandler {
  friend class ArdEventManager;

public:
  /**
   * @brief Construct an empty (and unusable) Ard Timer Event Handler object.
   *
   * @param a_event_manager Pointer to the event manager.  This way the timer
   * handlers can register themselves when they are activated (with startTimer.)
   *
   * @param a_ard_sys_int Pointer to the system interface, needed to access
   * millis()
   *
   * @note The object, as constructed, is not yet usable.  One must call
   * seTimer to set all parameters needed for this object to be useful
   *
   * Because we need to statically allocate all the objects that we need, one
   * has to instantiate a timer event handler before it is actually needed.
   * While it is possible to add enough parameters to other constructors in
   * order to be able to construct a fully functional timer at the very
   * beginning, certain parameters, notably the delay, do not make sense.
   */
  ArdTimerEventHandler(ArdEventManager *a_event_manager,
                       ArdSysInterface *a_ard_sys_int);

  /**
   * @brief Start this timer with the arguments passed to this method.
   *
   * @param a_delay The relative deadline in milliseconds.
   * @param a_timer_func Pointer to the TimerFunctor whose operator() will be
   * executed when the timer fires.
   * @param a_loop Set to true if this is a looping call.  I.e., the task will
   * be executed every a_delay milliseconds, starting when the event handler is
   * first created.
   */
  void startTimer(uint16_t a_delay, TimerFunctor *a_timer_func,
                  bool a_loop = false);
  /**
   * @brief Stop this timer.
   *
   * For the time being, this method does not check whether this timer is
   * running or not.
   */
  void stopTimer();
  virtual ~ArdTimerEventHandler() = default;
  bool CheckForEvent() override;
  void RunEventHandler() override;

private:
  /// The absolute deadline in milliseconds (this value can be compared with
  /// what millis returns).
  unsigned long m_abs_deadline;
  bool m_loop;
  uint16_t m_delay;
  TimerFunctor *m_timer_func;
  ArdEventManager *m_event_manager;
  ArdSysInterface *m_ard_sys_int;
  bool m_active;
};

class ARDRF12EventHandler : public ArdEventHandler {
public:
  ARDRF12EventHandler(ArdNetworkLayerOne *m_net_layer_one,
                      ArdSysInterface *a_ard_sys_int);
  virtual ~ARDRF12EventHandler() = default;
  bool CheckForEvent() override;
  void RunEventHandler() override;

private:
  ArdNetworkLayerOne *m_net_layer_one;
  ArdSysInterface *m_ard_sys_int;
};


#endif // End of include guard ARD_EVENTHANDLER_H