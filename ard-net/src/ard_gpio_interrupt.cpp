#include "ard_gpio_interrupt.h"
#include "ard_log.h"
#include <Arduino.h>
#include <EnableInterrupt.h>


///////////////////////////////////////////////////////////////////////
///    Some uggly stuffs about interrupt management....
///////////////////////////////////////////////////////////////////////
// To increase/decrease the number of handlers:
//    - add/remove NULLs in  hcsr04Parents
//    - declare new / remove _isrXXX functions
//    - add their pointers in isrTable
GpioInterrupt *g_gpio_parents[]={NULL, NULL, NULL, NULL};
const size_t MAX_NB_GPIO = sizeof(g_gpio_parents) / sizeof(*g_gpio_parents);
void _gpio_isr1() {
    g_gpio_parents[0]->_isr();
}
void _gpio_isr2() {
    g_gpio_parents[1]->_isr();
}
void _gpio_isr3() {
    g_gpio_parents[2]->_isr();
}
void _gpio_isr4() {
    g_gpio_parents[3]->_isr();
}
void (*const g_gpioIsrTable[])() = {
    _gpio_isr1,
    _gpio_isr2,
    _gpio_isr3,
    _gpio_isr4,
};

void( *allocGpioInterrupt(GpioInterrupt *thisPointer))(){
  size_t i;
  for(i=0; i<MAX_NB_GPIO && g_gpio_parents[i]!=NULL; i++); // Look for 1st NULL in g_gpio_parents

  if(i >= MAX_NB_GPIO){ // could return NULL
      error_pr(ARD_F("Too many GPIOs"));
  }

  g_gpio_parents[i] = thisPointer;
  return g_gpioIsrTable[i];
}
void freeGpioInterrupt(GpioInterrupt *thisPointer){
  size_t i;
  for(i=0; i<MAX_NB_GPIO && g_gpio_parents[i]!=thisPointer; i++); // Look for thisPointer in g_gpio_parents

  if( i<MAX_NB_GPIO && g_gpio_parents[i]==thisPointer)
      g_gpio_parents[i] = NULL;
}



///////////////////////////////////////////////////////////////////////
///    The Class
///////////////////////////////////////////////////////////////////////
GpioInterrupt::GpioInterrupt():
   m_last_call_ts(0)
{}

GpioInterrupt::~GpioInterrupt(){
  freeGpioInterrupt(this);
}

void GpioInterrupt::init(int a_pin, TriggerMode a_trig_mode, GpioFunctor* a_callback_functor){
  m_ioPin = a_pin;
  m_callback_functor = a_callback_functor;

  // Configure pins
  pinMode(m_ioPin, INPUT);

  // Attach isr
  void (*pIsr)() = allocGpioInterrupt(this);
  int trig = TriggerMode2EnableInterrupt(a_trig_mode);
  enableInterrupt(m_ioPin, pIsr, trig);
}


void GpioInterrupt::_isr()
{
  // De-bouncing...
  unsigned long ts = millis();
  if( ts < m_last_call_ts + m_debouncing_interval_in_ms) return;
  m_last_call_ts = ts;

  // Call the callback
  (*m_callback_functor)();
}

int GpioInterrupt::TriggerMode2EnableInterrupt(TriggerMode a_trig_mode){
  switch (a_trig_mode)
  {
  case TriggerMode::Falling:
    return FALLING;
    break;
  case TriggerMode::Rising:
    return RISING;
    break;
  case TriggerMode::Change:
  default:
    return CHANGE;
    break;
  }
}