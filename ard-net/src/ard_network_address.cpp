#include "ard_network_address.h"
#include "ard_log.h"

/********************** Address Translation  January 2020  *******************/
// MUST PUT THESE IN THE .cpp FILE AND NOT THE .h (otherwise the compiler
// complains about multiple definitions (because these are not templates
// anymore)

template <>
L3Addr mapAddr<AppAddr, L3Addr>(AppAddr a_addr, AppAddr a_this_addr) {
  uint16_t net_add = ((a_this_addr.m_addr & 0xF0) << 4) & 0xFF00;
  uint8_t app_add = a_addr.m_addr & 0x0F;

  // If the target device is the OCC (don't care about the network),
  // the network is the OCC network
  if (app_add == APP_DEV_OCC) 
    net_add = ArdNetConstants::occ_network_number << 8;

  // info_pr(ARD_F("App to L3:"), a_addr.m_addr, ARD_F(", "), a_this_addr.m_addr, ARD_F("\n") );
  // info_pr(ARD_F("  L3 add:"), net_add + app_add, ARD_F("\n") );

  return L3Addr(net_add + app_add);
}

template <> AppAddr mapAddr<L3Addr, AppAddr>(L3Addr a_addr, L3Addr a_this_addr){
  uint8_t netAdd = (uint8_t)((a_addr.m_addr & 0xFF00) >> 8);
  uint8_t devAdd = (uint8_t)(a_addr.m_addr & 0x0F);

  uint8_t dev = devAdd;
  uint8_t net = (netAdd & 0x0F) << 4;

  return AppAddr(net + dev);
}

template <> L2Addr mapAddr<L3Addr, L2Addr>(L3Addr a_addr, L3Addr a_this_addr) {
  uint8_t myNetAdd = (uint8_t)((a_this_addr.m_addr & 0xFF00) >> 8);
  uint8_t netAdd = (uint8_t)((a_addr.m_addr & 0xFF00) >> 8);
  uint8_t devAdd = (uint8_t)(a_addr.m_addr & 0x0F);

  uint8_t dev;
  uint8_t net = (myNetAdd & 0x0F) << 4;

  if (netAdd != ArdNetConstants::occ_network_number) {
    dev = devAdd;
  } else {
    dev = (uint8_t)L2_DEV_GATEWAY;
  }

  // info_pr(ARD_F("L3 to L2:"), a_addr.m_addr, ARD_F(", "), a_this_addr.m_addr, ARD_F("\n") );
  // info_pr(ARD_F("  netAdd:"), netAdd, ARD_F(", occNet:"), ArdNetConstants::occ_network_number, ARD_F("\n") );
  // info_pr(ARD_F("  L2 add:"), net+dev, ARD_F("\n") );

  return L2Addr(net + dev);
}

// App -> L2
template <>
L2Addr mapAddr<AppAddr, L2Addr>(AppAddr a_addr, AppAddr a_this_addr) {
  // info_pr(ARD_F("App to L2:"), a_addr.m_addr, ARD_F(", "), a_this_addr.m_addr, 
  //         ARD_F("\n") );
  return (L2Addr(a_addr.m_addr));
}
// L2 -> App
template <> AppAddr mapAddr<L2Addr, AppAddr>(L2Addr a_addr, L2Addr a_this_addr){
  // info_pr(ARD_F("L2 to App:"), a_addr.m_addr, ARD_F(", "), a_this_addr.m_addr, 
  //         ARD_F("\n") );
  return AppAddr(a_addr.m_addr);
}


// L2 -> L1
template <> L1Addr mapAddr<L2Addr, L1Addr>(L2Addr a_addr, L2Addr m_this_addr) {
  return L1Addr();
}
// L3 -> L1
template <> L1Addr mapAddr<L3Addr, L1Addr>(L3Addr a_addr, L3Addr m_this_addr) {
  return L1Addr();
}

// Any -> App
template <> AppAddr mapAddr<AnyAddr, AppAddr>(AnyAddr a_addr, AnyAddr m_this_addr) {
  switch (a_addr.m_addr_type) {
  case AddrType::L1:
    return GetAppAddress(APP_DEV_BCAST, 0xF0);
  case AddrType::L2:
    return mapAddr<L2Addr, AppAddr>(a_addr.m_l2_addr, a_addr.m_l2_addr);
  case AddrType::L3:
    return mapAddr<L3Addr, AppAddr>(a_addr.m_l3_addr, a_addr.m_l3_addr);
  case AddrType::App:
    return (a_addr.m_app_addr);
  default:
    return AppAddr(0);
  }
}

/********************* Comparison operator for AnyAddr *********************/
bool operator==(const AnyAddr &a1, const AnyAddr &a2) {
  if (a1.m_addr_type != a2.m_addr_type) {
    return false;
  }
  switch (a1.m_addr_type) {
  case AddrType::L1:
    return (a1.m_l1_addr == a2.m_l1_addr);
  case AddrType::L2:
    return (a1.m_l2_addr == a2.m_l2_addr);
  case AddrType::L3:
    return (a1.m_l3_addr == a2.m_l3_addr);
  case AddrType::App:
    return (a1.m_app_addr == a2.m_app_addr);
  default:
    return false;
  }
}

AppAddr GetAppAddress(uint8_t a_dev, uint8_t a_network){
  uint8_t net = ((a_network & 0x0F) << 4) & 0xF0;
  uint8_t dev = a_dev & 0x0F;
  // info_pr(ARD_F("GetAppAddress "), a_dev, ARD_F(", "), a_network,
  //         ARD_F(" net:"), net, ARD_F(", dev:"), dev,
  //         ARD_F(" =>"), net+dev,
  //         ARD_F("\n"));
  return AppAddr(net + dev);
}
uint8_t GetNetAddress(AppAddr a_app_addr){
  return (a_app_addr.m_addr & 0xF0) >> 4;
}
uint8_t GetDevAddress(AppAddr a_app_addr){
  return (a_app_addr.m_addr & 0x0F);
}


L3Addr GetL3Address(AppDevices a_dev, uint8_t a_network) {
  uint16_t net_add = (a_network << 8) & 0xFF00;
  lib_debug_pr(ARD_F("net_add = "), net_add, ARD_F(" net = "), a_network,
               ARD_F("\n"));
  switch (a_dev) {
  case APP_DEV_CONTROLLER:
  case APP_DEV_TRAIN:
  case APP_DEV_SWITCH:
  case APP_DEV_BCAST:
    return L3Addr(net_add + uint8_t(a_dev));
    break;
  case APP_DEV_OCC:
    return L3Addr(ArdNetConstants::occ_network_number<<8 + uint8_t(a_dev));
    break;
  default:
    error_pr(ARD_F("GetL3Address called with wrong device type\n"));
  } // END of switch(a_dev)
  return L3Addr(net_add + uint8_t(APP_DEV_BCAST));
}

uint8_t GetNetwork(L3Addr a_l3_add) {
  uint8_t netAdd = (uint8_t)(a_l3_add.m_addr >> 8);
  return netAdd;
}

L2Addr GetL2Address(L3Addr a_l3_add, uint8_t a_local_network_number) {
  uint8_t netAdd = (uint8_t)(a_l3_add.m_addr >> 8);
  uint8_t devAdd = (uint8_t)(a_l3_add.m_addr & 0xFF);

  uint8_t net;
  // lib_debug_pr(ARD_F("GetL2Address: "), netAdd,
  //              ARD_F(", a_local_network_number: "), a_local_network_number,
  //              ARD_F(", devAdd: "), devAdd,
  //              ARD_F("\n") );

  if (netAdd == a_local_network_number) {
    net = (netAdd << 4) & 0xF0;
  } else {
    net = (a_local_network_number << 4) & 0xF0;
    devAdd = (uint8_t)L2_DEV_GATEWAY;
  }
  return L2Addr(net + devAdd);

} // GetL2Address

L2Addr GetL2Address(AppDevices a_dev, uint8_t a_network) {
  uint8_t net = (a_network << 4) & 0xF0;
  switch (a_dev) {
  case APP_DEV_CONTROLLER:
    return L2Addr(net + L2_DEV_CONTROLLER);
    break;
  case APP_DEV_TRAIN:
    return L2Addr(net + L2_DEV_TRAIN);
    break;
  case APP_DEV_SWITCH:
    return L2Addr(net + L2_DEV_SWITCH);
    break;
  case APP_DEV_GATEWAY:
    return L2Addr(net + L2_DEV_GATEWAY);
    break;
  case APP_DEV_BCAST:
    return L2Addr(net + L2_DEV_BCAST);
    break;
  default:
    error_pr("GetL2Address called with wrong device type\n");
  } // END of switch(a_dev)
  return L2Addr(net + 0xF);
} // GetL2Address

L2Addr GetL2Address(L3Addr a_l3_add) {
  uint8_t netAdd = (uint8_t)(a_l3_add.m_addr >> 8);
  uint8_t devAdd = (uint8_t)(a_l3_add.m_addr & 0xFF);

  if (netAdd > 0xF) {
    error_pr(ARD_F("Networks with number > 15 not managed!\n"));
    netAdd = 0xF;
  }

  uint8_t net = (netAdd << 4) & 0xF0;
  return L2Addr(net + devAdd);
} // GetL2Address
