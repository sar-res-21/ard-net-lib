/**
 * @file ard_app_base.h
 * @brief Base Class for the application on top of the
 * communication stack
 *
 */

#ifndef ARD_APP_BASE_H
#define ARD_APP_BASE_H

#include "ard_callbacks.h" // For functors
#include "ard_network_address.h"
#include "ard_network_application_message.h"
#include "ard_serial_ui.h"
#include "ard_network_layer_base.h"

class AppBase;

class PingTimerFunctor : public TimerFunctor {
  friend class AppBase;

public:
  PingTimerFunctor();

  void operator()();
  void setParent(AppBase *a_parent);

protected:
  AppBase *m_parent;
  int m_dest;
};

class AppBase : public ArdNetworkLayer<AppAddr>,
                public ArdSerialEventHandler,
                public ArdNetSouthInterface {
  friend class PingTimerFunctor;

public:
  AppBase(PktMemPool *a_mem_pool, ArdSysInterface *a_ard_sys_int,
          ArdEventManager *a_event_manager,
          SendInterface<AppAddr> *a_send_interface, AppAddr a_this_addr);
  virtual void init(); /// Call this from the setup() function

  virtual void RunEventHandler();
  virtual void onDataReceived(PktBufPtr, AnyAddr, LayerId) override;
  virtual void dataHandlingDone(PktBufPtr, bool res) override;
  void send(PktBufPtr a_p, AppMsgTypes a_type, AppAddr a_dest);

protected:
  ArdMemPool<ArdPktBuffer> *m_mem_pool;
  uint8_t m_local_network_add;

  // Serial Port UI Processing
  uint8_t m_msg_buffer[m_buffer_max_size];
  size_t m_msg_buffer_pos;
  virtual bool ProcessSerialCmd(unsigned char *msg, size_t msgSize);
  uint8_t m_dest; /// Destination parsed from current command (used by inheriting
              /// classes)
  size_t m_pos_in_cmd_parsing; /// Position in currently parsed command  (used
                               /// by inheriting classes)

  // Rx Radio Message Processing
  virtual PktBufPtr processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                  bool *p_result);

  // Ping functions
  // public:
protected:
  unsigned long pingCounter = 1;
  unsigned long m_last_rx_ping_number = 0;

protected:
  virtual void ProcessPingCmd(char *msg, size_t msgSize);
  ArdTimerEventHandler m_ping_timer;
  PingTimerFunctor m_ping_functor;
};

#endif // end include guard ARD_APP_BASE_H
