/*
* We declare all the global variables here.
*/

#include "ard_iostream.h"


#ifdef ARDUINO_AVR_NANO

#include <Arduino.h>


int print_base = DEC;

ard_out_stream_type *ard_out_stream = &Serial;
ard_in_stream_type *ard_input_stream = &Serial;

#else

ard_out_stream_type *ard_out_stream = &std::cout;
// ard_input_stream_type * ar_input_stream = &std::cin;


// Global variables used by the radio module
// From rf12.h
uint16_t rf12_crc = 0;
uint8_t    rf12_len = 4;
uint8_t rf12_data[] ={1,2,3,4};


#endif

