/**
 * @file ard_app_central_controller.h
 * @author Christophe Couturier
 * @brief Class for the application of the central controller
 *
 */

#ifndef ARDAPP_CENTRAL_CONTROLLER_H
#define ARDAPP_CENTRAL_CONTROLLER_H

#include "ard_app_base.h"
#include "ard_network_application_message.h"

class AppCentralController : public AppBase {
  friend class PingTimerFunctor;

public:
  AppCentralController(PktMemPool *a_mem_pool, ArdSysInterface *a_ard_sys_int,
                       ArdEventManager *a_event_manager,
                       SendInterface<AppAddr> *a_send_interface,
                       AppAddr a_this_addr);
  virtual void init(); /// Call this from the setup() function

  // Serial Port UI Processing
protected:
  virtual bool ProcessSerialCmd(unsigned char *msg, size_t msgSize);
  void ProcessTrainCmd(char *msg, size_t msgSize);
  void ProcessSwitchCmd(char *msg, size_t msgSize);
  void ProcessRequest(char *msg, size_t msgSize);

public: // parsing functions can be used by train and switch applications
  static bool ParseTrainCommand(char *msg, size_t msgSize,
                                TrainControlMessage *tc);
  static bool ParseSwitchCommand(char *msg, size_t msgSize,
                                 SwitchControlMessage *sc);

  // Rx Radio Message Processing
protected:
  virtual PktBufPtr processRxData(PktBufPtr a_p, AnyAddr a_src_addr,
                                  bool *p_result);
  void sendSwitchStatusRequest();
  void sendTrainStatusRequest();
};

#endif // end include guard ARDAPP_CENTRAL_CONTROLLER_H
