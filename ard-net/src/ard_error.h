/**
 * @file ard_error.h
 * @brief Global error function called whenever there is an unexpected error.
 *
 * Given that there are not exceptions in Arduino, we call this function
 * whenever there is an error.  It cancels all events and makes the led start
 * blinking. It also prints the error message on the serial console.
 *
 */
#ifndef ARD_ERROR_H
#define ARD_ERROR_H

#ifdef ARDUINO_AVR_NANO
#include <Arduino.h>
using ard_string_type = __FlashStringHelper;

#else

using ard_string_type = char;

#include <exception>
#include <iostream>

class SimpleException : public std::exception {
public:
  SimpleException(const ard_string_type *msg) : m_msg(msg) {
    std::cout << "We have a problem: " << m_msg << std::endl;
  }

  virtual const char *what() const throw() { return m_msg; }

  const ard_string_type *m_msg;
};

#endif

void ard_error(const ard_string_type *);

#endif // End of include guard ARD_ERROR_H
