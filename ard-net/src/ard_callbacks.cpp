
#include "ard_callbacks.h"

BlinkLEDFunc::BlinkLEDFunc(ArdSysInterface *a_ard_sys_int)
    : m_state(BlinkLEDFunc::LEDState::OFF), m_ard_sys_int(a_ard_sys_int) {}


void BlinkLEDFunc::operator()() {
  if (m_state == LEDState::OFF) {
    m_ard_sys_int->arDigitalWrite(LED_JNODE, LOW);
    // digitalWrite(LED_BUILTIN, LOW);
    m_state = LEDState ::ON;
  } else {
    m_ard_sys_int->arDigitalWrite(LED_JNODE, HIGH);
    m_state = LEDState ::OFF;
  }
}