/**
 * @file ard_sys_interface_mock.h
 * @brief The system interface mock.
 */
#ifndef ARD_SYS_INTERFACE_MOCK_H
#define ARD_SYS_INTERFACE_MOCK_H

#include "ard_sys_interface.h"
#include "gmock/gmock.h"

/**
 * @brief The system interface mock.
 *
 * It mocks only a subset of the methods for the time being.
 */
struct ArdSysInterfaceImplMock : public ArdSysInterfaceImpl {
  MOCK_METHOD0(getMillis, unsigned long());
  MOCK_METHOD0(radioCanSend, bool());
  MOCK_METHOD3(radioSendStart, void(uint8_t hdr, const void *ptr, uint8_t len));
  MOCK_METHOD2(arDigitalWrite, void(uint8_t, uint8_t));
};

#endif // ARD_SYS_INTERFACE_MOCK_H
