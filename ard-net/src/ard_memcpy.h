/**
 * @file ard_memcpy.h
 * @brief Handles platform-dependent memcpy
 *
 * Not clear why we need two different implementations.  The system include
 * files are different but that's it.  I forgot why I did this, maybe linker
 * problems?
 */

#ifndef ARD_MEMCPY_H
#define ARD_MEMCPY_H

#ifdef ARDUINO_AVR_NANO
#include <string.h> // for size_t
#else
#include <cstring> // for size_t
#endif

void *ard_memcpy(void *dest, const void *src, size_t count);
#endif // ARD_MEMCPY_H
