/**
 * @file ard_network_layer_one.h
 * @brief The network layer one (i.e., the lowest one).
 */
#ifndef ARD_NETWORK_LAYER_H
#define ARD_NETWORK_LAYER_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else

#include <cstdint> // for uint8_t

#endif

#include "ard_callbacks.h"

#include "ard_callbacks.h"
#include "ard_event_handler.h"
#include "ard_event_manager.h"
#include "ard_network_address.h"
#include "ard_network_layer_base.h"
#include "ard_pkt_buffer.h"
#include "ard_sys_interface.h"

using PktBufPtr = ArdUniquePtrMemPool<ArdPktBuffer>;
using PktMemPool = ArdMemPool<ArdPktBuffer>;

/**
 * @brief The network layer one.
 *
 * It implement only the ArdNetNorthInterface as it is the lowest layer, i.e.,
 * the one "talking" with the physical device.
 */
class ArdNetworkLayerOne : public ArdNetworkLayer<L1Addr >,
                           public ArdNetNorthInterface<L1Addr> {
public:
  explicit ArdNetworkLayerOne(PktMemPool *a_mpool,
                              ArdSysInterface *a_ard_sys_int,
                              ArdEventManager *a_event_manager,
                              L1Addr a_this_addr);
  /**
   * @brief Implementation of sendRequest method of the ArdNetNorthInterface
   *
   * @param a_p A packet buffer with the message to be sent.
   * @param a_l1_addr This is actually never used as there are no layer one
   * addresses.  L1Addr is defined as an empty struct.  This way the
   * interface is always the same for every layer.
   */
  void sendRequest(PktBufPtr a_p, L1Addr a_l1_addr) override;
  /**
   * @brief Called by the ARDRF12EventHandler::RunEventHandler to retrieve
   * the data from the radio.  It then calls onDataReceived.
   *
   * This way all the "network-related" elements are in this class rather
   * than in the event handler class.  In particular, this way we avoid
   * having the handler deal with memory and packet buffers.
   *
   */
  void retrieveRadioFrame();

private:
  SendPktBufferLOneFunc m_timer_func;
  ArdTimerEventHandler m_timer_handler;
};


#endif // End of include guard ARD_NETWORK_LAYER_H