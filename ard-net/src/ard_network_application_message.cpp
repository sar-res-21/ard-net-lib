#include "ard_network_application_message.h"
#include "ard_memcpy.h"

/********************************************************************
 *                    Application Messages                          *
 ********************************************************************/
/////////////////////////////////
/// The Base
////////////////////////////////
AppMessage::AppMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
AppMessage::serialize(PktBufPtr sdu, PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->data[0] = (uint8_t)m_msg_type;
  ard_memcpy(&pdu->data[app_header_size], sdu->data, sdu->curr_size);

  pdu->curr_size = app_header_size + sdu->curr_size;
  return (pdu);
}

bool AppMessage::analyze(uint8_t *a_p_data, size_t a_length){
    // Sanity checks
    if (a_length < app_header_size){
        ard_error(ARD_F("Application Message too short\n"));
        return false;
    }
    uint8_t typ = a_p_data[0];
    if ( (typ != 0x00) && (typ != 0x01) && (typ != 0x02)
      && (typ != 0x11) && (typ != 0x12)  
      && (typ != 0x21) && (typ != 0x22) )
    {
        ard_error(ARD_F("Application message with an unknown message type\n"));
        return false;
    }
    m_msg_type = (AppMsgTypes)a_p_data[0];
    return true;    
}

ArdUniquePtrMemPool<ArdPktBuffer>
AppMessage::deSerialize(PktBufPtr pdu, PktMemPool *a_mem_pool) {
  // Sanity checks
  if (!analyze(pdu->data, pdu->curr_size)) {
    ard_error(ARD_F("Invalid Applicatiton message\n"));
  }

  ArdUniquePtrMemPool<ArdPktBuffer> sdu = a_mem_pool->AllocateSlot();
  sdu->curr_size = pdu->curr_size - app_header_size;
  ard_memcpy(sdu->data, &pdu->data[app_header_size], sdu->curr_size);
  return (sdu);
}


/////////////////////////////////
/// Ping Message
////////////////////////////////
PingMessage::PingMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
PingMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  uint32_t big_endian_seq = htonl(m_seq_num);
  ard_memcpy(pdu->data, &big_endian_seq, sizeof(big_endian_seq));

  pdu->curr_size = ping_msg_size;
  return (pdu);
}

void PingMessage::deSerialize(PktBufPtr pdu)
{
  // Sanity checks
  if (pdu->curr_size<ping_msg_size){
    ard_error(ARD_F("Ping Message too short\n"));
  }

  // m_seq_num = ntohl( *( (uint32_t*)pdu->data)  );
  uint32_t *p = (uint32_t*)(pdu->data);
  m_seq_num = ntohl(*p);
}


/////////////////////////////////
// Train Control Message
////////////////////////////////
TrainControlMessage::TrainControlMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
TrainControlMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  uint32_t big_endian_dur = htons(m_duration);
  pdu->data[0] = (uint8_t)m_direction;
  ard_memcpy(&pdu->data[1], &big_endian_dur, sizeof(big_endian_dur));
  pdu->data[3] = m_speed;

  pdu->curr_size = train_control_msg_size;

  return (pdu);
}

bool TrainControlMessage::analyze(uint8_t *a_p_data, size_t a_length){
  // Sanity checks
  if (a_length<train_control_msg_size){
    ard_error(ARD_F("Train Control Message too short\n"));
    return false;
  }

  if ( (a_p_data[0] != 0x00)
       && (a_p_data[0] != 0x01)
       && (a_p_data[0] != 0x02) )
  {
    ard_error(ARD_F("Train Control message with invalid direction\n"));
    return false;
  } else {
    m_direction = (TrainDirections)a_p_data[0];
  }

  m_speed = a_p_data[3];
  m_duration = ntohs( *( (uint16_t*)&a_p_data[1])  );
  return true;
}


// void TrainControlMessage::deSerialize(PktBufPtr pdu)
// {
//   // Sanity checks
//   if (pdu->curr_size<train_control_msg_size){
//     ard_error(ARD_F("Train Control Message too short\n"));
//   }

//   if ( (pdu->data[0] != 0x00)
//        && (pdu->data[0] != 0x01)
//        && (pdu->data[0] != 0x02) )
//   {
//     ard_error(ARD_F("Train Control message with invalid direction\n"));
//   } else {
//     m_direction = (TrainDirections)pdu->data[0];
//   }

//   m_speed = pdu->data[3];

//   m_duration = ntohs( *( (uint16_t*)&pdu->data[1])  );
// }


/////////////////////////////////
// Train Status Message
////////////////////////////////
TrainStatusMessage::TrainStatusMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
TrainStatusMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->data[0] = (uint8_t)m_direction;
  pdu->data[1] = m_speed;

  pdu->curr_size = train_status_msg_size;
  return (pdu);
}

bool TrainStatusMessage::analyze(uint8_t *a_p_data, size_t a_length){
  // Sanity checks
  if (a_length < train_status_msg_size){
      ard_error(ARD_F("Train Status Message too short\n"));
      return false;
  }

  if ( (a_p_data[0] != 0x00)
       && (a_p_data[0] != 0x01)
       && (a_p_data[0] != 0x02) )
  {
      ard_error(ARD_F("Train Status message with invalid direction\n"));
      return false;
  } else {
      m_direction = (TrainDirections)a_p_data[0];
  }

  m_speed = a_p_data[1];
  return true;
}
// void TrainStatusMessage::deSerialize(PktBufPtr pdu)
// {
//   // Sanity checks
//   if (pdu->curr_size<train_status_msg_size){
//     ard_error(ARD_F("Train Status Message too short\n"));
//   }

//   if ( (pdu->data[0] != 0x00)
//        && (pdu->data[0] != 0x01)
//        && (pdu->data[0] != 0x02) )
//   {
//     ard_error(ARD_F("Train Status message with invalid direction\n"));
//   } else {
//     m_direction = (TrainDirections)pdu->data[0];
//   }

//   m_speed = pdu->data[1];
// }

/////////////////////////////////
// Train Status Request Message
////////////////////////////////
TrainStatusRequestMessage::TrainStatusRequestMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
TrainStatusRequestMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->curr_size = train_status_request_msg_size;

  return (pdu);
}

/////////////////////////////////
// Switch Control Message
////////////////////////////////
SwitchControlMessage::SwitchControlMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
SwitchControlMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->data[0] = m_toggle;
  pdu->data[1] = m_direction;

  pdu->curr_size = switch_control_msg_size;

  return (pdu);
}

bool SwitchControlMessage::analyze(uint8_t *a_p_data, size_t a_length){
  // Sanity checks
  if (a_length<switch_control_msg_size){
    ard_error(ARD_F("Train Control Message too short\n"));
    return false;
  }

  m_toggle = a_p_data[0];
  m_direction = a_p_data[1];

  if ( (m_toggle != 0x00)
       && (m_toggle != 0x01) )
  {
    ard_error(ARD_F("Switch Status message with invalid toggle value\n"));
    return false;
  }

  if ( (m_direction != 0x00)
       && (m_direction != 0x01) )
  {
    ard_error(ARD_F("Switch Status message with invalid direction\n"));
    return false;
  }
  return true;
}

// void SwitchControlMessage::deSerialize(PktBufPtr pdu)
// {
//   // Sanity checks
//   if (pdu->curr_size<switch_control_msg_size){
//     ard_error(ARD_F("Train Control Message too short\n"));
//   }

//   m_toggle = pdu->data[0];
//   m_direction = pdu->data[1];

//   if ( (m_toggle != 0x00)
//        && (m_toggle != 0x01) )
//   {
//     ard_error(ARD_F("Switch Status message with invalid toggle value\n"));
//   }

//   if ( (m_direction != 0x00)
//        && (m_direction != 0x01) )
//   {
//     ard_error(ARD_F("Switch Status message with invalid direction\n"));
//   }

// }


/////////////////////////////////
// Switch Status Message
////////////////////////////////
SwitchStatusMessage::SwitchStatusMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
SwitchStatusMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->data[0] = m_direction;

  pdu->curr_size = switch_status_msg_size;
  return (pdu);
}

bool SwitchStatusMessage::analyze(uint8_t *a_p_data, size_t a_length){
  // Sanity checks
  if (a_length<switch_status_msg_size){
    ard_error(ARD_F("Switch Status Message too short\n"));
    return false;
  }

  m_direction = a_p_data[0];
  if ( (m_direction != 0x00)
       && (m_direction != 0x01)
       && (m_direction != 0x02) )
  {
    ard_error(ARD_F("Switch Status message with invalid direction\n"));
    return false;
  }

  return true;
}

// void SwitchStatusMessage::deSerialize(PktBufPtr pdu)
// {
//   // Sanity checks
//   if (pdu->curr_size<switch_status_msg_size){
//     ard_error(ARD_F("Switch Status Message too short\n"));
//   }

//   m_direction = pdu->data[0];
//   if ( (m_direction != 0x00)
//        && (m_direction != 0x01)
//        && (m_direction != 0x02) )
//   {
//     ard_error(ARD_F("Switch Status message with invalid direction\n"));
//   }
// }




/////////////////////////////////
// Switch Status Request Message
////////////////////////////////
SwitchStatusRequestMessage::SwitchStatusRequestMessage()
{
}

ArdUniquePtrMemPool<ArdPktBuffer>
SwitchStatusRequestMessage::serialize(PktMemPool *a_mem_pool) {
  ArdUniquePtrMemPool<ArdPktBuffer> pdu = a_mem_pool->AllocateSlot();

  pdu->curr_size = switch_status_request_msg_size;
  return (pdu);
}
