/**
 * @file ard_network_layer_interface_mock.h
 * @brief Network interface mocks used only by the tests.
 *
 * @note DO NOT USE THESE CLASSES
 *
 */
#ifndef ARD_NETWORK_LAYER_INTERFACE_MOCK_H
#define ARD_NETWORK_LAYER_INTERFACE_MOCK_H

#include "ard_network_layer_base.h"
#include "gmock/gmock.h"

template <typename T>
class ArdNorthInterfaceMock : public ArdNetNorthInterface<T> {
public:
  // virtual void sendRequest(PktBufPtr, T)
  MOCK_METHOD2_T(sendRequest, void(PktBufPtr, T));
};

class ArdSouthInterfaceMock : public ArdNetSouthInterface {
public:
  // virtual void onDataReceived(PktBufPtr a_p, AnyAddr a_src_addr,
  //                              LayerId a_l_id)
  MOCK_METHOD3(onDataReceived, void(PktBufPtr, AnyAddr, LayerId));
  //  virtual void dataHandlingDone(PktBufPtr a_p, bool res)
  MOCK_METHOD2(dataHandlingDone, void(PktBufPtr, bool));
};

#endif // ARD_NETWORK_LAYER_INTERFACE_MOCK_H
