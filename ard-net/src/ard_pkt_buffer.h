/**
 * @file ard_pkt_buffer.h
 *
 */

#ifndef ARD_PKTBUFFER_H
#define ARD_PKTBUFFER_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else
#include <cstdint> // for uint8_t
#include <sstream>
#endif
// see https://en.cppreference.com/w/cpp/types/integer

namespace ArdNetConstants {
constexpr uint8_t ard_pkt_buffer_data_size =
    40; /**< Number of bytes of the data attribute of the PktBuffer class  */
} // namespace ArdNetConstants

/**
 * @brief Simple data structure to hold serialized data.
 *
 */
struct ArdPktBuffer {
  uint8_t
      data[ArdNetConstants::ard_pkt_buffer_data_size]; ///< The buffer holding
                                                       ///< the packet data.
  uint8_t
      curr_size; ///< How many bytes are currently used in the data attribute.
  /**
   * @brief Default constructor.  Just set curr_size to zero.
   *
   */
  ArdPktBuffer();
  /**
   * @brief Reset the packet buffer (sets curr_size and data to 0)
   */
  void reset();

#ifndef ARDUINO_AVR_NANO
  /**
   * @brief Prints the index of each byte and the corresponding value in
   * hexadecimal
   *
   * @note This is only available in the native tests.  It is not compiled
   * for Arduino targets.
   */
  std::ostringstream prettyPrint() const;

  /**
   * @brief Copy constructor used by the tests.
   * @param other The object to copy.
   *
   * @note This is only available in the native tests.  It is not compiled
   * for Arduino targets.
   */
  ArdPktBuffer(const ArdPktBuffer &other) : curr_size(other.curr_size) {
    for (uint8_t i=0; i < other.curr_size; ++i) {
      data[i] = other.data[i];
    }
  }

  /**
   * @brief The assignemnt operator, used by the tests.
   *
   * @param other The object to copy from.
   * @return The object itself.
   *
   * @note This is only available in the native tests.  It is not compiled
   * for Arduino targets.
   */
  ArdPktBuffer &operator=(const ArdPktBuffer &other) {
    curr_size = other.curr_size;
    for (uint8_t i=0; i < other.curr_size; ++i) {
      data[i] = other.data[i];
    }
    return *this;
  }

#endif

  /**
   * @brief Comparison operator used by the tests.  Compares curr_size and
   * all the valid bytes in data[].
   *
   * @param b1 The first ArdPktBuffer to compare.
   * @param b2 The second ArdPktBuffer to compare.
   * @return True if curr_size and all the valid entries in data[] are equal.
   */
  friend bool operator==(const ArdPktBuffer &b1, const ArdPktBuffer &b2);
};

#endif // include guard ARD_PKTBUFFER_H
