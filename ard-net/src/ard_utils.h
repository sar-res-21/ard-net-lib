#ifndef ARD_UTILS_H
#define ARD_UTILS_H

/* From https://en.cppreference.com/w/cpp/types/remove_reference
 */
template <class T> struct ard_remove_reference { typedef T type; };
template <class T> struct ard_remove_reference<T &> { typedef T type; };
template <class T> struct ard_remove_reference<T &&> { typedef T type; };

/* from http://thbecker.net/articles/rvalue_references/section_08.html Similar
code also on this SO post: https://stackoverflow.com/a/7518365
 */
template <class T>
typename ard_remove_reference<T>::type &&ard_move(T &&a) noexcept {
  typedef typename ard_remove_reference<T>::type &&RvalRef;
  return static_cast<RvalRef>(a);
}

// From Stroustrup p. 1029.  As noted in the book, this version cannot be used
// to move lvalues (it should be OK for us).

template <typename T> void ard_swap(T &a, T &b) {
  T tmp{ard_move(a)};
  a = ard_move(b);
  b = ard_move(tmp);
}

void softResetArduino(bool a_print_trace=false);

#endif // End of include guard ARD_UTILS_H