#ifndef ARD_NET_APP_MSG_H
#define ARD_NET_APP_MSG_H

#include "ard_mempool.h"
#include "ard_network_address.h"
#include "ard_pkt_buffer.h"

using PktBufPtr = ArdUniquePtrMemPool<ArdPktBuffer>;
using PktMemPool = ArdMemPool<ArdPktBuffer>;

/********************************************************************
 *                    Application Messages                          *
 ********************************************************************/
/////////////////////////////////
// The Base
////////////////////////////////
enum AppMsgTypes {
  APP_TYP_PING = 0x00,
  APP_TYP_TRAIN_CONTROL  = 0x01,
  APP_TYP_SWITCH_CONTROL = 0x02,
  APP_TYP_TRAIN_STATUS   = 0x11,
  APP_TYP_SWITCH_STATUS  = 0x12,
  APP_TYP_TRAIN_STATUS_REQUEST  = 0x21,
  APP_TYP_SWITCH_STATUS_REQUEST = 0x22
};

class AppMessage {
public:
  /**
   * @brief Constructs a new AppMessage object.
   *
   */
  AppMessage();

  /**
   * @brief Serialize an AppMessage packet.
   *
   * Once every member field has been filled, call this method with
   * the payload in parameter using ard_move().
   * (eg. PktBufPtr pApp = msg.serialize(ard_move(pSdu));).
   *
   * @param sdu The payload of the message.
   * @param a_mem_pool A reference to the mempool to use to allocate the packet
   * buffer for the layer two frame.
   * @return The serialized packet to be passed to lower layer.
   */
  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktBufPtr sdu,
                                              PktMemPool *a_mem_pool);
  
    /**
   * @brief DeSerialize an AppMessage packet.
   *
   * Call this method with the received payload in parameter
   * using ard_move ().
   * (eg. PktBufPtr pUpper = msg.deSerialize(ard_move(pLower));).
   * This will fill extract header fields values in corresponding
   * object members (m_msg_type)
   * and return back the payload to be passed to the upper layer.
   *
   * @param pdu The payload from lower layer.
   * @param a_mem_pool A reference to the memory pool to use to allocate
   * the returned packet buffer for the payload packet buffer.
   * @return The packet payload to be passed to upper layer.
   */
  ArdUniquePtrMemPool<ArdPktBuffer> deSerialize(PktBufPtr pdu,
                                                PktMemPool *a_mem_pool);

  /**
   * @brief Parse an AppMessage packet without transfering buffer
   * property (i.e. NOT having to use ard_move()).
   *
   * Call this method with the received payload in parameter
   * (eg. analyze(pdu->data, pdu->curr_size); ).
   * This will fill extract header fields values in corresponding
   * object members (m_msg_type).
   *
   * @param a_p_data The pointer to the buffer to parse (pdu->data).
   * @param a_length The length of the buffer to parse (pdu->curr_size).
   * @return True is parsing was successful False otherwise.
   */
  bool analyze(uint8_t *a_p_data, size_t a_length);

  AppMsgTypes m_msg_type; /// Message Type Field

  const uint8_t app_header_size = 1;
};



/////////////////////////////////
// Ping Message
////////////////////////////////
class PingMessage {
public:
  /**
   * @brief Constructs a new PingMessage object.
   *
   */
  PingMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);
  void deSerialize(PktBufPtr pdu);

  uint32_t m_seq_num; /// Ping Sequence Number

  const uint8_t ping_msg_size = sizeof(m_seq_num);
};


/////////////////////////////////
// Train Control Message
////////////////////////////////
enum TrainDirections {
  TRAIN_DIR_STOP = 0x00,
  TRAIN_DIR_FW = 0x01,
  TRAIN_DIR_BW = 0x02
};

class TrainControlMessage {
public:
  /**
   * @brief Constructs a new TrainControlMessage object.
   *
   */
  TrainControlMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);
  bool analyze(uint8_t *a_p_data, size_t a_length);

  TrainDirections m_direction;   /// Direction 0:stop, 1:fwd, 2:bwd
  uint16_t m_duration;   /// Duration in ms (0: forever)
  uint8_t m_speed;       /// Speed: 0:stop; 255:max

  const uint8_t train_control_msg_size = 4;
};


/////////////////////////////////
// Train Status Message
////////////////////////////////
class TrainStatusMessage {
public:
  /**
   * @brief Constructs a new TrainControlMessage object.
   *
   */
  TrainStatusMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);
  bool analyze(uint8_t *a_p_data, size_t a_length);

  uint8_t m_direction;   /// Direction 0:stop, 1:fwd, 2:bwd
  uint8_t m_speed;       /// Speed: 0:stop; 255:max

  const uint8_t train_status_msg_size = 2;
};

/////////////////////////////////
// Train Status Request Message
////////////////////////////////
class TrainStatusRequestMessage {
public:
  /**
   * @brief Constructs a new TrainStatusRequestMessage object.
   *
   */
  TrainStatusRequestMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);

  const uint8_t train_status_request_msg_size = 0;
};

/////////////////////////////////
// Switch Control Message
////////////////////////////////
class SwitchControlMessage {
public:
  /**
   * @brief Constructs a new SwitchControlMessage object.
   *
   */
  SwitchControlMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);
  bool analyze(uint8_t *a_p_data, size_t a_length);

  uint8_t m_toggle;      /// Toggle 0:don't care, 1:change direction
  uint8_t m_direction;   /// Direction 0:straight, 1:turn

  const uint8_t switch_control_msg_size = 2;
};


/////////////////////////////////
// Switch Status Message
////////////////////////////////
class SwitchStatusMessage {
public:
  /**
   * @brief Constructs a new SwitchStatusMessage object.
   *
   */
  SwitchStatusMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);
  bool analyze(uint8_t *a_p_data, size_t a_length);

  uint8_t m_direction;   /// Direction 0:stop, 1:fwd, 2:bwd

  const uint8_t switch_status_msg_size = 1;
};

/////////////////////////////////
// Switch Status Request Message
////////////////////////////////
class SwitchStatusRequestMessage {
public:
  /**
   * @brief Constructs a new SwitchStatusRequestMessage object.
   *
   */
  SwitchStatusRequestMessage();

  ArdUniquePtrMemPool<ArdPktBuffer> serialize(PktMemPool *a_mem_pool);

  const uint8_t switch_status_request_msg_size = 0;
};





// FIXME: Move this to something like ard_utils
//        adapted from https://github.com/esp8266/Arduino/blob/master/libraries/Ethernet/src/utility/util.h
inline uint32_t htonl(uint32_t x) {
  return ( ((x)<<24 & 0xFF000000UL) | \
           ((x)<< 8 & 0x00FF0000UL) | \
           ((x)>> 8 & 0x0000FF00UL) | \
           ((x)>>24 & 0x000000FFUL) );
}
inline uint32_t ntohl(uint32_t x) { return htonl(x); }

inline uint16_t htons(uint16_t x) {
  return ( ((x)<< 8 & 0xFF00) | \
           ((x)>> 8 & 0x00FF) );
}
inline uint16_t ntohs(uint16_t x) { return htons(x); }

#endif // ARD_NET_APP_MSG_H
