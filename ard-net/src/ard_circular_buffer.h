/**
 * @file ard_circular_buffer.h
 * @brief Simple circular buffer container of fixed size.
 *
 */

#ifndef ARD_CIRCULAR_BUFFER_H
#define ARD_CIRCULAR_BUFFER_H

#ifdef ARDUINO_AVR_NANO
#include <stdint.h>
#else
#include <cstdint> // for uint8_t
#endif

#include "ard_error.h"
#include "ard_log.h"
#include "ard_utils.h"

namespace ArdNetConstants {
constexpr uint8_t circular_buff_size =
    6; /**< The number of slots in the buffer */
}

/**
 * @brief First In First Out queue, based on a circular buffer of fixed size
 * (defined in ArdNetConstants::circular_buff_size).
 *
 * @tparam T The type of objects stored in the buffer
 *
 * @note Users of this class must call isFull before calling addElement to
 * make sure that they are not trying to add an element to an already full
 * buffer.  Similarly, they must call isEmpty before calling removeElement to
 * make sure that they are not trying to remove an element from an empty
 * buffer.
 */
template <typename T> class ArdCircularBuffer {
public:
  ArdCircularBuffer() : m_head(0), m_count(0) {}
  /**
   * @brief Check if the buffer is empty.
   * @return Returns true if the buffer is empty (i.e., removeElement must
   * not be called), false otherwise.
   */
  bool isEmpty() { return (m_count == 0); }
  /**
   * @brief Check if the buffer is full
   * @return Returns true if the buffer is full, (i.e., addElement must not
   * be called), false otherwise.
   */
  bool isFull() { return m_count == ArdNetConstants::circular_buff_size; }
  /**
   * @brief Adds an element to the back of the queue (provided it is not full)
   *
   * @param a_elem The item to add to the buffer.
   *
   * @note Users of this class must call isFull before calling this method,
   * to make sure that there at least one empty slot.
   */
  void addElement(T a_elem);
  /**
   * @brief Adds an element to the front of the queue (provided it is not full)
   *
   * @param a_elem The item to add to the buffer.
   *
   * @note Users of this class must call isFull before calling this method,
   * to make sure that there at least one empty slot.
   */
  void addElementToFront(T a_elem);
  /**
   * @brief Removes the element at the front of the queue (provided it is not
   * empty)
   *
   * @return The element at the front of the queue.
   *
   * @note Users of this class must call isEmpty before calling this method,
   * to make sure that there is at least one element in the queue.
   */
  T removeElement();
  /**
   * @brief Returns the number of elements in the queue.
   *
   * @return The number of elements in the queue.
   */
  uint8_t count() { return m_count; }

private:
  T m_buff[ArdNetConstants::circular_buff_size];
  uint8_t m_count;
  uint8_t m_head;
};

template <typename T> void ArdCircularBuffer<T>::addElement(T a_elem) {
  if (m_count == ArdNetConstants::circular_buff_size) {
    ard_error(ARD_F("Trying to add an element to a full circular buffer\n"));
  }
  uint8_t tail = (m_head + m_count) % ArdNetConstants::circular_buff_size;
  m_buff[tail] = ard_move(a_elem);
  ++m_count;
}

template <typename T> void ArdCircularBuffer<T>::addElementToFront(T a_elem) {
  if (m_count == ArdNetConstants::circular_buff_size) {
    ard_error(ARD_F("Trying to add an element to a full circular buffer\n"));
  }
  if (m_head == 0) {
    m_head = ArdNetConstants::circular_buff_size - 1;
  } else {
    --m_head;
  }
  m_buff[m_head] = ard_move(a_elem);
  ++m_count;
}

template <typename T> T ArdCircularBuffer<T>::removeElement() {
  if (m_count == 0) {
    ard_error(ARD_F("Trying to remove an element from an empty circular "
                    "buffer\n"));
  }
  // it is OK to decrement m_count first because of zero-indexing
  --m_count;
  uint8_t e_index = m_head;
  m_head = (++m_head) % ArdNetConstants::circular_buff_size;
  return ard_move(m_buff[e_index]);
}

#endif // ARD_CIRCULAR_BUFFER_H
