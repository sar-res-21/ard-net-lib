#ifndef ARD_NETWORK_ETH_H
#define ARD_NETWORK_ETH_H

#include "ard_event_handler.h"
#include "ard_network_address.h"
#include <EtherCard.h>
#include "ard_event_manager.h"
#include "ard_pkt_buffer.h"
#include "ard_sys_interface.h"
#include "ard_network_layer_base.h"

 
class ArdNetworkEthernet;
 
class ArdEthEventHandler : public ArdEventHandler {
public:
  ArdEthEventHandler(ArdNetworkEthernet *m_net_layer_eth,
                     ArdSysInterface *a_ard_sys_int);
  virtual ~ArdEthEventHandler() = default;
  bool CheckForEvent() override;
  void RunEventHandler() override;

private:
  ArdNetworkEthernet *m_net_layer_eth;
  ArdSysInterface *m_ard_sys_int;
};




class ArdNetworkEthernet : public ArdNetworkLayer<L1Addr>,
                           public ArdNetNorthInterface<L1Addr> {
public:
  explicit ArdNetworkEthernet(PktMemPool *a_mem_pool,
                              ArdSysInterface *a_ard_sys_int,
                              ArdEventManager *a_event_manager,
                              L1Addr a_this_addr);
  void sendRequest(PktBufPtr, L1Addr) override;

  bool init();
  void retrieveEthernetFrame();

  uint8_t m_ss_pin = 7;

private:
  ArdEthEventHandler m_event_handler;
  static void ethListener(const char *data, uint16_t len);

  // SendPktBufferLOneFunc m_timer_func;
  // ArdTimerEventHandler m_timer_handler;
};



#endif // ARD_NETWORK_ETH_H
