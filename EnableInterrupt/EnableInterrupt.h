// in vim, :set ts=2 sts=2 sw=2 et

// EnableInterrupt, a library by GreyGnome.  Copyright 2014-2015 by Michael Anthony Schwager.

/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/ 

// Many definitions in /usr/avr/include/avr/io.h

#ifndef EnableInterrupt_h
#define EnableInterrupt_h
#include <Arduino.h>

#ifdef EI_ARDUINO_INTERRUPTED_PIN
extern uint8_t arduinoInterruptedPin;
#endif
// *************************************************************************************
// *************************************************************************************
// Function Prototypes *****************************************************************
// *************************************************************************************
// *************************************************************************************
// *** These are the only functions the end user (programmer) needs to consider.     ***
// *** This means you!                                                               ***
// *************************************************************************************
// *************************************************************************************

// Arduino Due (not Duemilanove) macros. Easy-peasy.
#if defined __SAM3U4E__ || defined __SAM3X8E__ || defined __SAM3X8H__
#ifdef NEEDFORSPEED
#error Due is already fast; the NEEDFORSPEED definition does not make sense on it.
#endif
define enableInterrupt(pin,userFunc,mode) attachInterrupt(pin, userFunc,mode)
define disableInterrupt(pin) detachInterrupt(pin)
#else

/* 
 * enableInterrupt- Sets up an interrupt on a selected Arduino pin.
 * or
 * enableInterruptFast- When used with the NEEDFORSPEED macro, sets up an interrupt on a selected Arduino pin.
 * 
 * Usage:
 * enableInterrupt(uint8_t pinNumber, void (*userFunction)(void), uint8_t mode);
 * or
 * enableInterrupt(uint8_t interruptDesignator, void (*userFunction)(void), uint8_t mode);
 *
 * For HiSpeed mode,
 * enableInterruptFast(uint8_t pinNumber, uint8_t mode);
 * or
 * enableInterruptFast(uint8_t interruptDesignator, uint8_t mode);
 *
 * ---------------------------------------------------------------------------------------
 *
 * disableInterrupt- Disables interrupt on a selected Arduino pin.
 *
 * Usage:
 *
 * disableInterrupt(uint8_t pinNumber);
 * or
 * disableInterrupt(uint8_t interruptDesignator);
 *
 * ---------------------------------------------------------------------------------------
 *
 * interruptDesignator: Essentially this is an Arduino pin, and if that's all you want to give
 * the function, it will work just fine. Why is it called an "interruptDesignator", then? Because
 * there's a twist: You can perform a bitwise "and" with the pin number and PINCHANGEINTERRUPT
 * to specify that you want to use a Pin Change Interrupt type of interrupt on those pins that
 * support both Pin Change and External Interrupts. Otherwise, the library will choose whatever
 * interrupt type (External, or Pin Change) normally applies to that pin, with priority to
 * External Interrupt. 
 *
 * The interruptDesignator is required because on the ATmega328 processor pins 2 and 3 support
 * ''either'' pin change or * external interrupts. On 644/1284-based systems, pin change interrupts
 * are supported on all pins and external interruptsare supported on pins 2, 10, and 11. 
 * Otherwise, each pin only supports a single type of interrupt and the
 * PINCHANGEINTERRUPT scheme changes nothing. This means you can ignore this whole discussion
 * for ATmega2560- or ATmega32U4-based Arduinos. You can probably safely ignore it for
 * ATmega328-based Arduinos, too.
 */

void enableInterrupt(uint8_t interruptDesignator, void (*userFunction)(void), uint8_t mode);
void disableInterrupt(uint8_t interruptDesignator);
void bogusFunctionPlaceholder(void);
#ifdef NEEDFORSPEED
#undef enableInterruptFast
// enableInterruptFast(uint8_t interruptDesignator, uint8_t mode);
#define enableInterruptFast(x, y) enableInterrupt(x, bogusFunctionPlaceholder, y)
#endif


// *************************************************************************************
// End Function Prototypes *************************************************************
// *************************************************************************************

#undef PINCHANGEINTERRUPT
#define PINCHANGEINTERRUPT 0x80

#undef attachPinChangeInterrupt
#undef detachPinChangeInterrupt
#define detachPinChangeInterrupt(pin)                   disableInterrupt(pin)
#define attachPinChangeInterrupt(pin,userFunc,mode)     enableInterrupt(pin, userFunc, mode)

#ifndef LIBCALL_ENABLEINTERRUPT // LIBCALL_ENABLEINTERRUPT ****************************************
#ifdef NEEDFORSPEED
extern void bogusFunctionPlaceholder(void);
#include "utility/ei_pindefs_speed.h"
#endif

// Example: EI_printPSTR("This is a nice long string that takes no static ram");
#define EI_printPSTR(x) EI_SerialPrint_P(PSTR(x))
extern void EI_SerialPrint_P(const char *str);

/* Arduino pin to ATmega port translaton is found doing digital_pin_to_port_PGM[] */
/* Arduino pin to PCMSKx bitmask is found by doing digital_pin_to_bit_mask_PGM[] */
/* ...except for PortJ, which is shifted left 1 bit in PCI1 */
extern volatile uint8_t *pcmsk;

// Arduino.h has these, but the block is surrounded by #ifdef ARDUINO_MAIN
#define PA 1
#define PB 2
#define PC 3
#define PD 4
#define PE 5
#define PF 6
#define PG 7
#define PH 8
#define PJ 10
#define PK 11
#define PL 12

typedef void (*interruptFunctionType)(void);

// ===========================================================================================
// CHIP SPECIFIC DATA STRUCTURES =============================================================
// ===========================================================================================

/* UNO SERIES *************************************************************************/
/* UNO SERIES *************************************************************************/
/* UNO SERIES *************************************************************************/
#if defined __AVR_ATmega168__ || defined __AVR_ATmega168A__ || defined __AVR_ATmega168P__ || \
  __AVR_ATmega168PA__ || \
  __AVR_ATmega328__ || __AVR_ATmega328P__

#define ARDUINO_328
#if defined EI_NOTPINCHANGE
#ifndef EI_NOTPORTB
#define EI_NOTPORTB
#endif
#ifndef EI_NOTPORTC
#define EI_NOTPORTC
#endif
#ifndef EI_NOTPORTD
#define EI_NOTPORTD
#endif
#endif // defined EI_NOTPINCHANGE

#ifndef NEEDFORSPEED
#define ARDUINO_PIN_B0 8
#define ARDUINO_PIN_B1 9
#define ARDUINO_PIN_B2 10
#define ARDUINO_PIN_B3 11
#define ARDUINO_PIN_B4 12
#define ARDUINO_PIN_B5 13
#define ARDUINO_PIN_C0 14
#define ARDUINO_PIN_C1 15
#define ARDUINO_PIN_C2 16
#define ARDUINO_PIN_C3 17
#define ARDUINO_PIN_C4 18
#define ARDUINO_PIN_C5 19
#define ARDUINO_PIN_D0 0
#define ARDUINO_PIN_D1 1
#define ARDUINO_PIN_D2 2
#define ARDUINO_PIN_D3 3
#define ARDUINO_PIN_D4 4
#define ARDUINO_PIN_D5 5
#define ARDUINO_PIN_D6 6
#define ARDUINO_PIN_D7 7

extern const uint8_t PROGMEM digital_pin_to_port_bit_number_PGM[];

#if ! defined(EI_NOTEXTERNAL) && ! defined(EI_NOTINT0) && ! defined (EI_NOTINT1)
extern interruptFunctionType functionPointerArrayEXTERNAL[2];
#endif

#ifndef EI_NOTPORTB
// 2 of the interrupts are unsupported on Arduino UNO.
struct functionPointersPortB {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
};
typedef struct functionPointersPortB functionPointersPortB;

extern functionPointersPortB portBFunctions;

// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how the ports were defined.
extern volatile uint8_t risingPinsPORTB;
extern volatile uint8_t fallingPinsPORTB;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotB;
#endif // EI_NOTPORTB

#ifndef EI_NOTPORTC
// 1 of the interrupts are used as RESET on Arduino UNO.
struct functionPointersPortC {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
};
typedef struct functionPointersPortC functionPointersPortC;

extern functionPointersPortC portCFunctions;

// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how the ports were defined.
extern volatile uint8_t risingPinsPORTC;
extern volatile uint8_t fallingPinsPORTC;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotC;
#endif // EI_NOTPORTC

#ifndef EI_NOTPORTD
// 1 of the interrupts are used as RESET on Arduino UNO.
struct functionPointersPortD {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
  interruptFunctionType pinSeven;
};
typedef struct functionPointersPortD functionPointersPortD;

extern functionPointersPortD portDFunctions;

// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how the ports were defined.
extern volatile uint8_t risingPinsPORTD;
extern volatile uint8_t fallingPinsPORTD;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotD;
#endif // EI_NOTPORTD
#endif // NEEDFORSPEED

// the PCINT?_vect's are defined in the avr.h files, like iom328p.h
#define PORTB_VECT PCINT0_vect
#define PORTC_VECT PCINT1_vect
#define PORTD_VECT PCINT2_vect

/* MEGA SERIES ************************************************************************/
/* MEGA SERIES ************************************************************************/
/* MEGA SERIES ************************************************************************/
#elif defined __AVR_ATmega640__ || defined __AVR_ATmega2560__ || defined __AVR_ATmega1280__ || \
  defined __AVR_ATmega1281__ || defined __AVR_ATmega2561__
#define ARDUINO_MEGA
#if defined EI_NOTPINCHANGE
#ifndef EI_NOTPORTB
#define EI_NOTPORTB
#endif
#ifndef EI_NOTPORTJ
#define EI_NOTPORTJ
#endif
#ifndef EI_NOTPORTK
#define EI_NOTPORTK
#endif
#endif

extern volatile uint8_t portJPCMSK; // This is a shifted version of PCMSK for PortJ, so I
			                         //	don't have to perform a shift in the IRQ.

#ifndef NEEDFORSPEED
// Pin change interrupts
#define ARDUINO_PIN_B0 53
#define ARDUINO_PIN_B1 52
#define ARDUINO_PIN_B2 51
#define ARDUINO_PIN_B3 50
#define ARDUINO_PIN_B4 10
#define ARDUINO_PIN_B5 11
#define ARDUINO_PIN_B6 12
#define ARDUINO_PIN_B7 13
#define ARDUINO_PIN_J0 15
#define ARDUINO_PIN_J1 14
// "fake" pins
#define ARDUINO_PIN_J2 70
#define ARDUINO_PIN_J3 71
#define ARDUINO_PIN_J4 72
#define ARDUINO_PIN_J5 73
#define ARDUINO_PIN_J6 74

#define ARDUINO_PIN_K0 62
#define ARDUINO_PIN_K1 63
#define ARDUINO_PIN_K2 64
#define ARDUINO_PIN_K3 65
#define ARDUINO_PIN_K4 66
#define ARDUINO_PIN_K5 67
#define ARDUINO_PIN_K6 68
#define ARDUINO_PIN_K7 69

#define ARDUINO_PIN_D0 21
#define ARDUINO_PIN_D1 20
#define ARDUINO_PIN_D2 19
#define ARDUINO_PIN_D3 18
#define ARDUINO_PIN_E4 2
#define ARDUINO_PIN_E5 3
#define ARDUINO_PIN_E6 75
#define ARDUINO_PIN_E7 76

extern const uint8_t PROGMEM digital_pin_to_port_bit_number_PGM[];

#if ! defined(EI_NOTEXTERNAL) && ! defined(EI_NOTINT0) && ! defined(EI_NOTINT1) && ! defined(EI_NOTINT2) && ! defined(EI_NOTINT3) && ! defined(EI_NOTINT4) && ! defined(EI_NOTINT5) && ! defined(EI_NOTINT6) && ! defined(EI_NOTINT7)
interruptFunctionType functionPointerArrayEXTERNAL[8];
#endif

#ifndef EI_NOTPORTB
struct functionPointersPortB {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
  interruptFunctionType pinSeven;
};
typedef struct functionPointersPortB functionPointersPortB;

extern functionPointersPortB portBFunctions;

extern volatile uint8_t risingPinsPORTB;
extern volatile uint8_t fallingPinsPORTB;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotB;
#endif

#ifndef EI_NOTPORTJ
// only 7 pins total of port J are supported as interrupts on the ATmega2560,
// and only PJ0 and 1 are supported on the Arduino MEGA.
// For PCI1 the 0th bit is PE0.   PJ2-6 are not exposed on the Arduino pins, but
// we will support them anyway. There are clones that provide them, and users may
// solder in their own connections (...go, Makers!)
struct functionPointersPortJ {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
};
typedef struct functionPointersPortJ functionPointersPortJ;

extern functionPointersPortJ portJFunctions;

// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how we were defined.
extern volatile uint8_t risingPinsPORTJ;
extern volatile uint8_t fallingPinsPORTJ;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotJ;
#endif

#ifndef EI_NOTPORTK
struct functionPointersPortK {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
  interruptFunctionType pinSeven;
};
typedef struct functionPointersPortK functionPointersPortK;

extern functionPointersPortK portKFunctions;

extern volatile uint8_t risingPinsPORTK;
extern volatile uint8_t fallingPinsPORTK;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotK;
#endif
#endif // NEEDFORSPEED

#define PORTB_VECT PCINT0_vect
#define PORTJ_VECT PCINT1_vect
#define PORTK_VECT PCINT2_vect

/* LEONARDO ***************************************************************************/
/* LEONARDO ***************************************************************************/
/* LEONARDO ***************************************************************************/
#elif defined __AVR_ATmega32U4__ || defined __AVR_ATmega16U4__
#define ARDUINO_LEONARDO
#if defined EI_NOTPINCHANGE
#ifndef EI_NOTPORTB
#define EI_NOTPORTB
#endif
#endif

#ifndef NEEDFORSPEED
#define ARDUINO_PIN_B0 17
#define ARDUINO_PIN_B1 15
#define ARDUINO_PIN_B2 16
#define ARDUINO_PIN_B3 14
#define ARDUINO_PIN_B4 8
#define ARDUINO_PIN_B5 9
#define ARDUINO_PIN_B6 10
#define ARDUINO_PIN_B7 11
#define ARDUINO_PIN_D0 3
#define ARDUINO_PIN_D1 2
#define ARDUINO_PIN_D2 0
#define ARDUINO_PIN_D3 1
#define ARDUINO_PIN_E6 7

/* To derive this list: 
   sed -n -e '1,/digital_pin_to_port_PGM/d' -e '/^}/,$d' -e '/P/p' \
       /usr/share/arduino/hardware/arduino/variants/leonardo/pins_arduino.h | \
       awk '{print "  ", $5 ", // " $5 "  pin: " $3}'
   ...then massage the output as necessary to create the below:
*/

extern const uint8_t PROGMEM digital_pin_to_port_bit_number_PGM[];


#if ! defined(EI_NOTEXTERNAL) && ! defined(EI_NOTINT0) && ! defined(EI_NOTINT1) && ! defined(EI_NOTINT2) && ! defined(EI_NOTINT3) && ! defined(EI_NOTINT6)
extern interruptFunctionType functionPointerArrayEXTERNAL[5];
#endif

#ifndef EI_NOTPORTB
struct functionPointersPortB {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
  interruptFunctionType pinSeven;
};
typedef struct functionPointersPortB functionPointersPortB;

extern functionPointersPortB portBFunctions;

// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how we were defined.
extern volatile uint8_t risingPinsPORTB;
extern volatile uint8_t fallingPinsPORTB;

// for the saved state of the ports
// extern static volatile uint8_t portSnapshotB;
#endif
#endif // NEEDFOR SPEED

#define PORTB_VECT PCINT0_vect

/* 644/1284 ***************************************************************************/
/* 644/1284 ***************************************************************************/
/* 644/1284 ***************************************************************************/
#elif defined __AVR_ATmega1284P__ || defined __AVR_ATmega1284__ || defined(__AVR_ATmega644P__) || defined(__AVR_ATmega644__)
#define MIGHTY1284
#if defined EI_NOTPINCHANGE
#ifndef EI_NOTPORTA
#define EI_NOTPORTA
#endif
#ifndef EI_NOTPORTB
#define EI_NOTPORTB
#endif
#ifndef EI_NOTPORTC
#define EI_NOTPORTC
#endif
#ifndef EI_NOTPORTD
#define EI_NOTPORTD
#endif
#endif

#ifndef INPUT_PULLUP
#define INPUT_PULLUP 0x2
#endif

#ifndef NEEDFORSPEED
#define ARDUINO_PIN_A0 24
#define ARDUINO_PIN_A1 25
#define ARDUINO_PIN_A2 26
#define ARDUINO_PIN_A3 27
#define ARDUINO_PIN_A4 28
#define ARDUINO_PIN_A5 29
#define ARDUINO_PIN_A6 30
#define ARDUINO_PIN_A7 31
#define ARDUINO_PIN_B0 0
#define ARDUINO_PIN_B1 1
#define ARDUINO_PIN_B2 2
#define ARDUINO_PIN_B3 3
#define ARDUINO_PIN_B4 4
#define ARDUINO_PIN_B5 5
#define ARDUINO_PIN_B6 6
#define ARDUINO_PIN_B7 7
#define ARDUINO_PIN_C0 16
#define ARDUINO_PIN_C1 17
#define ARDUINO_PIN_C2 18
#define ARDUINO_PIN_C3 19
#define ARDUINO_PIN_C4 20
#define ARDUINO_PIN_C5 21
#define ARDUINO_PIN_C6 22
#define ARDUINO_PIN_C7 23
#define ARDUINO_PIN_D0 8
#define ARDUINO_PIN_D1 9
#define ARDUINO_PIN_D2 10
#define ARDUINO_PIN_D3 11
#define ARDUINO_PIN_D4 12
#define ARDUINO_PIN_D5 13
#define ARDUINO_PIN_D6 14
#define ARDUINO_PIN_D7 15

extern const uint8_t PROGMEM digital_pin_to_port_bit_number_PGM[];


#if ! defined(EI_NOTEXTERNAL) && ! defined(EI_NOTINT0) && ! defined(EI_NOTINT1) && ! defined(EI_NOTINT2)
extern interruptFunctionType functionPointerArrayEXTERNAL[3];
#endif

struct functionPointers {
  interruptFunctionType pinZero;
  interruptFunctionType pinOne;
  interruptFunctionType pinTwo;
  interruptFunctionType pinThree;
  interruptFunctionType pinFour;
  interruptFunctionType pinFive;
  interruptFunctionType pinSix;
  interruptFunctionType pinSeven;
};

#ifndef EI_NOTPORTA
typedef struct functionPointers functionPointersPortA;
extern functionPointers portAFunctions;
// For Pin Change Interrupts; since we're duplicating FALLING and RISING in software,
// we have to know how the ports were defined.
extern volatile uint8_t risingPinsPORTA;
extern volatile uint8_t fallingPinsPORTA;
// for the saved state of the ports
// extern static volatile uint8_t portSnapshotA;
#endif

#ifndef EI_NOTPORTB
typedef struct functionPointers functionPointersPortB;
extern functionPointersPortB portBFunctions;
extern volatile uint8_t risingPinsPORTB;
extern volatile uint8_t fallingPinsPORTB;
// extern static volatile uint8_t portSnapshotB;
#endif

#ifndef EI_NOTPORTC
typedef struct functionPointers functionPointersPortC;
extern functionPointersPortC portCFunctions;
extern volatile uint8_t risingPinsPORTC;
extern volatile uint8_t fallingPinsPORTC;
// extern static volatile uint8_t portSnapshotC;
#endif

#ifndef EI_NOTPORTD
extern functionPointersPortD portDFunctions;
extern volatile uint8_t risingPinsPORTD;
extern volatile uint8_t fallingPinsPORTD;
// extern static volatile uint8_t portSnapshotD;
#endif
#endif // NEEDFORSPEED



// the vectors (eg, "PCINT0_vect") are defined in the avr.h files, like iom1284p.h
#define PORTA_VECT PCINT0_vect
#define PORTB_VECT PCINT1_vect
#define PORTC_VECT PCINT2_vect
#define PORTD_VECT PCINT3_vect

#endif // #if defined __AVR_ATmega168__ || defined __AVR_ATmega168A__ ... (etc.) ...

// ===========================================================================================
// END END END DATA STRUCTURES ===============================================================
// ===========================================================================================

// From /usr/share/arduino/hardware/arduino/cores/robot/Arduino.h
// #define CHANGE 1
// #define FALLING 2
// #define RISING 3

// ===========================================================================================
// ===========================================================================================
// enableInterrupt(interrupDesignator, userFunction, mode); ==================================
// ===========================================================================================
// ===========================================================================================

// "interruptDesignator" is simply the Arduino pin optionally OR'ed with
// PINCHANGEINTERRUPT (== 0x80)
void enableInterrupt(uint8_t interruptDesignator, interruptFunctionType userFunction, uint8_t mode);

// ===========================================================================================
// ===========================================================================================
// disableInterrupt(interrupDesignator); =====================================================
// ===========================================================================================
// ===========================================================================================
void disableInterrupt (uint8_t interruptDesignator);

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
////////////////////// ISRs /////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef EI_NOTEXTERNAL
#ifndef EI_NOTINT0
ISR(INT0_vect);
#endif // EI_NOTINT0

#ifndef EI_NOTINT1
ISR(INT1_vect);
#endif // EI_NOTINT1

#if defined ARDUINO_MEGA || defined ARDUINO_LEONARDO || defined MIGHTY1284
#ifndef EI_NOTINT2
ISR(INT2_vect);
#endif // EI_NOTINT2
#endif // ARDUINO_MEGA || ARDUINO_LEONARDO || MIGHTY1284

#if defined ARDUINO_MEGA || defined ARDUINO_LEONARDO
#ifndef EI_NOTINT3
ISR(INT3_vect);
#endif // EI_NOTINT3
#endif // ARDUINO_MEGA || ARDUINO_LEONARDO

#if defined ARDUINO_MEGA
#ifndef EI_NOTINT4
ISR(INT4_vect);
#endif // EI_NOTINT4

#ifndef EI_NOTINT5
ISR(INT5_vect);
#endif // EI_NOTINT5

#ifndef EI_NOTINT6
ISR(INT6_vect);
#endif // EI_NOTINT6

#ifndef EI_NOTINT7
ISR(INT7_vect);
#endif // EI_NOTINT7
#endif // defined ARDUINO_MEGA

#if defined ARDUINO_LEONARDO
#ifndef EI_NOTINT6
ISR(INT6_vect);
#endif // EI_NOTINT6
#endif // defined ARDUINO_LEONARDO
#endif // EI_NOTEXTERNAL

#if defined MIGHTY1284
#ifndef EI_NOTPORTA
ISR(PORTA_VECT);
#endif // EI_NOTPORTA
#endif // MIGHTY1284

#ifndef EI_NOTPORTB
ISR(PORTB_VECT);
#endif // EI_NOTPORTB

#if defined ARDUINO_328 || defined MIGHTY1284
#ifndef EI_NOTPORTC
ISR(PORTC_VECT);
#endif // EI_NOTPORTC

#ifndef EI_NOTPORTD
ISR(PORTD_VECT);
#endif // EI_NOTPORTD

#elif defined ARDUINO_MEGA
#ifndef EI_NOTPORTJ
ISR(PORTJ_VECT);
#endif // EI_NOTPORTJ

#ifndef EI_NOTPORTK
ISR(PORTK_VECT);
#endif // EI_NOTPORTK
#elif defined ARDUINO_LEONARDO
  // No other Pin Change Interrupt ports than B on Leonardo
#endif // defined ARDUINO_328

#endif // #ifndef LIBCALL_ENABLEINTERRUPT *********************************************************
#endif // #if defined __SAM3U4E__ || defined __SAM3X8E__ || defined __SAM3X8H__
#endif // #ifndef EnableInterrupt_h ***************************************************************
